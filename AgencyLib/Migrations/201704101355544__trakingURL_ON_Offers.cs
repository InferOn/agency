namespace AgencyLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _trakingURL_ON_Offers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "TrackingURL", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Offers", "TrackingURL");
        }
    }
}
