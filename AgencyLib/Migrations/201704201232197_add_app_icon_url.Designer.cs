// <auto-generated />
namespace AgencyLib.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class add_app_icon_url : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_app_icon_url));
        
        string IMigrationMetadata.Id
        {
            get { return "201704201232197_add_app_icon_url"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
