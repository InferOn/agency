namespace AgencyLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_trackingUrl_to_subscribed_offers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OfferSubscribeds", "TrackingURL", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OfferSubscribeds", "TrackingURL");
        }
    }
}
