namespace AgencyLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_app_icon_url : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "AppIconUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Offers", "AppIconUrl");
        }
    }
}
