namespace AgencyLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_preview_url : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "PreviewUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Offers", "PreviewUrl");
        }
    }
}
