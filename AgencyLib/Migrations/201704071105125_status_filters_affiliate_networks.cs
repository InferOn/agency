namespace AgencyLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class status_filters_affiliate_networks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AffiliateNetworks", "Active", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AffiliateNetworks", "Active");
        }
    }
}
