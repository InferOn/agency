﻿using System.Data.Entity;
using AgencyLib.Data;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace AgencyLib {
	public interface IAgencyDataContext {
		IDbSet<AffiliateNetwork> AffiliateNetworks { get; set; }
		DbSet<Offer> Offers { get; set; }
		IDbSet<Data.Notification.Notification> Notifications { get; set; }
		IDbSet<Data.Notification.Notification.Recipient> Recipients { get; set; }
		DbSet<OfferSubscribed> OffersSubscribed { get; set; }
		IDbSet<Campain> Campains { get; set; }
		IDbSet<TrafficSource> TrafficSources { get; set; }
		IDbSet<OfferPayout> OfferPayouts { get; set; }
		DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
		IDbSet<CampainPayout> CampainPayouts { get; set; }

		IDbSet<CampainHistory> CampainHistories { get; set; }
		



		//IDbSet<Category> Categories { get; set; }
		int SaveChanges();
	}
	public class AgencyContext : IdentityDbContext<IdentityUser>, IAgencyDataContext {
		public AgencyContext() : base("AgencyContext") { }
		public DbSet<Offer> Offers { get; set; }
		public DbSet<OfferSubscribed> OffersSubscribed { get; set; }
		public IDbSet<Campain> Campains { get; set; }
		public IDbSet<TrafficSource> TrafficSources { get; set; }
		public IDbSet<OfferPayout> OfferPayouts { get; set; }
		public IDbSet<CampainPayout> CampainPayouts { get; set; }
		public IDbSet<Data.Notification.Notification> Notifications { get; set; }
		public IDbSet<Data.Notification.Notification.Recipient> Recipients { get; set; }
		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			modelBuilder.Entity<AgencyLib.Data.Campain>().Property(x => x.bidPrice).HasPrecision(18, 3);
			base.OnModelCreating(modelBuilder);
		}
		public IDbSet<AffiliateNetwork> AffiliateNetworks { get; set; }

		public IDbSet<CampainHistory> CampainHistories { get; set; }
	}
}