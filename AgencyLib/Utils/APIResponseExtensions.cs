﻿using AgencyLib.Data;
using AgencyLib.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AgencyLib.Utils {
	public static class APIResponseExtensions {
		public static Data.Offer ToDataOffer(this Providers.AffiliateNetworks.APIResponse.Offer source, int networkId) {
			var _do = new Data.Offer() {
				NetworkId = networkId,
				OfferId = int.Parse(source.id ?? source.offer_id),
				Name = source.name,
				AppIconUrl = source.appIconUrl,
				PreviewUrl = source.preview_url,
				CreativeUrl = source.CreativeURL,
				Description = source.description,
				Countries = source.countries,
				DateExpiration = source.expiration_date,
				Currency = source.currency,
				ConversionType = source.payout_type,
				Status = source.status,
				#region app promoters only
				//AppRating = NULL,
				AppOS = source.platform,
				//AppPrice = NULL,
				//AppID = NULL,
				//AppDescription = NULL,
				//AppCategories = NULL,
				//TrackingId = null,

				TrackingURL = source.TrackingURL ?? string.Empty,
				#endregion

				#region hasoffers only
				require_approval = source.require_approval,
				require_terms_and_conditions = source.require_terms_and_conditions,
				terms_and_conditions = source.terms_and_conditions,
				preview_url = source.preview_url,
				default_payout = source.default_payout,
				percent_payout = source.percent_payout,
				featured = source.featured,
				allow_website_links = source.allow_website_links,
				allow_direct_links = source.allow_direct_links,
				show_custom_variables = source.show_custom_variables,
				show_mail_list = source.show_mail_list,
				dne_list_id = source.dne_list_id,
				email_instructions = source.email_instructions,
				email_instructions_from = source.email_instructions_from,
				email_instructions_subject = source.email_instructions_subject,
				has_goals_enabled = source.has_goals_enabled,
				default_goal_name = source.default_goal_name,
				use_target_rules = source.use_target_rules,
				is_expired = source.is_expired,
				dne_download_url = source.dne_download_url,
				dne_unsubscribe_url = source.dne_unsubscribe_url,
				dne_third_party_list = source.dne_third_party_list,
				approval_status = source.approval_status,

				#endregion
			};
			_do.PayoutCap = ParseCaps(source.payout_cap);
			_do.ConversionCap = ParseCaps(source.conversion_cap);
			_do.InitialPayoutCap = ParseCaps(source.payout_cap);
			_do.InitialConversionCap = ParseCaps(source.conversion_cap);

			return _do;
		}
		public static Data.Campain ToDataCampain(this Providers.Campains.Campain source, int trafficSourceId) {
			var dc = new Data.Campain() {
				campaignId = int.Parse(source.campaignId),
				trafficSourceId = trafficSourceId,
				Status = source.status,
				bidPrice = source.budget.bidPrice,
				dailyBudget = source.budget.dailyBudget,
				pricingModel = source.budget.pricingModel,
				totalBudget = source.budget.totalBudget,
				createdDate = DateTime.Parse(source.campaignInfo.createdDate),
				hasEndTime = source.campaignInfo.hasEndTime,
				hasStartTime = source.campaignInfo.hasStartTime,
				Name = source.campaignInfo.name,
				startDate = DateTime.Parse(source.campaignInfo.startDate),
				updateDate = DateTime.Parse(source.campaignInfo.updateDate),
				appName = source.campaignType.appName,
				category = source.campaignType.category,
				destinationUrl = source.campaignType.destinationUrl,
				platform = source.campaignType.platform,
			};
			dc.countries = string.Join("|", source.targeting.geo.countries.value.Select(x => x.countryKey));
			return dc;
		}
		private static decimal ParseCaps(string cap) {
			return decimal.Parse((cap ?? "0").Replace(".", ","), System.Globalization.NumberStyles.AllowDecimalPoint);
		}
		public static IEnumerable<Diff> Diff<T>(this T trg, T src) {
			foreach (PropertyInfo pi in trg
				.GetType()
					.GetRuntimeProperties()
						.Where(x => !Attribute.IsDefined(x, typeof(DiffIgnoreAttribute)))) {
				var v = new Diff() {
					Prop = pi.Name,
					trgVal = pi.GetValue(trg),
					srcVal = pi.GetValue(src)
				};
				if (Equals(v.trgVal, v.srcVal)) continue;
				yield return v;
			}
		}
	}
}
