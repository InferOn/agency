﻿using AgencyLib.Data;
using AgencyLib.Providers.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using AgencyLib.Utils;
using System.Reflection;

namespace AgencyLib.Providers {
	public sealed class CampainProvider {
		private IAgencyDataContext context { get; set; }
		private CampainProvider() {

		}
		private class NestedProvider {
			static NestedProvider() {

			}
			internal static readonly CampainProvider instance = new CampainProvider();
		}
		public static CampainProvider Instance { get { return NestedProvider.instance; } }
		public void FetchFrom(TrafficSource trafficsource, IAgencyDataContext context) {
			this.context = context;
			var setting = Newtonsoft.Json.JsonConvert.DeserializeObject<TrafficSource.AccountSetting>(trafficsource.AccountSettings);
			var apiresolver = ApiResolverFactory.Find(setting);
			Campains.APIResponse apiResponse = apiresolver.FindAll(CreateArg(setting));
			if (apiResponse.status != -1 && apiResponse.data.Any()) {
				IEnumerable<Campains.Campain> remoteData = apiResponse.data.Select((KeyValuePair<string, Campains.Campain> x) => x.Value);
				List<Data.Campain> inPlace = context.Campains.Where(x => x.trafficSourceId == trafficsource.Id).ToList();
				var remoteAlreadyInPlace = remoteData.Where(x => inPlace.Select(y => y.campaignId.ToString()).Contains(x.campaignId));
				Update(remoteAlreadyInPlace.ToList(), inPlace, context, trafficsource.Id);
				var remoteStillNotInPlace = remoteData.Except(remoteAlreadyInPlace);
				Insert(remoteStillNotInPlace, trafficsource.Id, context);
				trafficsource.LastGet = DateTime.Now;
				context.SaveChanges();
			}
		}
		private void Insert(IEnumerable<Campains.Campain> remoteStillNotInPlace, int traffricSourceId, IAgencyDataContext context) {
			foreach (Campains.Campain cmp in remoteStillNotInPlace) {
				context.Campains.Add(cmp.ToDataCampain(traffricSourceId));
			}
			context.SaveChanges();
		}
		private void Update(List<Campains.Campain> remoteAlreadyInPlace, List<Data.Campain> inPlace, IAgencyDataContext context, int id) {
			var shouldSave = false;
			foreach (Data.Campain p in inPlace) {
				Campains.Campain c = remoteAlreadyInPlace.Where(x => (x.campaignId) == p.campaignId.ToString()).SingleOrDefault();
				if (c != null) {
					var remote = c.ToDataCampain(p.trafficSourceId);
					var diffs = p.Diff(remote);
					if (null != diffs) {
						shouldSave = diffs.Any();
						if (shouldSave) {
							updateCampain(p, diffs);
						}
					}
				}
			}
			if (shouldSave) {
				context.SaveChanges();
			}
		}
		private void updateCampain(Data.Campain trg, IEnumerable<Diff> diffs) {
			foreach (Diff d in diffs) {
				trg.GetType().InvokeMember(d.Prop,
					BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
					Type.DefaultBinder, trg, new[] { d.srcVal });
			}
		}
		private static Argument CreateArg(TrafficSource.AccountSetting setting) {
			var action = new Argument() { BaseURL = setting.apiURL, Resource = setting.resource, Params = new Dictionary<string, string>() };
			foreach (var kv in setting.args)
				action.Params.Add(kv.Key, kv.Value);
			return action;
		}
	}
	/// <summary>
	/// compare-two-objects-and-find-the-differences
	/// <see cref="http://stackoverflow.com/questions/4951233/compare-two-objects-and-find-the-differences"/>
	/// <see cref="http://stackoverflow.com/questions/2051065/check-if-property-has-attribute"/>
	/// </summary>
	static class extensions {
		
	}
}
