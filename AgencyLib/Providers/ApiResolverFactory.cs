﻿using AgencyLib.Data;
using AgencyLib.Providers.Resolvers.Targets;
using System;
using AgencyLib.Providers.Actions;
using AgencyLib.Providers.Resolvers;

namespace AgencyLib.Providers {
	public static class ApiResolverFactory {
		public static AffiliateNetworks.IApiResolver Find(AffiliateNetwork.AccountSetting settings) {
			AffiliateNetworks.IApiResolver resolver = null;
			switch (settings.resolver) {
				case "HasOffers":
					resolver = new HasOfferAPIResolver(settings.apiURL, settings.args["api_key"], settings.args["NetworkId"]);
					break;
				case "IconPeak":
					resolver = new IconPeakAPIResolver(settings.apiURL, settings.resource, settings.args);
					break;
				case "AppPromoters":
					resolver = new AppPromotersAPIResolver(settings.apiURL);
					break;
				case "Clink":
					resolver = new ClinkAPIResolver(settings.apiURL);
					break;

				default:
					throw new Exception(string.Format("Missing api resolver for {0}", settings.resolver));
			}
			return resolver;
		}
		public static TrafficSources.IApiResolver Find(TrafficSource.AccountSetting settings) {
			TrafficSources.IApiResolver resolver = null;
			switch (settings.resolver) {
				case "startapp":
					//throw new Exception("ti sei dimenticato che devi fare il resolver!!!");
					resolver = new StartAppAPIResolver(settings.apiURL, settings.args["token"], settings.args["partner"]);
					break;


				default:
					throw new Exception(string.Format("Missing api resolver for {0}", settings.resolver));
			}
			return resolver;
		}
	}
	namespace AffiliateNetworks {
		public interface IApiResolver {
			APIResponse FindAll(Argument args);
			APIResponse Find(Argument args);
			string FetchTrackingURL(string offerId, string affiliatedId);
			string FetchCountries(string offerId, string affiliatedId);
			CreativitiesResult FetchCreativities(Argument args);
			bool RequestOfferAccess(Argument argument);
		}
	}
	namespace TrafficSources {
		public interface IApiResolver {
			Campains.APIResponse FindAll(Argument args);


		}
	}
}
