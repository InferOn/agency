﻿namespace AgencyLib.Providers
{
    public class Diff
    {
        public string Prop
        {
            get;
            set;
        }

        public object trgVal
        {
            get;
            set;
        }

        public object srcVal
        {
            get;
            set;
        }
    }
}