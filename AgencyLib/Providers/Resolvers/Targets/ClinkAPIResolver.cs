﻿using System.Collections.Generic;
using System.Net.Http;
using AgencyLib.Providers.Actions;
using AgencyLib.Providers.AffiliateNetworks;
using AgencyLib.Providers.Resolvers.Targets.AffiliateNetworks;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		public class ClinkAPIResolver : APIResolver {
			private string _baseUrl;
			public ClinkAPIResolver(string baseUrl) {
				_baseUrl = baseUrl;
			}
			public override APIResponse FindAll(Actions.Argument action) {
				var resp = new APIResponse() {
					response = new APIResponse.Response() {
						httpStatus = 200,
					}
				};
				try {
					using (var httpClient = new HttpClient()) {
						// http://api.clinkad.com/offer_api_v2?key=md94endmwc1himxp&pubid=3205&pagesize=100&page=1   
						var sb = new System.Text.StringBuilder(_baseUrl);
						sb.Append(action.Resource + "?");
						var count = 0;
						foreach (var param in action.Params) {
							count++;
							sb.AppendFormat("{0}={1}", param.Key, param.Value);
							if (count < action.Params.Count) {
								sb.Append("&");
							}
						}
						HttpResponseMessage response = null;
						var page_index = 1;
						var page_total = 0;
						do {
							var sbstring = sb.ToString();
							response = httpClient.GetAsync(sbstring + string.Format("&pagesize=100&page={0}", page_index)).Result;
							if (response.IsSuccessStatusCode) {
								var responseContent = response.Content;
								var result = Newtonsoft.Json.JsonConvert.DeserializeObject<APIResponse.ClinkRootObject>(responseContent.ReadAsStringAsync().Result);
								page_total = result.page_total;
								page_index = ++result.page_index;
								resp.response = resp.response ?? new APIResponse.Response();
								resp.response.data = resp.response.data ?? new Dictionary<string, APIResponse.OfferContainer>();
								foreach (var item in result.offers) {
									try 
									{
										if (!resp.response.data.ContainsKey(item.oid.ToString())) {
											resp.response.data.Add(item.oid.ToString(), new APIResponse.OfferContainer() { Offer = item.toOffer() });
										}
									}
									catch (System.Exception exx)
									{
									}
								}
							}
						} while (page_index <= page_total);
					}
				}
				catch (System.Exception ex) {
					// UNDONE: manage here the api request fails
					//throw;
					resp = new APIResponse() {
						response = new APIResponse.Response() {
							httpStatus = 500,
						}
					};
				}
				return resp;
			}

			public override APIResponse Find(Argument args) {
				return new APIResponse() {
					response = new APIResponse.Response() {
						httpStatus = 200,
						status = 1,
						data = new Dictionary<string, APIResponse.OfferContainer>(),
					}
				};
			}
		}
	}
}
