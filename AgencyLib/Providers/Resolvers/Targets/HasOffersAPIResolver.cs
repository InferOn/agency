﻿using System;
using System.Collections.Generic;
using RestSharp;
using Newtonsoft.Json;
using AgencyLib.Providers.Actions;
using AgencyLib.Providers.AffiliateNetworks;
using AgencyLib.Providers.Resolvers.Targets.AffiliateNetworks;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		public class HasOfferAPIResolver : APIResolver {
			private string _baseUrl, _key, _networkId;
			public HasOfferAPIResolver(string baseUrl, string key, string networkId) {
				_baseUrl = baseUrl;
				_key = key;
				_networkId = networkId;
			}
			public override APIResponse Find(Actions.Argument action) {
				/*
				 https://api.hasoffers.com/Apiv3/json?
					NetworkId=iconpeak
					&api_key=b8b748b25c08ad5255c5e34440947a45a040e2d736ed71565d86f75bdf09d739
					&Target=Affiliate_Offer
					&Method=findById
					&id=10270
				 */
				var content = string.Empty;
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				try {
					content = client.Execute<APIResponse>(request).Content;
				}
				catch (Exception) {
					//throw;
					var resp = new APIResponse() {
						response = new APIResponse.Response() {
							httpStatus = 500
						}
					};
					content = Newtonsoft.Json.JsonConvert.SerializeObject(resp, Newtonsoft.Json.Formatting.Indented);
				}

				var result = Newtonsoft.Json.JsonConvert.DeserializeObject<APIResponse>(content);
				return result;
			}
			public override APIResponse FindAll(Argument action) {
				/*
				 https://api.hasoffers.com/Apiv3/json?NetworkId=iconpeak&api_key=b8b748b25c08ad5255c5e34440947a45a040e2d736ed71565d86f75bdf09d739&Target=Affiliate_Offer&Method=findAll
				*/
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				var content = string.Empty;
				try {
					content = client.Execute<APIResponse>(request).Content;
				}
				catch (Exception) {
					//throw;
					var resp = new APIResponse() {
						response = new APIResponse.Response() {
							httpStatus = 500
						}
					};
					content = JsonConvert.SerializeObject(resp, Formatting.Indented);
				}

				var result = JsonConvert.DeserializeObject<APIResponse>(content);
				return result;
			}
			public override string FetchTrackingURL(string offerId, string affiliatedId) {
				/* https://api.hasoffers.com/Apiv3/json?
				NetworkId=iconpeak&
				api_key=b8b748b25c08ad5255c5e34440947a45a040e2d736ed71565d86f75bdf09d739&
				Target=Affiliate_Offer&
				Method=generateTrackingLink&
				offer_id=87786
				*/
				var client = new RestClient("https://api.hasoffers.com/Apiv3/");
				var request = new RestRequest("json");
				request.AddQueryParameter("NetworkId", _networkId);
				request.AddQueryParameter("Target", "Affiliate_Offer");
				request.AddQueryParameter("Method", "generateTrackingLink");
				request.AddQueryParameter("offer_id", offerId);
				request.AddQueryParameter("api_key", _key);
				var trackingUrl = base.FetchTrackingURL(offerId, affiliatedId);
				try {
					var trackingResponseString = client.Execute<TrackingURLResponse>(request).Content;
					var trackingResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<TrackingURLResponse>(trackingResponseString);
					if (trackingResponse.response.status == 1) {
						trackingUrl = trackingResponse.response.data.click_url;
					}
				}
				catch (Exception ex) {
					trackingUrl = string.Format("Traking URL request refused by provider.\n{0}", ex.Message);
					throw;
				}
				return trackingUrl;
			}
			public override string FetchCountries(string offerId, string affiliatedId) {
				var client = new RestClient("https://api.hasoffers.com/Apiv3/");
				var request = new RestRequest("json");
				request.AddQueryParameter("NetworkId", _networkId);
				request.AddQueryParameter("Target", "Affiliate_Offer");
				request.AddQueryParameter("Method", "getTargetCountries");
				request.AddQueryParameter(Uri.EscapeUriString("ids[]"), offerId);
				request.AddQueryParameter("api_key", _key);
				var countries = base.FetchCountries(offerId, affiliatedId);
				try {
					var countriesResponseString = client.Execute<CountryObject>(request).Content;
					var countryResponse = JsonConvert.DeserializeObject<CountryObject>(countriesResponseString);
					if (countryResponse.response.status == 1) {
						var cs = new List<string>();
						foreach (var ioc in countryResponse.response.data[0].countries) {
							cs.Add(ioc.Key);
						}
						countries = string.Join("|", cs.ToArray());
					}
				}
				catch (Exception ex) {
					countries = string.Empty;
				}
				return countries;
			}
			public override CreativitiesResult FetchCreativities(Argument action) {
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				var content = string.Empty;
				try {
					content = client.Execute<APICreativity.RootObject>(request).Content;
				}
				catch (Exception) {
					var resp = new APICreativity.RootObject() {
						response = new APICreativity.Response() {
							httpStatus = 500
						}
					};
					content = JsonConvert.SerializeObject(resp, Formatting.Indented);
				}
				APICreativity.RootObject r;
				try {
					r = JsonConvert.DeserializeObject<APICreativity.RootObject>(content);
				}
				catch (Exception) {
					r = new APICreativity.RootObject() {
						response = new APICreativity.Response() { 
							httpStatus = 500,
							status = -1,
							errorMessage = "no creativities found",
							data = new APICreativity.Data() {
								data = new Dictionary<int, APICreativity.OfferFileContainer>()
							}
						},
					};
				}

				var creativitiesResult = new CreativitiesResult();

				var results = new List<APICreativity.Creativity>();
				if (r.response != null && r.response.status == 1 && r.response.data.data != null) {
					creativitiesResult.Results = results;
					creativitiesResult.Status = r.response.status;
					foreach (var item in r.response.data.data) {
						results.Add(new APICreativity.Creativity() {
							Url = item.Value.OfferFile.url,
							Name = item.Value.OfferFile.filename
						});
					}
				}else {
					creativitiesResult.Status = r.response.status;
					creativitiesResult.ErrorMessage = r.response.errorMessage;
				}
				return creativitiesResult;
			}
			public override bool RequestOfferAccess(Argument action) {
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				var content = string.Empty;
				try {
					content = client.Execute<APICreativity.RootObject>(request).Content;
				}
				catch (Exception) {
					var resp = new APICreativity.RootObject() {
						response = new APICreativity.Response() {
							httpStatus = 500
						}
					};
					content = JsonConvert.SerializeObject(resp, Formatting.Indented);
				}
				var r = JsonConvert.DeserializeObject<APICreativity.RootObject>(content);
				return r.response != null && r.response.status == 1;
			}

			public class TrackingURLResponse {
				public class Response {
					public int status { get; set; }
					public int httpStatus { get; set; }
					public Data data { get; set; }
					public class Data {
						public string click_url { get; set; }
						public string impression_pixel { get; set; }
					}
				}
				public Response response { get; set; }
			}
			public class CountryData {
				public string id { get; set; }
				public string code { get; set; }
				public string name { get; set; }

			}
			[JsonObject]
			public class Datum {
				public string offer_id { get; set; }
				public Dictionary<string, CountryData> countries { get; set; }
			}
			public class Response {
				public int status { get; set; }
				public int httpStatus { get; set; }
				public Datum[] data { get; set; }

			}
			public class CountryObject {
				public Response response { get; set; }
			}
		}
	}

	namespace APICreativity {
		public class Filters {
			public string offer_id { get; set; }
		}
		public class Request {
			public string Target { get; set; }
			public string Format { get; set; }
			public string Service { get; set; }
			public string Version { get; set; }
			public string api_key { get; set; }
			public string Method { get; set; }
			public Filters filters { get; set; }
			public string _ { get; set; }
		}
		public class OfferFile {
			public string id { get; set; }
			public string offer_id { get; set; }
			public string display { get; set; }
			public string filename { get; set; }
			public string size { get; set; }
			public string status { get; set; }
			public string type { get; set; }
			public string width { get; set; }
			public string height { get; set; }
			public string code { get; set; }
			public object flash_vars { get; set; }
			public string @interface { get; set; }
			public string account_id { get; set; }
			public string is_private { get; set; }
			public string created { get; set; }
			public string modified { get; set; }
			public string url { get; set; }
			public string thumbnail { get; set; }
		}
		public class Data {
			public Data() {
				data = new Dictionary<int, OfferFileContainer>();
			}
			public int page { get; set; }
			public int current { get; set; }
			public int count { get; set; }
			public int pageCount { get; set; }

			
			public Dictionary<int, OfferFileContainer> data { get; set; }
		}
		
		public class OfferFileContainer { 
			public OfferFile OfferFile { get; set; }
		}
		public class Response {
			public Response() {
				errors = new List<object>();
			}
			public int status { get; set; }
			public int httpStatus { get; set; }
			public Data data { get; set; }
			public List<object> errors { get; set; }
			public string errorMessage { get; set; }
		}
		public class RootObject {
			public Request request { get; set; }
			public Response response { get; set; }
		}
		public class Creativity { 
			public string Url{ get; set; }
			public string Name{ get; set; }
		}
	}
}
