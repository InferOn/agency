﻿using RestSharp;
using System.Collections.Generic;
using AgencyLib.Providers.Actions;
using AgencyLib.Providers.AffiliateNetworks;
using AgencyLib.Providers.Resolvers.Targets.AffiliateNetworks;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		public class AppPromotersAPIResolver : APIResolver {
			private string _baseUrl;
			public AppPromotersAPIResolver(string baseUrl) {
				_baseUrl = baseUrl;

			}
			public override APIResponse FindAll(Actions.Argument action) {
				//http://activeoffers.appromoters.com/v2/active_offers?publisher_id=116&publisher_token=Z9D3E2luYQK_xtaFEmw5uA
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				var content = string.Empty;
				var tempContent = string.Empty;
				var resp = new APIResponse() {
					response = new APIResponse.Response() {
						httpStatus = 200,
					}
				};
				try {
					tempContent = client.Execute<APIResponse.Response>(request).Content;
					var result = Newtonsoft.Json.JsonConvert.DeserializeObject<AppPromotersRootObject>(tempContent);
					resp.response = new APIResponse.Response() {
						data = new Dictionary<string, APIResponse.OfferContainer>()
					};
					foreach (var item in result.activeoffers) {
						resp.response.data.Add(item.offer_id.ToString(), new APIResponse.OfferContainer() { Offer = item.ToOffer() });
					}
				}
				catch (System.Exception ex) {
					// UNDONE: manage here the api request fails
					//throw;
					resp = new APIResponse() {
						response = new APIResponse.Response() {
							httpStatus = 500
						}
					};
				}
				return resp;
			}

			public override APIResponse Find(Argument args) {
				return new APIResponse() {
					response = new APIResponse.Response() {
						httpStatus = 200,
						status = 1,
						data = new Dictionary<string, APIResponse.OfferContainer>(),
					}
				};
			}

			public override CreativitiesResult FetchCreativities(Argument action) {
				return base.FetchCreativities(action);
			}
		}
	}
}

public class AppPromotersActiveoffer {
	public int offer_id { get; set; }
	public string name { get; set; }
	public string offer_name { get; set; }
	public string description { get; set; }
	public string status { get; set; }
	public object expiration_date { get; set; }
	public string offer_exclusivity { get; set; }
	public string conversion_tracking { get; set; }
	public string conversion_type { get; set; }
	public string currency { get; set; }
	public string payout { get; set; }
	public int payout_cap { get; set; }
	public int monthly_payout_cap { get; set; }
	public int conversion_cap { get; set; }
	public object monthly_conversion_cap { get; set; }
	public double rating { get; set; }
	public string app_os { get; set; }



	public string app_icon { get; set; }
	public string preview_url { get; set; }
	public string app_price { get; set; }
	public string app_short_description { get; set; }
	public string app_rating { get; set; }
	public string app_category { get; set; }
	public string validated { get; set; }
	public string packagename { get; set; }
	public string device_type { get; set; }
	public string incent { get; set; }
	public string country { get; set; }
	public string click_url { get; set; }

	internal APIResponse.Offer ToOffer() {
		return new APIResponse.Offer() {
			offer_id = offer_id.ToString(),
			description = description,
			appIconUrl = app_icon,
			preview_url = preview_url,
			name = offer_name,
			default_payout = payout,
			countries = country,
			category = app_category,
			payout_cap = payout_cap.ToString() ?? string.Empty,
			monthly_payout_cap = monthly_payout_cap.ToString(),
			monthly_conversion_cap = monthly_conversion_cap != null ? monthly_conversion_cap.ToString() : string.Empty,
			conversion_cap = conversion_cap.ToString() ?? string.Empty,
			TrackingURL = click_url ?? string.Empty,
			currency = currency,
			platform = app_os
			//expiration_date = this.expiration_date
		};
	}
}

public class AppPromotersRootObject {
	public List<AppPromotersActiveoffer> activeoffers { get; set; }
}
