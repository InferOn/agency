﻿using System;
using System.Net.Http;
using AgencyLib.Providers.Campains;
using System.Collections.Generic;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		public class StartAppAPIResolver : TrafficSources.APIResolver {
			private string _baseUrl, _token, _partner;
			public StartAppAPIResolver(string baseUrl, string token, string partner) {
				_baseUrl = baseUrl;
				_token = token;
				_partner = partner;
			}
			
			public override APIResponse FindAll(Actions.Argument action) {
				var resp = new APIResponse();
				try {
					using (var httpClient = new HttpClient()) {
						var sb = new System.Text.StringBuilder(_baseUrl);
						sb.Append(action.Resource + "?");
						var count = 0;
						foreach (var param in action.Params) {
							count++;
							sb.AppendFormat("{0}={1}", param.Key, param.Value);
							if (count < action.Params.Count) {
								sb.Append("&");
							}
						}
						HttpResponseMessage response = null;
						var page_index = 1;
						var page_total = 0;
						do {
							var sbstring = sb.ToString();
							response = httpClient.GetAsync(sbstring + string.Format("&pagesize=100&page={0}", page_index)).Result;
							if (response.IsSuccessStatusCode) {
								resp.httpStatus = (int)response.StatusCode;
								resp.status = 1;
								var responseContent = response.Content;
								var asyncResponseContent = responseContent.ReadAsStringAsync().Result;
								var results = Newtonsoft
															.Json
															.JsonConvert
															.DeserializeObject<IEnumerable<Campain>>
															(asyncResponseContent);
								//page_total = result.page_total;
								//page_index = ++result.page_index;

								resp.data = resp.data ?? new Dictionary<string, Campain>();

								foreach (var item in results) {
									if(!resp.data.ContainsKey(item.campaignId.ToString())) {
										resp.data.Add(item.campaignId.ToString(),item);
									}
								}
								//foreach (var item in results) {
								//	if (!resp.response.data.ContainsKey(item.campaignId.ToString())) {
								//		//resp.response.data.Add(item.campaignId.ToString(),);
								//	}
									//	try 
									//	{
									//		if (!resp.response.data.ContainsKey(item.oid.ToString())) {
									//			resp.response.data.Add(item.oid.ToString(), new APIResponse.OfferContainer() { Offer = item.toOffer() });
									//		}
									//	}
									//	catch (System.Exception exx)
									//	{
									//	}
									//}

								//}
							}
						} while (page_index <= page_total);
					}
				}
				catch (Exception ex) {
					// UNDONE: manage here the api request fails
					//throw;
					resp = new APIResponse() {
						httpStatus = 500,
					};
				}
				return resp;
			}
		}
	}
}
