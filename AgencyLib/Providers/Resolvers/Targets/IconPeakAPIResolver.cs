﻿using System;
using System.Collections.Generic;
using AgencyLib.Providers.Actions;
using RestSharp;
using AgencyLib.Providers.AffiliateNetworks;
using AgencyLib.Providers.Resolvers.Targets.AffiliateNetworks;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		public class IconPeakAPIResolver : APIResolver {
			private Dictionary<string, string> args;
			private string resource;
			public IconPeakAPIResolver(string apiURL, string resource, Dictionary<string, string> args) {
				this.apiURL = apiURL;
				Resource = resource;
				Args = args;
			}
			public Dictionary<string, string> Args {
				get {
					return args;
				}

				set {
					args = value;
				}
			}
			public string Resource {
				get {
					return resource;
				}

				set {
					resource = value;
				}
			}
			public override APIResponse FindAll(Argument action) {
				var client = new RestClient(action.BaseURL);
				var request = new RestRequest(action.Resource);
				foreach (var param in action.Params) {
					request.AddQueryParameter(param.Key, param.Value);
				}
				var content = string.Empty;
				try {
					content = client.Execute<object>(request).Content;
				}
				catch (Exception) {
					var resp = new APIResponse() {
						response = new APIResponse.Response() {
							httpStatus = 500
						}
					};
					content = Newtonsoft.Json.JsonConvert.SerializeObject(resp, Newtonsoft.Json.Formatting.Indented);
				}
				return Newtonsoft.Json.JsonConvert.DeserializeObject<APIResponse>(content);
			}
			public override APIResponse Find(Argument args) {
				throw new NotImplementedException();
			}

			public override CreativitiesResult FetchCreativities(Argument action) {
				return base.FetchCreativities(action);
			}
		}
	}
}
