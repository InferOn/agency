﻿using System.Collections.Generic;
using AgencyLib.Providers.Resolvers.APICreativity;

namespace AgencyLib.Providers.Resolvers {
	public class CreativitiesResult {
		public CreativitiesResult() {
			Results = new List<Creativity>();
		}
		public string ErrorMessage { get; set; }
		public List<Creativity> Results { get; set; }
		public int Status { get; set; }
	}
}