﻿using AgencyLib.Providers.Actions;
using AgencyLib.Providers.Campains;

namespace AgencyLib.Providers.Resolvers {
	namespace Targets {
		namespace AffiliateNetworks {
			public abstract class APIResolver : Providers.AffiliateNetworks.IApiResolver {
				public string apiURL { get; set; }
				public string key { get; set; }
				public string networkId { get; set; }
				public abstract Providers.AffiliateNetworks.APIResponse FindAll(Actions.Argument action);
				public abstract Providers.AffiliateNetworks.APIResponse Find(Argument args);
				public virtual string FetchTrackingURL(string offerId, string affiliatedId) {
					return null;
				}
				public virtual string FetchCountries(string offerId, string affiliatedId) {
					return string.Empty;
				}
				public virtual CreativitiesResult FetchCreativities(Argument action) {
					return new CreativitiesResult();
				}
				public virtual bool RequestOfferAccess(Argument argument) {
					return false;
				}
			}
		}
		namespace TrafficSources {
			public abstract class APIResolver : Providers.TrafficSources.IApiResolver {
				public string apiURL { get; set; }
				public string key { get; set; }
				public string networkId { get; set; }
				public abstract APIResponse FindAll(Argument args);
			}
		}
	}
}
