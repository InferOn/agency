﻿using System;
using System.Collections.Generic;

namespace AgencyLib.Providers {
	namespace AffiliateNetworks {
		public class APIResponse {
			public class Request {
				public string Target { get; set; }
				public string Format { get; set; }
				public string Service { get; set; }
				public string Version { get; set; }
				public string Method { get; set; }
				public string api_key { get; set; }
				public string NetworkId { get; set; }
			}
			public class OfferContainer {
				public Offer Offer { get; set; }
			}
			public class RootObject {
				public List<Offer> activeoffers { get; set; }
			}
			public class ClinkRootObject {
				public int page_total { get; set; }
				public int page_index { get; set; }
				public int offer_total { get; set; }
				public int offer_count { get; set; }
				public string result { get; set; }
				public List<ClinkOffer> offers { get; set; }
			}
			public class ClinkOffer {
				public int oid { get; set; }
				public string name { get; set; }
				public string desc { get; set; }
				public decimal payout { get; set; }
				public string tracking_link { get; set; }
				public string countries { get; set; }
				public string category { get; set; }
				public string CreativeUrl { get; set; }
				public string PreviewUrl { get; set; }
				public string appIconUrl { get; set; }
				public string PackageName { get; set; }
				public string platform { get; set; }
				public string market { get; set; }
				public bool RequiredGAID { get; set; }
				public string minOSVersion { get; set; }
				public string appSize { get; set; }
				public string appSizeInBytes { get; set; }
				private string getPlatform() {
					var result = "iOS";
					result = !string.IsNullOrEmpty(this.platform) ? this.platform.ToLowerInvariant() == "android" ? "android" : result : result;
					return result;
				}
				public Offer toOffer() {
					return new Offer() {
						offer_id = this.oid.ToString(),
						//this.appSize
						appIconUrl = this.appIconUrl,
						countries = countries,
						platform = getPlatform(),
						//this.appSizeInBytes
						//this.category 
						//this.countries
						//this.CreativeUrl
						description = this.desc,
						//this.market
						//this.minOSVersion
						name = this.name,
						//this.PackageName
						default_payout = this.payout.ToString(),
						//this.platform
						preview_url = this.PreviewUrl,
						TrackingURL = this.tracking_link,
						CreativeURL = this.CreativeUrl,
						category = this.category,
						//expiration_date = null,
						//this.RequiredGAID
						//
					};
				}
			}
			public class Offer {
				public string category { get; set; }
				public string id { get; set; }
				public string offer_id { get; set; }
				public string appIconUrl { get; set; }
				public string countries { get; set; }
				public string name { get; set; }
				public string description { get; set; }
				public string require_approval { get; set; }
				public string require_terms_and_conditions { get; set; }
				public string terms_and_conditions { get; set; }
				public string preview_url { get; set; }
				private string _platform;
				public string platform {
					get {
						if (_platform == null && !string.IsNullOrEmpty(preview_url)) {
							return preview_url.IndexOf("play.google.com") >= 0 ? "android" : "iOS";
						}
						return _platform;
					}
					set {
						_platform = value;
					}
				}
				public string currency { get; set; }
				public string default_payout { get; set; }
				public string status { get; set; }
				public DateTime? expiration_date { get; set; }
				public string payout_type { get; set; }
				public string percent_payout { get; set; }
				public string featured { get; set; }
				public string conversion_cap { get; set; }
				public string monthly_conversion_cap { get; set; }
				public string payout_cap { get; set; }
				public string monthly_payout_cap { get; set; }
				public string allow_website_links { get; set; }
				public string allow_direct_links { get; set; }
				public string show_custom_variables { get; set; }
				public string show_mail_list { get; set; }
				public string dne_list_id { get; set; }
				public string email_instructions { get; set; }
				public string email_instructions_from { get; set; }
				public string email_instructions_subject { get; set; }
				public string has_goals_enabled { get; set; }
				public string default_goal_name { get; set; }
				public string use_target_rules { get; set; }
				public string is_expired { get; set; }
				public string dne_download_url { get; set; }
				public string dne_unsubscribe_url { get; set; }
				public string dne_third_party_list { get; set; }
				public string approval_status { get; set; }
				public string TrackingURL { get; set; }
				public string CreativeURL { get; set; }
			}
			public class Response {
				public int status { get; set; }
				public int httpStatus { get; set; }
				public Dictionary<string, OfferContainer> data { get; set; }
			}
			public Request request { get; set; }
			public Response response { get; set; }
		}

	}

	namespace Campains {
		public class APIResponse {
			public int status { get; set; }
			public int httpStatus { get; set; }
			public Dictionary<string, Campain> data { get; set; }
		}

		public class Image {
			public string url { get; set; }
			public int width { get; set; }
			public int height { get; set; }
			public string type { get; set; }
		}
		public class CreativeList {
			public string url { get; set; }
			public string name { get; set; }
			public string creativeId { get; set; }
			public string status { get; set; }
			public List<Image> images { get; set; }
			public string createdDate { get; set; }
			public string creativeType { get; set; }
		}
		public class Budget {
			public decimal? bidPrice { get; set; }
			public string pricingModel { get; set; }
			public decimal? dailyBudget { get; set; }
			public decimal? totalBudget { get; set; }
		}
		public class LocationContext {
			public bool academiaEnabled { get; set; }
			public bool workEnabled { get; set; }
			public bool homeEnabled { get; set; }
			public bool enabled { get; set; }
		}
		public class Interests {
			public List<string> value { get; set; }
			public bool include { get; set; }
			public bool enabled { get; set; }
		}
		public class AudienceGroup {
			public List<string> value { get; set; }
			public bool include { get; set; }
			public bool enabled { get; set; }
		}
		public class Gender {
			public bool enable { get; set; }
		}
		public class Audience {
			public bool targetAdvertiserId { get; set; }
			public LocationContext locationContext { get; set; }
			public Interests interests { get; set; }
			public AudienceGroup audienceGroup { get; set; }
			public Gender gender { get; set; }
		}
		public class Placements {
			public List<string> adTypes { get; set; }
		}
		public class DevId {
			public List<string> value { get; set; }
			public bool enabled { get; set; }
		}
		public class AppId {
			public List<string> value { get; set; }
			public bool enabled { get; set; }
		}
		public class Exclude {
			public string condition { get; set; }
			public List<string> value { get; set; }
			public bool enabled { get; set; }
		}
		public class Include {
			public string condition { get; set; }
			public List<string> value { get; set; }
			public bool enabled { get; set; }
		}
		public class AppPresence {
			public Exclude exclude { get; set; }
			public Include include { get; set; }
			public bool enabled { get; set; }
		}
		public class Optimization {
			public DevId devId { get; set; }
			public AppId appId { get; set; }
			public AppPresence appPresence { get; set; }
			public List<string> category { get; set; }
		}
		public class ManufacturerDevice {
			public string manufacturerName { get; set; }
			public string _id { get; set; }
			public List<object> devices { get; set; }
		}
		public class ManufacturerDevices {
			public bool exclude { get; set; }
			public List<ManufacturerDevice> value { get; set; }
		}
		public class Max {
			public string versionName { get; set; }
		}
		public class Min {
			public string versionName { get; set; }
		}
		public class OsVersion {
			public Max max { get; set; }
			public Min min { get; set; }
		}
		public class Device {
			public ManufacturerDevices manufacturerDevices { get; set; }
			public string deviceType { get; set; }
			public OsVersion osVersion { get; set; }
			public string connectionType { get; set; }
		}
		public class Isp {
			public int ispKey { get; set; }
			public string ispName { get; set; }
			public string countryKey { get; set; }
			public string _id { get; set; }
		}
		public class Isps {
			public bool exclude { get; set; }
			public bool enableFilter { get; set; }
			public List<Isp> value { get; set; }
		}
		public class Cities {
			public bool exclude { get; set; }
			public bool enableFilter { get; set; }
			public List<object> value { get; set; }
		}
		public class Value {
			public string countryName { get; set; }
			public string countryKey { get; set; }
		}
		public class Countries {
			public bool exclude { get; set; }
			public bool enableFilter { get; set; }
			public List<Value> value { get; set; }
		}
		public class Geo {
			public Isps isps { get; set; }
			public Cities cities { get; set; }
			public Countries countries { get; set; }
		}
		public class Targeting {
			public Audience audience { get; set; }
			public Placements placements { get; set; }
			public Optimization optimization { get; set; }
			public Device device { get; set; }
			public Geo geo { get; set; }
		}
		public class CampaignInfo {
			public string name { get; set; }
			public object endDate { get; set; }
			public List<string> dayParting { get; set; }
			public bool hasEndTime { get; set; }
			public bool hasStartTime { get; set; }
			public string startDate { get; set; }
			public string updateDate { get; set; }
			public string createdDate { get; set; }
		}
		public class CampaignType {
			public string destinationUrl { get; set; }
			public string platform { get; set; }
			public string appName { get; set; }
			public string category { get; set; }
		}
		public class Campain {
			public string campaignId { get; set; }
			public string userId { get; set; }
			public string status { get; set; }
			public List<CreativeList> creativeList { get; set; }
			public Budget budget { get; set; }
			public Targeting targeting { get; set; }
			public CampaignInfo campaignInfo { get; set; }
			public CampaignType campaignType { get; set; }
		}
	}
}
