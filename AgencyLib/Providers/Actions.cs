﻿using System.Collections.Generic;

namespace AgencyLib.Providers {

	namespace Actions {
		public sealed class Argument {
			public Dictionary<string, string> Params { get; internal set; }
			public string Resource { get; internal set; }
			public string BaseURL { get; internal set; }
		}
		
	}
}
