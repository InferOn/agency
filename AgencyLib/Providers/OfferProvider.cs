﻿using AgencyLib.Data;
using AgencyLib.Providers.Actions;
using AgencyLib.Providers.AffiliateNetworks;
using AgencyLib.Providers.Resolvers;
using AgencyLib.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace AgencyLib.Providers {
	public sealed class OfferProvider {
		public sealed class FetchSummary {
			public Dictionary<string, IEnumerable<Diff>> ChangesInfo { get; internal set; }
			public Dictionary<string, Tuple<string, string>> DeleteInfo { get; internal set; }
			public Dictionary<string, Tuple<string, string>> InsertInfo { get; internal set; }
			public void NetworkChangesInfo(List<APIResponse.Offer> remoteData, IEnumerable<Offer> inPlace) {
				try {
					foreach (var p in inPlace) {
						var _r = remoteData.Where(x => (x.id ?? x.offer_id) == p.OfferId.ToString()).SingleOrDefault();
						if (_r != null) {
							var remote = _r.ToDataOffer(p.NetworkId);
							IEnumerable<Diff> diffs = p.Diff(remote);
							if (diffs.Any()) {
								if (ChangesInfo == null)
									ChangesInfo = new Dictionary<string, IEnumerable<Diff>>();
								ChangesInfo.Add(string.Format("{0} - {1}", p.OfferId.ToString(), p.Name), diffs);
							}
						}
					}
				}
				catch (Exception ex) {
					throw;
				}
			}
			public void NetworkInsertInfo(IEnumerable<APIResponse.Offer> remoteStillNotInPlace) {
				InsertInfo = new Dictionary<string, Tuple<string, string>>();
				foreach (var itemToInsert in remoteStillNotInPlace) {
					InsertInfo.Add(itemToInsert.offer_id ?? itemToInsert.id, new Tuple<string, string>(itemToInsert.offer_id ?? itemToInsert.id, string.Format("{0} - {1}", itemToInsert.offer_id ?? itemToInsert.id, itemToInsert.name)));
				}
			}
			public void NetworkDeleteInfo(List<Offer> noMoreInPlace) {
				DeleteInfo = new Dictionary<string, Tuple<string, string>>();
				foreach (var itemToDelete in noMoreInPlace) {
					DeleteInfo.Add(itemToDelete.OfferId.ToString(), new Tuple<string, string>(itemToDelete.OfferId.ToString(), string.Format("{0} - {1}", itemToDelete.OfferId, itemToDelete.Name)));
				}
			}
		}
		private const string PENDING = "pending";
		private const string REJECTED = "rejected";
		private OfferProvider() {
		}
		public void FetchDetailFrom(AffiliateNetwork network, int offerId, IAgencyDataContext context) {
			this.context = context;
			var setting = Newtonsoft.Json.JsonConvert.DeserializeObject<AffiliateNetwork.AccountSetting>(network.AccountSettings);
			var apiresolver = ApiResolverFactory.Find(setting);
			var apiResponse = apiresolver.Find(CreateDetailArg(setting, offerId));
		}
		public FetchSummary FetchFrom(AffiliateNetwork network, IAgencyDataContext context) {
			var fSummary = new FetchSummary();
			this.context = context;
			var setting = Newtonsoft.Json.JsonConvert.DeserializeObject<AffiliateNetwork.AccountSetting>(network.AccountSettings);
			var apiresolver = ApiResolverFactory.Find(setting);
			var apiResponse = apiresolver.FindAll(CreateArg(setting));
			if (apiResponse.response.status != -1 && apiResponse.response.data != null) {
				var remoteData = apiResponse.response.data.Select(x => x.Value.Offer);
				EnrichRemoteData(remoteData, apiresolver, setting);
				var inPlace = context.Offers.Where(x => x.NetworkId == network.Id).ToList();
				var remoteAlreadyInPlace = remoteData.Where(x => inPlace.Select(y => y.OfferId.ToString()).Contains(x.id ?? x.offer_id));
				fSummary.NetworkChangesInfo(remoteAlreadyInPlace.ToList(), inPlace);
				Update(remoteAlreadyInPlace.Select(x => x).ToList(), inPlace, context, network.Id);
				var remoteStillNotInPlace = remoteData.Except(remoteAlreadyInPlace);
				fSummary.NetworkInsertInfo(remoteStillNotInPlace);
				Insert(network.Id, remoteStillNotInPlace);
				var noMoreInPlace = inPlace.Where(x => !remoteData.Select(y => y.id ?? y.offer_id).Contains(x.OfferId.ToString())).ToList();
				fSummary.NetworkDeleteInfo(noMoreInPlace);
				Delete(noMoreInPlace);
				network.LastGet = DateTime.Now;
				context.SaveChanges();
			}
			else {
				//trace
				//log
				//notify
			}
			return fSummary;
		}
		private static Argument CreateArg(AffiliateNetwork.AccountSetting setting) {
			var action = new Argument() { BaseURL = setting.apiURL, Resource = setting.resource, Params = new Dictionary<string, string>() };
			foreach (var kv in setting.args)
				action.Params.Add(kv.Key, kv.Value);
			return action;
		}
		private static Argument CreateDetailArg(AffiliateNetwork.AccountSetting setting, int detailId) {
			var action = new Argument() { BaseURL = setting.apiURL, Resource = setting.resource, Params = new Dictionary<string, string>() };
			foreach (var kv in setting.detailArgs ?? new Dictionary<string, string>())
				action.Params.Add(kv.Key, kv.Value);

			action.Params.Add("id", detailId.ToString());
			return action;
		}
		private static Argument CreateCreativitiesArg(AffiliateNetwork.AccountSetting setting, int detailId) {
			Argument args = null;
			if (setting.creativitiesArgs != null) {
				args = new Argument() { BaseURL = setting.apiURL, Resource = setting.creativitiesArgs["resource"], Params = new Dictionary<string, string>() };
				foreach (var kv in setting.creativitiesArgs ?? new Dictionary<string, string>()) {
					if (kv.Key != "resource")
						args.Params.Add(kv.Key, kv.Value);
				}
				args.Params.Add("filters[offer_id]", detailId.ToString());
			}
			return args;
		}
		private static Argument CreateRequestAccessArg(AffiliateNetwork.AccountSetting setting, int detailId) {
			Argument args = null;
			if (setting.requestAccessArgs != null) {
				args = new Argument() { BaseURL = setting.apiURL, Resource = setting.requestAccessArgs["resource"], Params = new Dictionary<string, string>() };
				foreach (var kv in setting.requestAccessArgs ?? new Dictionary<string, string>()) {
					if (kv.Key != "resource")
						args.Params.Add(kv.Key, kv.Value);
				}
				args.Params.Add("offer_id", detailId.ToString());
			}
			return args;
		}
		private void EnrichRemoteData(IEnumerable<APIResponse.Offer> remoteData, IApiResolver apiresolver, AffiliateNetwork.AccountSetting settings) {
			if (settings.generateTrackingURL) {
				foreach (var remote in remoteData) {
					remote.TrackingURL = apiresolver.FetchTrackingURL(remote.offer_id ?? remote.id, settings.affiliatedId);
					Thread.Sleep(100);
				}
			}
			if (settings.fetchCountries) {
				foreach (var remote in remoteData) {
					remote.countries = apiresolver.FetchCountries(remote.offer_id ?? remote.id, settings.affiliatedId);
					Thread.Sleep(100);
				}
			}
		}
		private void Insert(int networkId, IEnumerable<APIResponse.Offer> items) {
			foreach (var item in items) {
				//SMELL => use automapper!!!
				var remoteOffer = item;
				context.Offers.Add(new Offer() {
					NetworkId = networkId,
					OfferId = int.Parse(remoteOffer.id ?? remoteOffer.offer_id),
					Name = remoteOffer.name,
					AppIconUrl = remoteOffer.appIconUrl,
					PreviewUrl = remoteOffer.preview_url,
					Description = remoteOffer.description,
					Countries = remoteOffer.countries,
					DateExpiration = remoteOffer.expiration_date,
					Currency = remoteOffer.currency,
					ConversionType = remoteOffer.payout_type,
					Status = remoteOffer.status,
					#region app promoters only
					//AppRating = NULL,
					AppOS = remoteOffer.platform,
					//AppPrice = NULL,
					//AppID = NULL,
					//AppDescription = NULL,
					//AppCategories = NULL,
					//TrackingId = null,
					#endregion
					#region hasoffers only
					require_approval = remoteOffer.require_approval,
					require_terms_and_conditions = remoteOffer.require_terms_and_conditions,
					terms_and_conditions = remoteOffer.terms_and_conditions,
					preview_url = remoteOffer.preview_url,
					default_payout = remoteOffer.default_payout.Replace(",", "."),
					percent_payout = remoteOffer.percent_payout,
					featured = remoteOffer.featured,
					allow_website_links = remoteOffer.allow_website_links,
					allow_direct_links = remoteOffer.allow_direct_links,
					show_custom_variables = remoteOffer.show_custom_variables,
					show_mail_list = remoteOffer.show_mail_list,
					dne_list_id = remoteOffer.dne_list_id,
					email_instructions = remoteOffer.email_instructions,
					email_instructions_from = remoteOffer.email_instructions_from,
					email_instructions_subject = remoteOffer.email_instructions_subject,
					has_goals_enabled = remoteOffer.has_goals_enabled,
					default_goal_name = remoteOffer.default_goal_name,
					use_target_rules = remoteOffer.use_target_rules,
					is_expired = remoteOffer.is_expired,
					dne_download_url = remoteOffer.dne_download_url,
					dne_unsubscribe_url = remoteOffer.dne_unsubscribe_url,
					dne_third_party_list = remoteOffer.dne_third_party_list,
					approval_status = remoteOffer.approval_status,

					#endregion
					StatusValidation = eStatusValidation.Active,
					TrackingURL = remoteOffer.TrackingURL,
					#region clink ad only
					CreativeUrl = remoteOffer.CreativeURL,
					#endregion
				});
			}
		}
		private void Update(List<APIResponse.Offer> remoteAlreadyInPlace, IEnumerable<Offer> inPlace, IAgencyDataContext context, int networkId) {
			var shouldSave = false;
			var _inPlace = inPlace;
			var subscribed = context.OffersSubscribed.Where(x => x.NetworkId == networkId);
			foreach (var p in _inPlace) {
				var _o = remoteAlreadyInPlace.Where(x => (x.id ?? x.offer_id) == p.OfferId.ToString()).SingleOrDefault();
				if (_o != null) {
					var remote = _o.ToDataOffer(p.NetworkId);
					var diffs = p.Diff(remote);
					if (null != diffs) {
						shouldSave = diffs.Any();
						if (shouldSave) {
							if (subscribed.Any()) {
								var si = subscribed.Where(x => x.OfferId == p.OfferId).SingleOrDefault();
								if (si != null) {
									updateOfferSubscribed(si, diffs);
								}
							}
							updateOffer(p, diffs);
						}
					}
				}
			}
			if (shouldSave) {
				context.SaveChanges();
			}
		}
		private void Delete(IEnumerable<Offer> noMoreInPlace) {
			var noMoreInPlaceIds = noMoreInPlace.Select(y => y.OfferId);
			var subscribedToDelete = context.OffersSubscribed
																.Where(x => noMoreInPlaceIds.Contains(x.OfferId));
			context.Offers.RemoveRange(noMoreInPlace);
			context.OffersSubscribed.RemoveRange(subscribedToDelete);
			if (noMoreInPlace.Any()) {
				context.SaveChanges();
			}
		}
		private void updateOfferSubscribed(OfferSubscribed trg, IEnumerable<Diff> diffs) {
			//#region conditions
			//subscribed.AppOS = updated.AppOS;
			//subscribed.Countries = updated.Countries;
			//subscribed.allow_direct_links = updated.allow_direct_links;
			//subscribed.allow_website_links = updated.allow_website_links;
			//subscribed.approval_status = updated.approval_status;
			//subscribed.ConversionCap = updated.ConversionCap;
			//subscribed.Currency = updated.Currency;
			//subscribed.default_goal_name = updated.default_goal_name;
			//subscribed.default_payout = updated.default_payout;
			//subscribed.Description = updated.Description;
			//subscribed.dne_download_url = updated.dne_download_url;
			//subscribed.dne_list_id = updated.dne_list_id;
			//subscribed.dne_third_party_list = updated.dne_third_party_list;
			//subscribed.dne_unsubscribe_url = updated.dne_unsubscribe_url;
			//subscribed.email_instructions = updated.email_instructions;
			//subscribed.email_instructions_from = updated.email_instructions_from;
			//subscribed.email_instructions_subject = updated.email_instructions_subject;
			//subscribed.DateExpiration = updated.DateExpiration;
			//subscribed.featured = updated.featured;
			//subscribed.has_goals_enabled = updated.has_goals_enabled;
			//subscribed.is_expired = updated.is_expired;
			////	subscribed.monthly_conversion_cap = _l.monthly_conversion_cap;
			////	subscribed.monthly_payout_cap = _l.monthly_payout_cap;
			//subscribed.Name = updated.Name;
			//subscribed.PayoutCap = updated.PayoutCap;
			//subscribed.payout_type = updated.payout_type;
			//subscribed.percent_payout = updated.percent_payout;
			//subscribed.require_approval = updated.require_approval;
			//subscribed.require_terms_and_conditions = updated.require_terms_and_conditions;
			//subscribed.show_custom_variables = updated.show_custom_variables;
			//subscribed.show_mail_list = updated.show_mail_list;
			//subscribed.Status = updated.Status;
			//subscribed.terms_and_conditions = updated.terms_and_conditions;
			//subscribed.use_target_rules = updated.use_target_rules;
			//subscribed.TrackingURL = updated.TrackingURL;
			//subscribed.CreativeUrl = updated.CreativeUrl;

			//#endregion
			foreach (Diff d in diffs) {
				trg.GetType().InvokeMember(d.Prop,
					BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
					Type.DefaultBinder, trg, new[] { d.srcVal });
			}
		}
		private void updateOffer(Offer trg, IEnumerable<Diff> diffs) {
			foreach (Diff d in diffs) {
				trg.GetType().InvokeMember(d.Prop,
					BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
					Type.DefaultBinder, trg, new[] { d.srcVal });
			}
		}
		internal CreativitiesResult FetchCreativitiesFrom(AffiliateNetwork affiliateNetwork, int offerId, IAgencyDataContext _context) {
			this.context = _context;
			var setting = Newtonsoft.Json.JsonConvert.DeserializeObject<AffiliateNetwork.AccountSetting>(affiliateNetwork.AccountSettings);
			var apiresolver = ApiResolverFactory.Find(setting);
			CreativitiesResult apiResponse = new CreativitiesResult();
			var argument = CreateCreativitiesArg(setting, offerId);
			if (argument != null) {
				apiResponse = apiresolver.FetchCreativities(argument);
			}
			else {
				var det = _context.Offers.Where(x => x.OfferId == offerId && x.NetworkId == affiliateNetwork.Id).SingleOrDefault();
				apiResponse = new CreativitiesResult();

				if (det != null && det.CreativeUrl != null) {
					((List<Resolvers.APICreativity.Creativity>)apiResponse.Results)
						.Add(new Resolvers.APICreativity.Creativity() { Name = System.IO.Path.GetFileName(det.CreativeUrl), Url = det.CreativeUrl });

				}
			}
			return apiResponse;
		}
		internal bool RequestAccess(AffiliateNetwork affiliateNetwork, int offerId, IAgencyDataContext _context) {
			context = _context;
			var result = false;
			var offer = _context.Offers.Where(x => x.NetworkId == affiliateNetwork.Id && x.OfferId == offerId).SingleOrDefault();
			if (offer.approval_status != PENDING && offer.approval_status != REJECTED) {
				var setting = Newtonsoft.Json.JsonConvert.DeserializeObject<AffiliateNetwork.AccountSetting>(affiliateNetwork.AccountSettings);
				result = ApiResolverFactory.Find(setting).RequestOfferAccess(CreateRequestAccessArg(setting, offerId));
				offer.ApprovalRequested = true;
				context.SaveChanges();
			}
			return result;
		}
		public static OfferProvider Instance { get { return NestedProvider.instance; } }
		private class NestedProvider {
			static NestedProvider() {

			}
			internal static readonly OfferProvider instance = new OfferProvider();
		}
		private IAgencyDataContext context { get; set; }
	}

}
