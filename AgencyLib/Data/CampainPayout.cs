﻿using System;

namespace AgencyLib.Data {
	public class CampainPayout {
		public int Id { get; set; }
		public DateTime DateOpen { get; set; }
		public DateTime DateClose { get; set; }
		public int CampainId { get; set; }
		public decimal Payout { get; set; }
	}
}