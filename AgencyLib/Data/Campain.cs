﻿using System;
using System.Collections.Generic;

namespace AgencyLib.Data {
	public class CampainListFilter {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
		public int Id { get; set; }
		public decimal Bid { get; set; }
		public decimal DailyBudget { get; set; }
		public string PricingModel { get; set; }
		public string Platform { get; set; }
		public string Countries { get; set; }
		public string Statuses { get; set; }
		public System.DateTime StartedDateFrom { get; set; }
		public System.DateTime StartedDateTo { get; set; }
	}
	public class Campain {
		[DiffIgnore]
		public int Id { get; set; }
		public string Name { get; set; }
		public int campaignId { get; set; }
		public int trafficSourceId { get; set; }
		public string Status { get; set; }
		public decimal? bidPrice { get; set; }
		public decimal? dailyBudget { get; set; }
		public string pricingModel { get; set; }
		public decimal? totalBudget { get; set; }
		public DateTime createdDate { get; set; }
		public bool hasEndTime { get; set; }
		public bool hasStartTime { get; set; }
		public DateTime startDate { get; set; }
		public DateTime updateDate { get; set; }
		public string appName { get; set; }
		public string category { get; set; }
		public string destinationUrl { get; set; }
		public string platform { get; set; }
		public string countries { get; set; }
		[DiffIgnore]
		public TrafficSource TrafficSource { get; set; }

	}
}