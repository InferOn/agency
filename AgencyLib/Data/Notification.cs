﻿using AgencyLib.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgencyLib.Data.Notification {
	public abstract class Notification {
		public int Id { get; set; }
		public int ChannelType { get; set; }
		public int Status { get; set; }
		public string Header { get; set; }
		public string Body { get; set; }
		public DateTime? DateCreation { get; set; }
		public DateTime? DateSend { get; set; }
		public List<Recipient> Recipients { get; set; }
		public abstract class Recipient {
			public int Id { get; set; }
			public string Address { get; set; }
		}
	}
	public class MailNotification : Notification {
		public MailNotification() {
			ChannelType = (int)eChannelType.Email;
			Status = (int)eStatusNotification.ToBeSent;
			Recipients = new List<Recipient>();
		}
		public class MailRecipient : Recipient {


		}
	}
	public enum eChannelType {
		None = 0,
		Email = 1,
		Fax = 2
	}
	public enum eStatusNotification {
		ToBeSent = 0,
		Sent = 1,
		ErrorOnSent = 2,
	}
}
