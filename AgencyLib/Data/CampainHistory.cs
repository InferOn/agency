﻿using System;
using System.Collections.Generic;

namespace AgencyLib.Data {
	public class Personnel {
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
	public class CampainHistoryListFilter {
		public int Page { get; set; }
		public int Limit { get; set; }
	}
	public class CampainHistory { 
		public int Id { get; set; }
		public string Status { get; set; }
		public string NetworkName { get; set; }
		public string Name { get; set; }
		public string Os { get; set; }
		public string Geo { get; set; }
		public decimal? Payout { get; set; }
		public decimal? DailyCap { get; set; }
		public decimal? TotalCap { get; set; }
		public DateTime? StartDate { get; set; }
		public string DeliveredOn { get; set; }
		public string ManagedBy { get; set; }
		public string Kpi { get; set; }
		public string Notes { get; set; }
	}
}