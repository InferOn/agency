﻿using System;

namespace AgencyLib.Data {

	public class OfferPayout {
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public int OfferId { get; set; }
		public decimal Payout { get; set; }
	}
}