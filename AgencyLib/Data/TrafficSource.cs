﻿using System;
using System.Collections.Generic;

namespace AgencyLib.Data {
	public class TrafficSource { 
		public int Id { get; set; }
		public string Name { get; set; }
		public string AccountSettings { get; set; }
		public class AccountSetting {
			public string apiURL { get; set; }
			public string affiliatedId { get; set; }
			public string resource { get; set; }
			public string resolver { get; set; }
			public Dictionary<string, string> args { get; set; }
			public Dictionary<string, string> detailArgs { get; set; }
		}
		public int Active { get; set; }
		public DateTime LastGet { get; set; }
	}

	public class TrafficSourceListFilter {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }

		public string Status { get; set; }
		public string Sort { get; set; }
		public string SortDirection { get; set; }

		public int NetworkId { get; set; }
		public string Os { get; set; }
		public string Ioc { get; set; }
		public string Id { get; set; }
	}
}