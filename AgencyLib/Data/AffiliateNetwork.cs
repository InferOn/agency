﻿using System;
using System.Collections.Generic;

namespace AgencyLib.Data {
	public class AffiliateNetwork {
		public int Id { get; set; }
		public string Name { get; set; }
		public string AccountSettings { get; set; }
		public string TrackingId { get; set; }
		public DateTime? LastGet { get; set; }
		public int? Active { get; set; }
		public string Note { get; set; }
		public class AccountSetting {
			public string apiURL { get; set; }
			public string affiliatedId { get; set; }
			public string resource { get; set; }
			public string resolver { get; set; }
			public Dictionary<string, string> args { get; set; }
			public Dictionary<string, string> detailArgs { get; set; }
			public Dictionary<string, string> creativitiesArgs { get; set; }
			public Dictionary<string, string> requestAccessArgs { get; set; }
			public bool generateTrackingURL { get; set; }
			public bool fetchCountries { get; set; }
			
		}
	}

	public class AffiliateNetworkListFilter {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
		public int Active { get; set; }
	}
}