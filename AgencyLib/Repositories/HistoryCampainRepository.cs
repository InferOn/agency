﻿using AgencyLib.Data;
using Dapper;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace AgencyLib {
	public interface IHistoryCampainRepository {
		IEnumerable<CampainHistory> Get(CampainHistoryListFilter chlf);
		int Create(CampainHistory ch);
		int Update(CampainHistory ch);
		int Delete(int[] campaignIds);
	}
	public class HistoryCampainRepository : IHistoryCampainRepository {
		private IAgencyDataContext _context;
		private string _connectionString;
		public HistoryCampainRepository(string connectionString, IAgencyDataContext context) {
			_connectionString = connectionString;
			_context = context;
		}

		public IEnumerable<CampainHistory> Get(CampainHistoryListFilter chlf) {
			//IQueryable<CampainHistory> iq = _context.CampainHistory.AsQueryable();
			////var clause = PredicateBuilder.New<CampainHistory>();
			////iq = iq.Where(clause).AsExpandable<CampainHistory>();
			//return iq.ToList();
			var chs = _context.CampainHistories.ToList();
			return chs;
		}
		public int Update(CampainHistory ch) {
			var local = _context.CampainHistories.SingleOrDefault(x => x.Id == ch.Id);
			local.Geo = ch.Geo;
			local.DailyCap = ch.DailyCap;
			local.DeliveredOn = ch.DeliveredOn;
			local.Kpi = ch.Kpi;
			local.ManagedBy = ch.ManagedBy;
			local.NetworkName = ch.NetworkName;
			local.Name = ch.Name;
			local.Notes = ch.Notes;
			local.Os = ch.Os;
			local.Payout = ch.Payout;
			local.StartDate = ch.StartDate;
			local.Status = ch.Status;
			local.TotalCap = ch.TotalCap;
			return _context.SaveChanges();
		}
		public int Create(CampainHistory ch) {
			_context.CampainHistories.Add(ch);
			return _context.SaveChanges();
		}
		int IHistoryCampainRepository.Delete(int[] campaignIds) {
			var ch = _context.CampainHistories.Where(x => campaignIds.Contains(x.Id));
			if (ch.Any()) {
				foreach (var c in ch) {
					_context.CampainHistories.Remove(c);
				}
				return _context.SaveChanges();
			}
			return -1;
		}
	}
}
