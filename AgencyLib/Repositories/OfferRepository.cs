﻿using AgencyLib.Data;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using AgencyLib.Providers;
using System.Data;
using AgencyLib.Providers.Resolvers;
using LinqKit;

namespace AgencyLib {
	public interface IOfferRepository {
		//IEnumerable<OfferData> Get(OfferListFilter lf);
		IEnumerable<OfferData> Get(OfferListFilter lf);
		Offer Get(int offerId, int networkId);
		IEnumerable<Offer> Get(int networkId);
		CreativitiesResult FetchCreativities(int networkId, int offerId);
		bool RequestAccess(int networkId, int offerId);
		void Post(Offer value);
		void Put(Offer value);
		void Delete(int id);
		void WatchRemoteAPI();
		void NotifyOffers();
	}

	public class OfferRepository : IOfferRepository {
		private IAgencyDataContext _context;
		private string _connectionString;
		private INotifier _notifier;
		public IAffiliateNetworkRepository _affiliateNetworkRepository { get; private set; }
		public OfferRepository(string connectionString, IAgencyDataContext context, IAffiliateNetworkRepository affiliateNetworkRepository, INotifier notifier) {
			_connectionString = connectionString;
			_context = context;
			_affiliateNetworkRepository = affiliateNetworkRepository;
			_notifier = notifier;
		}
		public void Delete(int id) {
			_context.Offers.Remove(_context.Offers.Where(x => x.OfferId == id).SingleOrDefault());
			_context.SaveChanges();
		}
		public IEnumerable<OfferData> Get(OfferListFilter lf) {
			IQueryable<Offer> iq = _context.Offers.Include("Network").AsQueryable();
			var clause = PredicateBuilder.New<Offer>();
			if (!string.IsNullOrEmpty(lf.Name)) {
				var args = lf.Name.Split('%');
				foreach (var a in args) {
					clause = clause.And(x => x.Name.Contains(a));
				}
			}
			if (!string.IsNullOrEmpty(lf.Status) && lf.Status != "undefined") {
				clause = clause.And(x => lf.Status.Contains(x.Status));
			}
			if (lf.DateExpirationFrom > DateTime.MinValue) {
				clause = clause.And(x => x.DateExpiration >= lf.DateExpirationFrom);
			}
			if (lf.DateExpirationTo > DateTime.MinValue) {
				clause = clause.And(x => x.DateExpiration <= lf.DateExpirationTo);
			}
			if (lf.DateExpiration > DateTime.MinValue) {
				clause = clause.And(x => x.DateExpiration == lf.DateExpiration);
			}
			if (lf.ConversionCap > 0) {
				clause = clause.And(x => x.ConversionCap == lf.ConversionCap);
			}
			if (lf.ConversionCapFrom > 0) {
				clause = clause.And(x => x.ConversionCap >= lf.ConversionCapFrom);
			}
			if (lf.ConversionCapTo > 0) {
				clause = clause.And(x => x.ConversionCap <= lf.ConversionCapTo);
			}
			if (!string.IsNullOrEmpty(lf.ConversionType)) {
				clause = clause.And(x => lf.ConversionType.Contains(x.ConversionType));
			}
			if (lf.PayoutCap > 0) {
				clause = clause.And(x => x.PayoutCap == lf.PayoutCap);
			}
			if (lf.PayoutCapFrom > 0) {
				clause = clause.And(x => x.PayoutCap >= lf.PayoutCapFrom);
			}
			if (lf.NetworkId > 0) {
				clause = clause.And(x => x.NetworkId == lf.NetworkId);
			}
			if (!string.IsNullOrEmpty(lf.Os)) {
				clause = clause.And(x => x.AppOS == lf.Os);
			}
			if (!string.IsNullOrEmpty(lf.Ioc)) {
				var args = lf.Ioc.Split('|');
				for (int i = 0; i < args.Count(); i++) {
					var item = args[i];
					if (i == 0) {
						clause = clause.And(x => x.Countries.Contains(item));
					}
					else {
						clause = clause.Or(x => x.Countries.Contains(item));
					}
				}

			}
			if (!string.IsNullOrEmpty(lf.Id)) {
				clause = clause.And(x => x.OfferId == int.Parse(lf.Id));
			}
			if (!string.IsNullOrEmpty(lf.ApprovalStatus)) {
				clause = clause.And(x => x.approval_status == lf.ApprovalStatus);
			}
			List<Offer> result;
			if (clause.IsStarted) {
				result = iq.Where(clause).ToList();
			}
			else { 
				result = iq.ToList();
			}
			return result.Select(x => new OfferData() { 
				allow_direct_links = x.allow_direct_links,
				allow_website_links = x.allow_website_links,
				AppCategories = x.AppCategories,
				AppDescription = x.AppDescription,
				AppIconUrl = x.AppIconUrl,
				AppID = x.AppID,
				AppOS = x.AppOS,
				AppPrice = x.AppPrice,
				AppRating = x.AppRating,
				approval_status = x.approval_status,
				ConversionCap = x.ConversionCap,
				ConversionType = x.ConversionType,
				Countries = x.Countries,
				Currency = x.Currency,
				DateExpiration = x.DateExpiration,
				default_goal_name = x.default_goal_name,
				default_payout = x.default_payout,
				Description = x.Description,
				dne_download_url = x.dne_download_url,
				dne_list_id = x.dne_list_id,
				dne_third_party_list = x.dne_third_party_list,
				dne_unsubscribe_url = x.dne_unsubscribe_url,
				email_instructions = x.email_instructions,
				email_instructions_from = x.email_instructions_from,
				email_instructions_subject = x.email_instructions_subject,
				featured = x.featured,
				has_goals_enabled = x.has_goals_enabled,
				InitialConversionCap = x.InitialConversionCap,
				InitialPayoutCap = x.InitialPayoutCap,
				is_expired = x.is_expired,
				Name = x.Name,
				NetworkId = x.NetworkId,
				NetworkName = x.Network.Name,
				OfferId = x.OfferId,
				PayoutCap = x.PayoutCap,
				payout_type = x.payout_type,
				percent_payout = x.percent_payout,
				PreviewUrl = x.PreviewUrl,
				preview_url = x.preview_url,
				require_approval = x.require_approval,
				require_terms_and_conditions = x.require_terms_and_conditions,
				show_custom_variables = x.show_custom_variables,
				show_mail_list = x.show_mail_list,
				Status = x.Status,
				StatusValidation = x.StatusValidation,
				terms_and_conditions = x.terms_and_conditions,
				TrackingId = x.TrackingId,
				TrackingURL = x.TrackingURL,
				use_target_rules = x.use_target_rules
			});
		}d
		public IEnumerable<Offer> Get(int networkId) {
			var n = _context.AffiliateNetworks.Where(x => x.Id == networkId).SingleOrDefault();
			if (n != null && n.LastGet != null) {
				if ((DateTime.Now - n.LastGet) > TimeSpan.FromMinutes(15)) {
					FetchFrom(n);
				}
			}
			return _context.Offers.Where(x => x.NetworkId == networkId);
		}
		public Offer Get(int offerId, int networkId) {
			FetchDetailFrom(offerId, networkId);

			return _context.Offers.Where(x => x.OfferId == offerId && x.NetworkId == networkId).SingleOrDefault();
		}
		public void Post(Offer value) {
			_context.Offers.Add(value);
			_context.SaveChanges();
		}
		public void Put(Offer value) {
			var cq = @"UPDATE [dbo].[Offers]
				SET [NetworkId] = @NetworkId,
						[OfferId] = @OfferId,
						[Name] = @Name,
						[Description] = @Description,
						[Countries] = @Countries,
						[DateExpiration] = @DateExpiration,
						[Currency] = @Currency,
						[ConversionType] = @ConversionType,
						[Status] = @Status,
						[InitialPayoutCap] = @InitialPayoutCap,
						[InitialConversionCap] = @InitialConversionCap,
						[PayoutCap] = @PayoutCap,
						[ConversionCap] = @ConversionCap,
						[AppRating] = @AppRating,
						[AppOS] = @AppOS,
						[AppPrice] = @AppPrice,
						[AppID] = @AppID,
						[AppDescription] = @AppDescription,
						[AppCategories] = @AppCategories,
						[TrackingId] = @TrackingId,
				WHERE [NetworkId] = @NetworkId AND [OfferId] = @OfferId";

			using (var conn = new SqlConnection(_connectionString)) {
				conn.Open();
				using (var transaction = conn.BeginTransaction()) {
					try {
						conn.Execute(cq, new {
							NetworkId = value.NetworkId,
							OfferId = value.OfferId,
							Name = value.Name,
							Description = value.Description,
							Countries = value.Countries,
							DateExpiration = value.DateExpiration,
							Currency = value.Currency,
							ConversionType = value.ConversionType,
							Status = value.Status,
							InitialPayoutCap = value.InitialPayoutCap,
							InitialConversionCap = value.InitialConversionCap,
							PayoutCap = value.PayoutCap,
							ConversionCap = value.ConversionCap,
							AppRating = value.AppRating,
							AppOS = value.AppOS,
							AppPrice = value.AppPrice,
							AppID = value.AppID,
							AppDescription = value.AppDescription,
							AppCategories = value.AppCategories,
							TrackingId = value.TrackingId,
						}, transaction);
						transaction.Commit();
					}
					catch (Exception) {
						transaction.Rollback();
						throw;
					}
				}
			}
		}
		public void WatchRemoteAPI() {
			FetchFrom(_context.AffiliateNetworks.Where(x => x.Active == 1 && !string.IsNullOrEmpty(x.AccountSettings)).OrderBy(y => y.LastGet).ToList());
		}
		public void NotifyOffers() {
			_notifier.SendNotifications(_context);
		}
		private void FetchDetailFrom(int offerId, int networkID) {
			OfferProvider.Instance.FetchDetailFrom(_context.AffiliateNetworks.Where(x => x.Id == networkID).SingleOrDefault(), offerId, _context);
		}
		private void FetchFrom(IEnumerable<AffiliateNetwork> nets) {
			foreach (var net in nets) {
				try {
					FetchFrom(net);
				}
				catch (Exception ex) {
					throw ex;
				}
			}
		}
		private void FetchFrom(AffiliateNetwork net) {
			_notifier.CreateNotification(net.Name, OfferProvider.Instance.FetchFrom(net, _context), _context);
			_notifier.SendNotifications(_context);
		}
		private IEnumerable<OfferData> QueryFor(OfferListFilter lf) {
			const string DATE_TIME_FORMAT_STRING = "yyyy-MM-ddThh:mm:ss.fffZ";
			using (var conn = new SqlConnection(_connectionString)) {
				conn.Open();
				try {
					DynamicParameters spparams = new DynamicParameters();
					spparams.Add("@Page", lf.Page);
					spparams.Add("@Limit", lf.Limit);
					spparams.Add("@OrderBy", lf.Sort);
					spparams.Add("@SortOrder", lf.SortDirection);
					if (!string.IsNullOrEmpty(lf.Name)) {
						spparams.Add("@Name", lf.Name);
					}
					if (!string.IsNullOrEmpty(lf.Status)) {
						spparams.Add("@Status", lf.Status);
					}
					if (lf.DateExpiration > DateTime.MinValue) {
						spparams.Add("@DateExpiration", lf.DateExpiration.ToString(DATE_TIME_FORMAT_STRING));
					}
					if (lf.DateExpirationFrom > DateTime.MinValue) {
						spparams.Add("@DateExpirationFrom", lf.DateExpirationFrom.ToString(DATE_TIME_FORMAT_STRING));
					}
					if (lf.DateExpirationTo > DateTime.MinValue) {
						spparams.Add("@DateExpirationTo", lf.DateExpirationTo.ToString(DATE_TIME_FORMAT_STRING));
					}
					if (lf.ConversionCap > 0) {
						spparams.Add("@ConversionCap", lf.ConversionCap);
					}
					if (lf.ConversionCapFrom > 0) {
						spparams.Add("@ConversionCapFrom", lf.ConversionCapFrom);
					}
					if (lf.ConversionCapTo > 0) {
						spparams.Add("@ConversionCapTo", lf.ConversionCapTo);
					}
					if (!string.IsNullOrEmpty(lf.ConversionType)) {
						spparams.Add("@ConversionType", lf.ConversionType);
					}
					if (lf.PayoutCap > 0) {
						spparams.Add("@PayoutCap", lf.PayoutCap);
					}
					if (lf.PayoutCapFrom > 0) {
						spparams.Add("@PayoutCapFrom", lf.PayoutCapFrom);
					}
					if (lf.PayoutCapTo > 0) {
						spparams.Add("@PayoutCapTo", lf.PayoutCapTo);
					}
					if (lf.NetworkId > 0) {
						spparams.Add("@NetworkId", lf.NetworkId);
					}
					if (!string.IsNullOrEmpty(lf.Os)) {
						spparams.Add("@Os", lf.Os);
					}
					if (!string.IsNullOrEmpty(lf.Ioc)) {
						spparams.Add("@Ioc", lf.Ioc);
					}
					if (!string.IsNullOrEmpty(lf.Id)) {
						spparams.Add("@Id", lf.Id);
					}
					if (!string.IsNullOrEmpty(lf.ApprovalStatus)) {
						spparams.Add("@ApprovalStatus", lf.ApprovalStatus);
					}
					return conn.Query<OfferData>("[dbo].GetOffers", spparams, commandType: CommandType.StoredProcedure);
				}
				catch (Exception ex) {
					throw ex;
				}
			}
		}




		public CreativitiesResult FetchCreativities(int networkId, int offerId) {
			return OfferProvider
								.Instance
									.FetchCreativitiesFrom(_context.AffiliateNetworks.Where(x => x.Id == networkId).SingleOrDefault(), offerId, _context);
		}

		public bool RequestAccess(int networkId, int offerId) {
			return OfferProvider.Instance.RequestAccess(_context.AffiliateNetworks.Where(x => x.Id == networkId).SingleOrDefault(), offerId, _context);

		}
	}
}
