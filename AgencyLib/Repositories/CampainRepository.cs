﻿using AgencyLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using AgencyLib.Providers;
using LinqKit;

namespace AgencyLib {
	public interface ICampainRepository {
		//IEnumerable<Campain> Get();
		IEnumerable<Campain> Get(CampainListFilter cfl);
		Campain Get(int id);
		void Post(Campain value);
		void Put(Campain value);
		void Delete(int id);
		void WatchRemoteAPI();
	}
	public class CampainRepository : ICampainRepository {
		private string _connectionString;
		private IAgencyDataContext _context;
		public CampainRepository(string connectionString, IAgencyDataContext context) {
			_connectionString = connectionString;
			_context = context;
		}
		public void Delete(int id) {
			var cq = @"DELETE FROM [dbo].[Campains] WHERE Id = @Id";
			using (var conn = new SqlConnection(_connectionString)) {
				conn.Open();
				using (var transaction = conn.BeginTransaction()) {
					try {
						conn.Execute(cq, new {
							Id = id,
						}, transaction);
						transaction.Commit();
					}
					catch (Exception) {
						transaction.Rollback();
						throw;
					}
				}
			}
		}
		public IEnumerable<Campain> Get(CampainListFilter clf) {
			var tfs = _context.TrafficSources.Where(x => x.Active == 1).OrderBy(y => y.LastGet).Select(z => z.Id);
			IQueryable<Campain> iq = _context.Campains.AsQueryable();
			var clause = PredicateBuilder.New<Campain>();
			if (!string.IsNullOrEmpty(clf.Name)) {
				var args = clf.Name.Split('%');
				foreach (var a in args) {
					clause = clause.And(x => x.appName.Contains(a));
				}
			}
			if (clf.Id > 0) {
				//iq = iq.Where(x => x.campaignId == clf.Id);
				clause = clause.And(x => x.campaignId == clf.Id);
			}
			if (clf.Bid > 0) {
				clause = clause.And(x => x.bidPrice == clf.Bid);
				//iq = iq.Where(x => x.bidPrice == clf.Bid);
			}
			if (clf.DailyBudget > 0) {
				//iq = iq.Where(x => x.dailyBudget == clf.DailyBudget);
				clause = clause.And(x => x.dailyBudget == clf.DailyBudget);

			}
			if (!string.IsNullOrEmpty(clf.PricingModel)) {
				//iq = iq.Where(x => clf.PricingModel.Contains(x.pricingModel));
				clause = clause.And(x => clf.PricingModel.Contains(x.pricingModel));
			}
			if (!string.IsNullOrEmpty(clf.Platform)) {
				//iq = iq.Where(x => x.platform == clf.Platform);
				clause = clause.And(x => x.platform == clf.Platform);
			}
			if (!string.IsNullOrEmpty(clf.Countries)) {
				var args = clf.Countries.Split('|');
				for (int i = 0; i < args.Count(); i++) {
					var item = args[i];
					if (i == 0) {
						clause = clause.And(x => x.countries.Contains(item));
					}
					else {
						clause = clause.Or(x => x.countries.Contains(item));
					}
				}
			}
			if (clf.StartedDateFrom > DateTime.MinValue) {
				clause = clause.And(x => x.startDate >= clf.StartedDateFrom);
			}
			if (clf.StartedDateTo > DateTime.MinValue) {
				clause = clause.And(x => x.startDate <= clf.StartedDateTo);
			}
			if (!string.IsNullOrEmpty(clf.Statuses) && clf.Statuses != "undefined") {
				clause = clause.And(x => clf.Statuses.Contains(x.Status));
			}
			clause = clause.And(x => tfs.Contains(x.trafficSourceId));
			iq = iq.Where(clause).AsExpandable<Campain>();
			return iq.ToList();
		}
		public Campain Get(int id) {
			#region EF6
			return _context
								.Campains
								.Where(x => x.Id == id).SingleOrDefault();
			#endregion
		}
		public void Post(Campain value) {
			var cq = @"
				INSERT INTO [dbo].[Campains]
					 ([TrackingId]
					 ,[Name]
					 ,[Offer_NetworkId]
					 ,[Offer_OfferId]
					 ,[TrafficSource_Id]
					 ,[Payout]
					 ,[Status])
				VALUES
					 (
						 @TrackingId,
						 @Name,
						 @Offer_NetworkId,
						 @Offer_OfferId,
						 @TrafficSource_Id,
						 @Payout,
						 @Status
						)
			";
			Execute(value, cq);
		}
		public void Put(Campain value) {
			var cq = @"UPDATE [dbo].[Campains]
				SET [TrackingId] = @TrackingId,
						[Name] = @Name,
						[Payout] = @Payout,
						[Status] = @Status,
						[Offer_NetworkId] = @Offer_NetworkId,
						[Offer_OfferId] = @Offer_OfferId,
						[TrafficSource_Id] = @TrafficSourceId
				WHERE [Id] = @Id";
			Execute(value, cq);
		}
		public void WatchRemoteAPI() {
			FetchFrom(_context.TrafficSources.Where(x => x.Active == 1).OrderBy(y => y.LastGet).ToList());
		}

		private void Execute(Campain value, string cq) {
			using (var conn = new SqlConnection(_connectionString)) {
				conn.Open();
				using (var transaction = conn.BeginTransaction()) {
					try {
						conn.Execute(cq, new {
							Id = value.Id,
							Name = value.Name,
							TrafficSourceId = value.TrafficSource.Id
						}, transaction);
						transaction.Commit();
					}
					catch (Exception) {
						transaction.Rollback();
						throw;
					}
				}
			}
		}
		private void FetchFrom(IEnumerable<TrafficSource> trafficsources) {
			var results = new List<Providers.Campains.Campain>();
			foreach (var ts in trafficsources) {
				try {
					FetchFrom(ts);
				}
				catch (Exception ex) {
					throw ex;
				}
			}
		}
		private void FetchFrom(TrafficSource ts) {
			CampainProvider.Instance.FetchFrom(ts, _context);
		}
	}
}
