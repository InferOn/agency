﻿using AgencyLib.Data;
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace AgencyLib.Repositories {
	public interface IAuthRepository: IDisposable {
		Task<IdentityResult> RegisterUser(UserModel userModel);
		Task<IdentityUser> FindUser(string userName, string password);
		Task<IdentityResult> DeleteUser(UserModel userModel);
		IEnumerable<IdentityUser> GetAll();
		void Dispose();
	}
	public class AuthRepository : IAuthRepository {
		private AgencyContext _context;
		private string _connectionString;

		private UserManager<IdentityUser> _userManager;
		public AuthRepository(string connectionString) {
			_connectionString = connectionString;
			_context = new AgencyContext();
			_userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_context));
		}
		public async Task<IdentityResult> RegisterUser(UserModel userModel) {
			IdentityUser user = new IdentityUser {
				UserName = userModel.UserName
			};
			var result = await _userManager.CreateAsync(user, userModel.Password);
			return result;
		}
		public async Task<IdentityResult> DeleteUser(UserModel userModel) {
			var result = await _userManager.DeleteAsync(_userManager.FindByName(userModel.UserName));
			return result;
		}

		public async Task<IdentityUser> FindUser(string userName, string password) {
			IdentityUser user = await _userManager.FindAsync(userName, password);
			return user;
		}

		public IEnumerable<IdentityUser> GetAll() {
			return _userManager.Users.ToList();
		}
		public void Dispose() {
			_userManager.Dispose();
		}
	}
}