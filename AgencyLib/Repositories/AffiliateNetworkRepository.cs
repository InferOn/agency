﻿using AgencyLib.Data;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System;
using System.Data.SqlClient;
using Dapper;

namespace AgencyLib {
	public interface IAffiliateNetworkRepository {
		IEnumerable<AffiliateNetwork> Get(AffiliateNetworkListFilter listFilter);
		AffiliateNetwork Get(int id);
		void Post(AffiliateNetwork value);
		void Put(AffiliateNetwork value);
		int Delete(int id);
		int Delete(int[] campaignIds);
	}
	public class AffiliateNetworkRepository : IAffiliateNetworkRepository {
		private IAgencyDataContext _context;
		private string _connectionString;
		public AffiliateNetworkRepository(string connectionString, IAgencyDataContext context) {
			_connectionString = connectionString;
			_context = context;
		}
		
		public IEnumerable<AffiliateNetwork> Get(AffiliateNetworkListFilter listFilter) {
			return ApplyPaging(listFilter, ApplyFilters(listFilter));
		}
		public AffiliateNetwork Get(int id) {
			return _context.AffiliateNetworks.Where(x => x.Id == id).SingleOrDefault();
		}
		public void Post(AffiliateNetwork value) {

			if (value.LastGet <= DateTime.MinValue) {
				value.LastGet = null;
			}
			_context.AffiliateNetworks.Add(value);
			_context.SaveChanges();
			//var cq = @"
			//	INSERT INTO [dbo].[AffiliateNetworks]
			//		 ([Name]
			//		 ,[AccountSettings]
			//		 ,[TrackingId])
			//	VALUES
			//		 (@Name,
			//			@AccountSettings,
			//			@TrackingId)";
			//using (var conn = new SqlConnection(_connectionString)) {
			//	conn.Open();
			//	using (var transaction = conn.BeginTransaction()) {
			//		object o;
			//		try {
			//			o = new {
			//				TrackingId = value.TrackingId,
			//				Name = value.Name,
			//				AccountSettings = value.AccountSettings,
			//				Note = value.Name,
			//				Active = value.Active, 
			//				LastGet = null
			//			};
			//			if (value.LastGet > DateTime.MinValue) {
			//				o = new {
			//					TrackingId = value.TrackingId,
			//					Name = value.Name,
			//					AccountSettings = value.AccountSettings,
			//					Note = value.Name,
			//					Active = value.Active,
			//					LastGet = value.LastGet
			//				};
			//			}
			//			conn.Execute(cq, o, transaction);
			//			transaction.Commit();
			//		}
			//		catch (Exception) {
			//			transaction.Rollback();
			//			throw;
			//		}
			//	}
			//}
		}
		public void Put(AffiliateNetwork value) {
			var n = _context.AffiliateNetworks.Where(x => x.Id == value.Id).SingleOrDefault();
			n.Active = value.Active;
			n.Name = value.Name;
			n.Note = value.Note;
			_context.SaveChanges();
		}
		private static IQueryable<AffiliateNetwork> ApplyPaging(AffiliateNetworkListFilter listFilter, IOrderedQueryable<AffiliateNetwork> context) {
			var page = 0; var limit = 10;
			if (listFilter != null) {
				page = listFilter.Page > 1 ? listFilter.Page -1 : listFilter.Page;
				limit = listFilter.Limit;
			}
			return context.Skip((page) * limit).Take(limit);
		}
		private IOrderedQueryable<AffiliateNetwork> ApplyFilters(AffiliateNetworkListFilter listFilter) {
			IOrderedQueryable<AffiliateNetwork> context;
			if (!string.IsNullOrEmpty(listFilter.Name)) {
				context = _context.AffiliateNetworks.Where(a => a.Name.StartsWith(listFilter.Name)).OrderBy(x => x.Id);
			}
			else {
				context = _context.AffiliateNetworks.OrderBy(x => x.Id);
			}
			return context;
		}

		public int Delete(int[] networkIds) {
			var ch = _context.AffiliateNetworks.Where(x => networkIds.Contains(x.Id));
			if (ch.Any()) {
				foreach (var c in ch) {
					_context.AffiliateNetworks.Remove(c);
				}
				return _context.SaveChanges();
			}
			return -1;
		}
		public int Delete(int id) {
			_context.AffiliateNetworks.Remove(_context.AffiliateNetworks.Where(x => x.Id == id).SingleOrDefault());
			return _context.SaveChanges();
		}
	}
}
