﻿using AgencyLib.Data;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace AgencyLib {
	public interface ITrafficSourceRepository {
		IEnumerable<TrafficSource> Get();
		IEnumerable<TrafficSource> Get(TrafficSourceListFilter lf);
		TrafficSource Get(int id);
		void Post(TrafficSource value);
		void Put(TrafficSource value);
		void Delete(int id);
	}
	public class TrafficSourceRepository : ITrafficSourceRepository {
		private IAgencyDataContext _context;
		private string _connectionString;
		public TrafficSourceRepository(string connectionString, IAgencyDataContext context) {
			_connectionString = connectionString;
			_context = context;
		}		
		public virtual void Delete(int id) {
				_context.TrafficSources.Remove(_context.TrafficSources.Where(x => x.Id == id).SingleOrDefault());
				_context.SaveChanges();
		}
		public virtual IEnumerable<TrafficSource> Get() {
			return _context.TrafficSources;
		}
		public virtual IEnumerable<TrafficSource> Get(TrafficSourceListFilter lf) {
			return _context.TrafficSources;
		}

		public virtual TrafficSource Get(int id) {
			return _context.TrafficSources.Where(x=> x.Id == id).SingleOrDefault();

		}

		public void Post(TrafficSource value) {
			_context.TrafficSources.Add(value);
			_context.SaveChanges();
		}

		public void Put(TrafficSource value) {
			var cq = @"UPDATE [dbo].[TrafficSources]
				SET [Name] = @Name
				WHERE [Id] = @Id";
			using (var conn = new SqlConnection(_connectionString)) {
				conn.Open();
				using (var transaction = conn.BeginTransaction()) {
					try {
						conn.Execute(cq, new {
							Id = value.Id,
							Name = value.Name,
						}, transaction);
						transaction.Commit();
					}
					catch (Exception) {
						transaction.Rollback();
						throw;
					}
				}
			}
		}
	}
}
