﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using AegisImplicitMail;
using static AgencyLib.Providers.OfferProvider;

namespace AgencyLib {
	public interface INotifier {
		void CreateNotification(string networkName, FetchSummary summary, IAgencyDataContext context);
		void SendNotifications(IAgencyDataContext context);
	}

	public interface INotifierConfiguration {
		string MimeMailer { get; set; }
		string MimePort { get; set; }
		string MimeUser { get; set; }
		string MimePassword { get; set; }
		string MimeFrom { get; set; }
		string SummaryAddresses { get; set; }
	}

	public class NotifierConfiguration: INotifierConfiguration {
		#region members
		public string MimeMailer { get; set; }
		public string MimePort { get; set; }
		public string MimeUser { get; set; }
		public string MimePassword { get; set; }
		public string MimeFrom { get; set; }
		public string SummaryAddresses { get; set; }

		#endregion
		public NotifierConfiguration(
				string mimeMailer,
				string mimePort,
				string mimeUser,
				string mimePassword,
				string mimeFrom,
				string summaryAddresses) {
			MimeMailer = mimeMailer;
			MimePort = mimePort;
			MimeUser = mimeUser;
			MimePassword = mimePassword;
			MimeFrom = mimeFrom;
			SummaryAddresses = summaryAddresses;
		}
	}

	public class Notifier : INotifier {
		private INotifierConfiguration _configuration;
		public Notifier(INotifierConfiguration configuration) {
			_configuration = configuration;
		}
		public void CreateNotification(string networkName, FetchSummary summary, IAgencyDataContext context) {
			var recipient = new Data.Notification.MailNotification.MailRecipient() { Address = _configuration.SummaryAddresses};
			context.Recipients.Add(recipient);// CR: it is necessary?
			var notification = new Data.Notification.MailNotification();
			notification.Recipients.Add(recipient);
			notification.DateCreation = DateTime.Now;
			notification.Header = string.Format("{0} offers summary at {1}", networkName, notification.DateCreation.Value.ToString("dd/MM/yyyy HH:mm"));
			notification.Body = ProcessWatchSummary(networkName, summary);
			context.Notifications.Add(notification);
			context.SaveChanges();
		}
		public void SendNotifications(IAgencyDataContext context) {
			var mailer = new MimeMailer(_configuration.MimeMailer,  int.Parse(_configuration.MimePort));
			mailer.User = _configuration.MimeUser;
			mailer.Password =  _configuration.MimePassword;
			mailer.SslType = SslMode.Ssl;
			mailer.AuthenticationMode = AuthenticationType.Base64;


			var tosend = context.Notifications.Include(x => x.Recipients).Where(x => x.Status == 0).ToList();
			foreach (var target in tosend) {
				var ts = target;
				var mailmessage = new MimeMailMessage();
				mailmessage.From = new MimeMailAddress(_configuration.MimeFrom);
				foreach (var rc in ts.Recipients) {
					mailmessage.To.Add(rc.Address);
				}
				mailmessage.Subject = ts.Header;
				mailmessage.IsBodyHtml = true;
				mailmessage.Body = ts.Body;
				try {
					mailer.SendMail(mailmessage);
					ts.Status = 1;
					context.SaveChanges();
				}
				catch (Exception ex) {
					//TODO: uncomment
					throw;
				}
			}
		}
		private string ProcessWatchSummary(string networkName, FetchSummary summary) {
			if (summary.InsertInfo == null || summary.ChangesInfo == null || summary.DeleteInfo == null) {
				return string.Empty;
			}

			var insertCount = summary.InsertInfo.Count;
			var deleteCount = summary.DeleteInfo.Count;
			var ciGroups = summary.ChangesInfo.GroupBy(x => x.Key);
			var updateGroupCount = ciGroups.Count();
			var result = string.Empty;
				var sb = new System.Text.StringBuilder();
				sb.AppendLine("<html>");
				sb.AppendLine("<head>");
				sb.AppendFormat("<meta http-equiv=\"Content - Type\" content=\"text / html; charset =UTF-8\" />", networkName);
				sb.AppendFormat("<title>*|Summary for remote network {0} changes|*</title>", networkName);
				addStyleTag(sb);
				sb.AppendLine("</head>");
				sb.AppendLine("<body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\">");
				sb.AppendFormat("<h2>Summary for remote network {0} changes:</h2>", networkName);
				sb.AppendLine("<hr/>");
				sb.AppendLine("<br/>");
				sb.AppendFormat("<h3>New {0} offers has been inserted:<h3>", insertCount);
				sb.AppendLine("<hr/>");
				sb.AppendLine("<br/>");
				foreach (var ii in summary.InsertInfo) {
					sb.AppendFormat("<p>{0}</p>", ii.Value.Item2);
					sb.AppendLine("<br/>");
				}
				sb.AppendLine("<br/>");
				sb.AppendFormat("<h3>{0} offers has been deleted:</h3>", deleteCount);
				sb.AppendLine("<hr/>");
				sb.AppendLine("<br/>");
				foreach (var di in summary.DeleteInfo) {
					sb.AppendFormat("<p>{0}</p>", di.Value.Item2);
					sb.AppendLine("<br/>");
				}
				sb.AppendLine("<br/>");
				sb.AppendFormat("<h3>{0} offers has been updated:</h3>", updateGroupCount);
				sb.AppendLine("<br/>");
				foreach (var ci in ciGroups) {
					sb.AppendFormat("<p>Offer id: {0}", ci.First().Key);
					sb.Append("<ul>");
					foreach (var c in ci) {
						foreach (var v in c.Value) {
							sb.Append("<li>");
							sb.AppendFormat("{0} was {1}, now is {2}.", v.Prop, v.trgVal, v.srcVal);
							sb.Append("</li>");
						}
					}
					sb.Append("</ul>");
					sb.Append("</p>");
					sb.AppendLine("<br/>");
				}
				sb.AppendLine("</body>");
				sb.AppendLine("</html>");
				result = sb.ToString();
			return result;
		}
		private static void addStyleTag(System.Text.StringBuilder sb) {
			sb.AppendLine("<style>");
			sb.AppendLine("h2 {");
			sb.AppendLine("color: #404040 !important;");
			sb.AppendLine("display: block;");
			sb.AppendLine("font-family: Helvetica;");
			sb.AppendLine("font-size: 20px;");
			sb.AppendLine("font-style: normal;");
			sb.AppendLine("display: block;");
			sb.AppendLine("font-weight: bold;");
			sb.AppendLine("line-height: 100 %;");
			sb.AppendLine("letter-spacing: normal;");
			sb.AppendLine("margin-top: 0;");
			sb.AppendLine("margin-right: 0;");
			sb.AppendLine("margin-bottom: 10px;");
			sb.AppendLine("margin-left: 0;");
			sb.AppendLine("text-align: left;");
			sb.AppendLine("}");

			sb.AppendLine("h3{");
			sb.AppendLine("color: #606060 !important;");
			sb.AppendLine("display: block;");
			sb.AppendLine("font-family: Helvetica;");
			sb.AppendLine("font-size: 16px;");
			sb.AppendLine("font-style: italic;");
			sb.AppendLine("font-style: italic;");
			sb.AppendLine("font-style: italic;");
			sb.AppendLine("font-style: italic;");
			sb.AppendLine("font-weight: normal;");
			sb.AppendLine("line-height: 100 %;");
			sb.AppendLine("letter-spacing: normal;");
			sb.AppendLine("font-weight: normal;");
			sb.AppendLine("margin-top: 0;");
			sb.AppendLine("margin-right: 0;");
			sb.AppendLine("margin-bottom: 10px;");
			sb.AppendLine("margin-left: 0;");
			sb.AppendLine("text-align: left;");
			sb.AppendLine("}");

			sb.AppendLine("ul{");
			sb.AppendLine("list-style-type: none;");
			sb.AppendLine("list-style-position: inside;");
			sb.AppendLine("}");

			sb.AppendLine("</style>");
		}
		private string ProcessBody(Dictionary<string, Dictionary<string, Tuple<string, string>>> recordChangedInfo) {
			var info = recordChangedInfo;
			var sb = new System.Text.StringBuilder();
			foreach (var item in info) {
				sb.AppendLine(item.Key);
				foreach (var i in item.Value) {
					sb.AppendLine(string.Format("{0}: {1}", i.Key, i.Value));
				}
			}
			return sb.ToString();
		}
	}
}
