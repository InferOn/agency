﻿using AgencyLib.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace AgencyLib.Test {
	[TestClass]
	public class TrafficSourceRepository {
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void TrafficSourceRepository_Get_Count_Should_Return_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.TrafficSources)
			//		.Returns(new FakeDbSet<TrafficSource>
			//		{
			//			new TrafficSource { Id = 1 },
			//			new TrafficSource { Id = 2 }
			//		});
			//var repository = new AgencyLib.TrafficSourceRepository("", mock.Object);
			//// Act
			//var results = repository.Get();
			//// Assert
			//Assert.AreEqual(2, results.Count());
		}
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void TrafficSourceRepository_Get_Count_Should_Return_0() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.TrafficSources)
			//		.Returns(new FakeDbSet<TrafficSource> { });
			//var repository = new AgencyLib.TrafficSourceRepository("", mock.Object);
			//// Act
			//var results = repository.Get();
			//// Assert
			//Assert.AreEqual(0, results.Count());
		}
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void TrafficSourceRepository_Get_By_Id_2_Should_Return_Item_Id_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.TrafficSources)
			//		.Returns(new FakeDbSet<TrafficSource>
			//		{
			//			new TrafficSource { Id = 1},
			//			new TrafficSource { Id = 2}
			//		});
			//var repository = new AgencyLib.TrafficSourceRepository("", mock.Object);
			//// Act
			//var result = repository.Get(2);
			//// Assert
			//Assert.IsNotNull(result);
			//Assert.IsTrue(result.Id == 2);
		}
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void TrafficSource_Post_ShouldReturn_Count_Count_More_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.TrafficSources)
			//		.Returns(new FakeDbSet<TrafficSource>
			//		{
			//			new TrafficSource { Id = 1 },
			//		});
			//var repository = new AgencyLib.TrafficSourceRepository("", mock.Object);
			//var oldCount = repository.Get().Count();
			//// Act
			//repository.Post(new TrafficSource() { Id = 3, Name = "test name", TrackingId = "abc" });
			//var newCount = repository.Get().Count();
			//Assert.AreEqual(oldCount + 1, newCount);
		}
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void TrafficSource_Delete_ShouldReturn_Count_Count_less_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.TrafficSources)
			//		.Returns(new FakeDbSet<TrafficSource>
			//		{
			//			new TrafficSource { Id = 1 },
			//			new TrafficSource { Id = 2 },
			//		});
			//var repository = new AgencyLib.TrafficSourceRepository("", mock.Object);
			//var oldCount = repository.Get().Count();
			//// Act
			//repository.Delete(2);
			//var newCount = repository.Get().Count();
			//Assert.AreEqual(oldCount - 1, newCount);
		}
		[TestMethod]
		[TestCategory("Traffic Sources")]
		public void Offer_Post_Should_ShouldUpdate_RelatedEntity() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			////mock.SetupAllProperties();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 },
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository(mock.Object);
			//var temp = repository.Get(2);
			////// act
			//temp.Name = "hit";
			//repository.Put(temp);
			//var result = repository.Get(2);
			////// assert
			//Assert.AreEqual("hit", result.Name);
		}
	}
}
