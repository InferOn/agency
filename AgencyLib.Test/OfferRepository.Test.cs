﻿using AgencyLib.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace AgencyLib.Test {
	[TestClass]
	public class OfferRepository {
		[TestMethod]
		[TestCategory("Offers")]
		public void OfferRepository_Get_Count_Should_Return_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.Offers)
			//		.Returns(new FakeDbSet<Offer>
			//		{
			//			new Offer { OfferId = 1 },
			//			new Offer { OfferId = 2 }
			//		});
			//var affRep = new Mock<IAffiliateNetworkRepository>();
			//var notifierMock = new Mock<INotifier>();
			//var repository = new AgencyLib.OfferRepository("", mock.Object, affRep.Object, notifierMock.Object);
			//// Act
			//var results = repository.Get(new OfferListFilter() { });
			//// Assert
			//Assert.AreEqual(2, results.Count());
		}
		[TestMethod]
		[TestCategory("Offers")]
		public void OfferRepository_Get_Count_Should_Return_0() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.Offers)
			//		.Returns(new FakeDbSet<Offer> { });
			//var affRep = new Mock<IAffiliateNetworkRepository>();
			//var notifierMock = new Mock<INotifier>();
			//var repository = new AgencyLib.OfferRepository("", mock.Object, affRep.Object, notifierMock.Object);
			//// Act
			//var results = repository.Get(new OfferListFilter() { });
			//// Assert
			//Assert.AreEqual(0, results.Count());
		}
		[TestMethod]
		[TestCategory("Offers")]
		public void OfferRepository_Get_By_Id_2_Should_Return_Item_Id_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.Offers)
			//		.Returns(new FakeDbSet<Offer>
			//		{
			//			new Offer { OfferId = 1, NetworkId = 1 },
			//			new Offer { OfferId = 2, NetworkId = 1 }
			//		});
			//var affRep = new Mock<IAffiliateNetworkRepository>();
			//var notifierMock = new Mock<INotifier>();
			//var repository = new AgencyLib.OfferRepository("", mock.Object, affRep.Object, notifierMock.Object);
			//// Act
			//var result = repository.Get(2, 1);
			//// Assert
			//Assert.IsNotNull(result);
			//Assert.IsTrue(result.OfferId == 2);
		}
		[TestMethod]
		[TestCategory("Offers")]
		public void Offer_Post_ShouldReturn_Count_Count_More_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.Offers)
			//		.Returns(new FakeDbSet<Offer>
			//		{
			//			new Offer { OfferId = 1 },
			//		});
			//var affRep = new Mock<IAffiliateNetworkRepository>();
			//var notifierMock = new Mock<INotifier>();
			//var repository = new AgencyLib.OfferRepository("", mock.Object, affRep.Object, notifierMock.Object);
			//var oldCount = repository.Get(new OfferListFilter() { }).Count();
			//// Act
			//repository.Post(new Offer() { OfferId = 3, Name = "test name", TrackingId = "abc" });
			//var newCount = repository.Get(new OfferListFilter() { }).Count();
			//Assert.AreEqual(oldCount + 1, newCount);
		}
		[TestMethod]
		[TestCategory("Offers")]
		public void Offer_Delete_ShouldReturn_Count_Count_less_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.Offers)
			//		.Returns(new FakeDbSet<Offer>
			//		{
			//			new Offer { OfferId = 1 },
			//			new Offer { OfferId = 2 },
			//		});
			//var affRep = new Mock<IAffiliateNetworkRepository>();
			//var notifierMock = new Mock<INotifier>();
			//var repository = new AgencyLib.OfferRepository("", mock.Object, affRep.Object, notifierMock.Object);
			//var oldCount = repository.Get(new OfferListFilter() { }).Count();
			//// Act
			//repository.Delete(2);
			//var newCount = repository.Get(new OfferListFilter() { }).Count();
			//Assert.AreEqual(oldCount - 1, newCount);
		}
		[TestMethod]
		[TestCategory("Offers")]
		public void Offer_Post_Should_ShouldUpdate_RelatedEntity() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			////mock.SetupAllProperties();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 },
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository(mock.Object);
			//var temp = repository.Get(2);
			////// act
			//temp.Name = "hit";
			//repository.Put(temp);
			//var result = repository.Get(2);
			////// assert
			//Assert.AreEqual("hit", result.Name);
		}
	}
}
