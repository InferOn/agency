﻿using AgencyLib.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace AgencyLib.Test {
	//[TestClass]
	//public class CampainRepository {
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void CampainRepository_Get_Count_Should_Return_2() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain>
	//				{
	//					new Campain { Id = 1 },
	//					new Campain { Id = 2 }
	//				});
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		// Act
	//		var results = repository.Get();
	//		// Assert
	//		Assert.AreEqual(2, results.Count());
	//	}
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void CampainRepository_Get_Count_Should_Return_0() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain> { });
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		// Act
	//		var results = repository.Get();
	//		// Assert
	//		Assert.AreEqual(0, results.Count());
	//	}
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void CampainRepository_Get_By_Id_2_Should_Return_Item_Id_2() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain>
	//				{
	//					new Campain { Id = 1 },
	//					new Campain { Id = 2 }
	//				});
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		// Act
	//		var result = repository.Get(2);
	//		// Assert
	//		Assert.IsNotNull(result);
	//		Assert.IsTrue(result.Id == 2);
	//	}
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void Campain_Post_ShouldReturn_Count_Count_More_1() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain>
	//				{
	//					new Campain { Id = 1 },
	//				});
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		var oldCount = repository.Get().Count();
	//		// Act
	//		repository.Post(new Campain() { Id = 3, Name = "test name", TrackingId = "abc" });
	//		var newCount = repository.Get().Count();
	//		Assert.AreEqual(oldCount + 1, newCount);
	//	}
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void Campain_Delete_ShouldReturn_Count_Count_less_1() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain>
	//				{
	//					new Campain { Id = 1 },
	//					new Campain { Id = 2 },
	//				});
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		var oldCount = repository.Get().Count();
	//		// Act
	//		repository.Delete(2);
	//		var newCount = repository.Get().Count();
	//		Assert.AreEqual(oldCount - 1, newCount);
	//	}
	//	[TestMethod]
	//	[TestCategory("Campains")]
	//	public void Campain_Post_Should_ShouldUpdate_RelatedEntity() {
	//		// arrange
	//		var mock = new Mock<IAgencyDataContext>();
	//		mock.Setup(x => x.Campains)
	//				.Returns(new FakeDbSet<Campain>
	//				{
	//					new Campain { Id = 1 },
	//					new Campain { Id = 2 },
	//				});
	//		var repository = new AgencyLib.CampainRepository("", mock.Object);
	//		var temp = repository.Get(2);
	//		//// act
	//		temp.Name = "hit";
	//		repository.Put(temp);
	//		var result = repository.Get(2);
	//		//// assert
	//		Assert.AreEqual("hit", result.Name);
	//	}
	//}
}
