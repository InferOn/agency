﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AgencyLib.Data;
using Moq;
using System.Linq;

namespace AgencyLib.Test {
	[TestClass]
	public class AffiliateNetworkRepository {
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void NetworkRepository_Get_Count_Should_Return_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 }
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository("", mock.Object);
			//// Act
			//var results = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1});
			//// Assert
			//Assert.AreEqual(2, results.Count());
		}
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void NetworkRepository_Get_Count_Should_Return_0() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork> { });
			//var repository = new AgencyLib.AffiliateNetworkRepository("", mock.Object);
			//// Act
			//var results = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1 });
			//// Assert
			//Assert.AreEqual(0, results.Count());
		}
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void NetworkRepository_Get_By_Id_2_Should_Return_Item_Id_2() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 }
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository("", mock.Object);
			//// Act
			//var result = repository.Get(2);
			//// Assert
			//Assert.IsNotNull(result);
			//Assert.IsTrue(result.Id == 2);
		}
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void AffiliateNetwork_Post_ShouldReturn_Count_Count_More_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository("", mock.Object);
			//var oldCount = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1 }).Count();
			//// Act
			//repository.Post(new AffiliateNetwork() { Id = 3, Name = "test name", TrackingId = "abc", AccountSettings = "{key:1}" });
			//var newCount = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1 }).Count();
			//Assert.AreEqual(oldCount + 1, newCount);
		}
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void AffiliateNetwork_Delete_ShouldReturn_Count_Count_less_1() {
			// arrange
			//var mock = new Mock<IAgencyDataContext>();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 },
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository("", mock.Object);
			//var oldCount = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1 }).Count();
			//// Act
			//repository.Delete(2);
			//var newCount = repository.Get(new AffiliateNetworkListFilter() { Limit = 10, Page = 1 }).Count();
			//Assert.AreEqual(oldCount -1, newCount);
		}
		[TestMethod]
		[TestCategory("Affiliate Networks")]
		public void AffiliateNetwork_Post_Should_ShouldUpdate_RelatedEntity() {
			//arrange
			//var mock = new Mock<IAgencyDataContext>();
			////mock.SetupAllProperties();
			//mock.Setup(x => x.AffiliateNetworks)
			//		.Returns(new FakeDbSet<AffiliateNetwork>
			//		{
			//			new AffiliateNetwork { Id = 1 },
			//			new AffiliateNetwork { Id = 2 },
			//		});
			//var repository = new AgencyLib.AffiliateNetworkRepository("",mock.Object);
			//var temp = repository.Get(2);
			////// act
			//temp.Name = "hit";
			//repository.Put(temp);
			//var result = repository.Get(2);
			////// assert
			//Assert.AreEqual("hit", result.Name);
		}
	}
}
