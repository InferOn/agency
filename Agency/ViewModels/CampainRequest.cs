﻿using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

	[TsClass(Module = "ViewModels")]
	public class CampainRequest {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
		public int Id { get; set; }
		public decimal Bid { get; set; }
		public decimal DailyBudget { get; set; }
		public string PricingModel { get; set; }
		public string Platform { get; set; }
		public string Countries { get; set; }
		public string Statuses { get; set; }
		public System.DateTime StartedDateFrom { get; set; }
		public System.DateTime StartedDateTo { get; set; }
	}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}