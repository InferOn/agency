﻿using System.Collections.Generic;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	[TsClass(Module = "ViewModels")]
	public class TrafficSource {
		public int Id { get; set; }
		public string TrackingId { get; set; }
		public string Name { get; set; }
		public string AccountSettings { get; set; }
		public int Total_Rows { get; internal set; }
	}

	public class TrafficSourceRequest {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
		public string Sort { get; set; }
		public string SortDirection { get; set; }
		public int NetworkId { get; set; }
		public string Id { get; set; }
	}

	public class TrafficSourceResponse {
		public IEnumerable<TrafficSource> TrafficSources { get; set; }
		public int RowCount { get; set; }

	}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}