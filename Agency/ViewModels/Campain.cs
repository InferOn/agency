﻿using System.Collections.Generic;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	[TsClass(Module = "ViewModels")]
	public class Campain {
		public int campaignId { get; set; }
		public int trafficSourceId { get; set; }
		public string status { get; set; }
		public decimal? bidPrice { get; set; }
		public decimal? dailyBudget { get; set; }
		public string pricingModel { get; set; }
		public decimal? totalBudget { get; set; }
		public System.DateTime createdDate { get; set; }
		public bool? hasEndTime { get; set; }
		public bool? hasStartTime { get; set; }
		public string name { get; set; }
		public System.DateTime startDate { get; set; }
		public System.DateTime updateDate { get; set; }
		public string appName { get; set; }
		public string category { get; set; }
		public string destinationUrl { get; set; }
		public string platform { get; set; }
		public string countries { get; set; }
	};

	public class CampainResponse {
		public IEnumerable<Campain> Campains { get; set; }
		public int RowCount { get; set; }
	}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}