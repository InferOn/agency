﻿using System;
using System.Collections.Generic;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	[TsClass(Module = "ViewModels")]
	public class CampainHistory {
		public int Id { get; set; }
		public string Status { get; set; }
		public string NetworkName { get; set; }
		public int NetworkId { get; set; }
		public string Name { get; set; }
		public string Os { get; set; }
		public string Geo { get; set; }
		public decimal? Payout { get; set; }
		public decimal? DailyCap { get; set; }
		public decimal? TotalCap { get; set; }
		public DateTime? StartDate { get; set; }
		public string DeliveredOn { get; set; }
		public string DeliveredOnId { get; set; }
		public string ManagedBy { get; set; }
		public string Kpi { get; set; }
		public string Notes { get; set; }
	}

	public class HistoryCampainRequest {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Id { get; set; }
	}

	public class HistoryCampainResponse {
		public IEnumerable<CampainHistory> HistoryCampains { get; set; }
		public int RowCount { get; set; }
	}
	public class HistoryCampainSaveResponse { 
			
	}

	



#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}