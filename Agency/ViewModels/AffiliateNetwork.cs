﻿using System;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	[TsClass(Module = "ViewModels")]
	public class AffiliateNetwork {
		public int Id { get; set; }
		public string Name { get; set; }
		public string AccountSettings { get; set; }
		public string TrackingId { get; set; }
		public DateTime LastGet { get; set; }
		public int Active { get; set; }
		public string Note { get; set; }
	}

	public interface IPageable {
		int Page { get; set; }
		int Limit { get; set; }
	}

	public class AffiliateNetworkRequest : IPageable { 
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
	}


#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}