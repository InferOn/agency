﻿using AgencyLib.Data;
using System;
using System.Collections.Generic;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	[TsClass(Module = "ViewModels")]
	public class Offer {
		public int NetworkId { get; set; }
		public string NetworkName { get; set; }
		public int OfferId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Countries { get; set; }
		public DateTime? DateExpiration { get; set; }
		public string Currency { get; set; }
		public string ConversionType { get; set; }
		public string Status { get; set; }

		// payouts and budget
		public decimal InitialPayoutCap { get; set; }
		public decimal InitialConversionCap { get; set; }
		public decimal PayoutCap { get; set; }
		public decimal ConversionCap { get; set; }//RemainingLeads

		// app details
		public decimal AppRating { get; set; }
		public string AppOS { get; set; }
		public string AppPrice { get; set; }
		public int AppID { get; set; }
		public string AppDescription { get; set; }
		public string AppCategories { get; set; }

		public AffiliateNetwork Network { get; set; }
		public List<OfferPayout> Payouts { get; set; }
		public string TrackingId { get; set; }

		public string TrackingURL { get; set; }
		public string AppIconUrl { get; set; }
		
		public string PreviewUrl { get; set; }

		public AgencyLib.Data.eStatusValidation StatusValidation { get; set; }

		#region iconpeak 
		public string require_approval { get; set; }
		public string require_terms_and_conditions { get; set; }
		public string terms_and_conditions { get; set; }
		public string preview_url { get; set; }
		public string default_payout { get; set; }
		public string payout_type { get; set; }
		public string percent_payout { get; set; }
		public string featured { get; set; }
		public string allow_website_links { get; set; }
		public string allow_direct_links { get; set; }
		public string show_custom_variables { get; set; }
		public string show_mail_list { get; set; }
		public string dne_list_id { get; set; }
		public string email_instructions { get; set; }
		public string email_instructions_from { get; set; }
		public string email_instructions_subject { get; set; }
		public string has_goals_enabled { get; set; }
		public string default_goal_name { get; set; }
		public string use_target_rules { get; set; }
		public string is_expired { get; set; }
		public string dne_download_url { get; set; }
		public string dne_unsubscribe_url { get; set; }
		public string dne_third_party_list { get; set; }
		public string approval_status { get; set; }
		public int Total_Rows { get; internal set; }
		#endregion
	}
	public class OfferDetailRequest {
		public int NetworkId { get; set; }
		public int OfferId { get; set; }
	}
	public class OfferRequest {
		public int Page { get; set; }
		public int Limit { get; set; }
		public string Name { get; set; }
		public string ConversionType { get; set; }
		public DateTime DateExpiration { get; set; }
		public DateTime DateExpirationFrom { get; set; }
		public DateTime DateExpirationTo { get; set; }
		public decimal PayoutCap { get; set; }
		public decimal ConversionCap { get; set; }
		public string Status { get; set; }
		public string Sort { get; set; }
		public string SortDirection { get; set; }
		public int NetworkId { get; set; }
		public string Os { get; set; }
		public string Ioc { get; set; }
		public string Id { get; set; }
		public string ApprovalStatus { get; set; }
	}

	public class OfferResponse {
		public IEnumerable<OfferData> Offers { get; set; }
		public int RowCount { get; set; }
	}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}