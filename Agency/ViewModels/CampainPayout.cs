﻿using System;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member


	[TsClass(Module = "ViewModels")]
	public class CampainPayout {
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public int CampainId { get; set; }
		public decimal Payout { get; set; }
	}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}