﻿using System;
using System.Collections.Generic;
using TypeLite;

namespace Agency.ViewModels {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

	[TsClass(Module = "ViewModels")]
	public class NetworkRequest {
		public int Id { get; set; }
		public string Name { get; set; }
		public string AccountSettings { get; set; }
		public string TrackingId { get; set; }
		public bool Active { get; set; }
		public DateTime LastGet { get; set; }
		public string Note { get; set; }
	}

	[TsClass(Module = "ViewModels")]
	public class AffiliateNetworkResponse { 
		public IEnumerable<ViewModels.AffiliateNetwork> Networks { get; set; }
		public int RowCount { get; set; }
}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}