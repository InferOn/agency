﻿namespace Agency {
	"use strict";
	export namespace Services {
		"use strict";
		export class AuthInterceptorServiceFactory {
			constructor(private $q, private $location, private localStorageService) {
			}
			request = (config) => {
				//config.headers = config.headers || {};
				
				const authData = this.localStorageService.get("authorizationData");
				if (authData) {
					//config.headers.Authorization = "beaerer " + authData.token;
					config.headers = {
						"Authorization": "beaerer " + authData.token,
						"Accept": "application/json",
						"Content-Type": "application/json"
					};
				}
				return config;
			}
			responseError = (rejection) => {
				if (rejection.status === 401) {
					this.$location.path("/login/"); 
				}
				return this.$q.reject(rejection);
			}
		}
	}
}
app.factory("authInterceptorService", ["$q", "$location", "localStorageService", ($q, $location, localStorageService) => {
		let authInterceptorServiceFactory = new Agency.Services.AuthInterceptorServiceFactory($q, $location, localStorageService);
		return authInterceptorServiceFactory;
}]);
