﻿namespace Agency {
	"use strict";
	export namespace Services {
		"use strict";
		export class FetchRequestArguments {
			page: number;
			limit: number;
			tags: any[];
			sort: string;
			expirationDateAmount: any;
			expirationDateRange: any;
			statusFilter: string;
			network: any;
			os: any;
			ioc: any;
			id: any;
			approvalStatus: any;

		}

		export class CampainFetchRequestArguments extends FetchRequestArguments {
			constructor() {
				super();
			}
			bid: any;
			dailyBudget: any;
			pricingModel: any;
			platform: any;
			countries: any;
			startedDateFrom: any;
			startedDateTo: any;
			statuses: any;
		}

		export class HistoryCampainFetchRequestArguments extends FetchRequestArguments {

		}

		export class UserFetchRequestArguments extends FetchRequestArguments {}

		export abstract class FetchRequest {
			protected request: string;
			constructor(protected apiURL: string, protected args: FetchRequestArguments) {
			}
			protected applyTags = (tags: any) => {
				if (tags && (tags as any[]).length > 0) {
					this.request += "&request.name=" + (tags as any[]).reduce((p: any, c: any, cidx: number, src: any[]) => {
						return p + "%" + c;
					});
				}
				return this.request;
			};
			protected applySort = (args: any) => {
				if (args.sort && args.sort.text) {
					const split = args.sort.text.split(" ");
					this.request += ("&request.sort=" + split[0]);
					if (split[1]) {
						this.request += "&request.SortDirection=" + split[1];
					}
				}
				return this.request;
			};
			abstract resolve;
		}

		export class OffersFetchDetailRequest extends FetchRequest {
			constructor(apiURL: string, networkId: number, offerId: number) {
				super(apiURL, null);
				this.request = apiURL + "offers/" + networkId + "/" + offerId;
			}
			resolve = () => {
				return this.request;
			};
		}

		export class UserFetchRequest extends FetchRequest {
			constructor(apiURL: string, args: UserFetchRequestArguments) {
				super(apiURL, args);
				this.request = apiURL + "Auth/Users/";
			}
			resolve = (): string => {
				let __args = this.args as UserFetchRequestArguments;
				return this.request;
			};
		}

		export class HistoryCampainsFetchRequest extends FetchRequest {
			constructor(apiURL: string, args: HistoryCampainFetchRequestArguments) {
				super(apiURL, args);
				this.request = apiURL + "historyCampain/?request.page=" + args.page + "&request.limit=" + args.limit;
			}
			resolve = (): string => {
				let __args = this.args as HistoryCampainFetchRequestArguments;
				return this.request;
			};
		}

		export class CampainsFetchRequest extends FetchRequest {
			constructor(apiURL: string, args: CampainFetchRequestArguments) {
				super(apiURL, args);
				this.request = apiURL + "campains/?request.page=" + args.page + "&request.limit=" + args.limit;
			}
			resolve = (): string => {
				let __args = this.args as CampainFetchRequestArguments;
				this.applyTags(__args.tags);
				this.applyID(__args.id);
				this.applyBid(__args.bid);
				this.applyDailyBudget(__args.dailyBudget);
				this.applyPricingModel(__args.pricingModel);
				this.applyPlatform(__args.platform);
				this.applyCountries(__args.countries);
				this.applyStartedDate(__args.startedDateFrom, __args.startedDateTo);
				this.applyStatuses(__args.statuses);
				return this.request;
			};
			protected applyBid = (bid: string) => {
				if (bid) {
					this.request += "&request.bid=" + bid;
				}
			};
			protected applyID = (id: string) => {
				if (id) {
					this.request += "&request.id=" + id;
				}
			};
			protected applyDailyBudget = (db: any) => {
				if (db) {
					this.request += "&request.dailyBudget=" + db;
				}
			};
			protected applyPricingModel = (pricingModels: any) => {
				if (pricingModels && (pricingModels as any[]).length > 0) {
					let result = pricingModels
						.map((item: any) => {
							return item.Value;
						})
						.reduce((p: any, v: any) => {
							return p + "," + v;
						});
					this.request += "&request.pricingModel=" + result;
				}
			};
			protected applyPlatform = (platform: any) => {
				if (platform) {
					this.request += "&request.platform=" + platform.Value;
				}
			};
			protected applyCountries = (countries: any) => {
				if (countries) {
					this.request += "&request.countries=" + countries;
				}
			};
			protected applyStartedDate = (from: Date, to: Date) => {
				if (from) {
					this.request += "&request.startedDateFrom=" + from.toISOString();
				}
				if (to) {
					this.request += "&request.startedDateTO=" + to.toISOString();
				}
			};
			protected applyStatuses = (statuses: string) => {
				this.request += "&request.statuses=" + statuses;
			};
		}

		export class OffersFetchRequest extends FetchRequest {
			constructor(apiURL: string, args: FetchRequestArguments) {
				super(apiURL, args);
				this.request = apiURL + "offers/?request.page=" + args.page + "&request.limit=" + args.limit;
			}
			protected applyExpiryDateFilter = (args: any) => {
				if (args) {
					const now = new Date();
					let fromDate = new Date();
					let toDate = new Date();
					fromDate.setDate(now.getDate() + args[0]);
					toDate.setDate(now.getDate() + 1);
					this.request += "&request.dateExpirationFrom=" + fromDate.toISOString();
					this.request += "&request.dateExpirationTo=" + toDate.toISOString();
				}
			};
			protected applyExpiryDateRangeFilter = (arg: any) => {
				if (arg) {
					if (arg.from) {
						this.request += "&request.dateExpirationFrom=" + arg.from.toISOString();
					}
					if (arg.to) {
						this.request += "&request.dateExpirationTo=" + arg.to.toISOString();
					}
				}
			};
			protected applyStatusFilter = (arg: string) => {
				if (arg) {
					this.request += "&request.status=" + arg;
				}
			};
			protected applyNetwork = (network: any) => {
				if (network) {
					this.request += "&request.networkId=" + network.Id;
				}
			};
			protected applyCountry = (ioc: any) => {
				if (ioc) {
					this.request += "&request.ioc=" + ioc;
				}
			};

			protected applyOs = (os: any) => {
				if (os) {
					this.request += "&request.os=" + os.Value;
				}
			};

			protected applyID = (id: any) => {
				if (id) {
					this.request += "&request.id=" + id;
				}
			};

			protected applyApprovalStatus = (approvalStatus: any) => {
				if (approvalStatus) {
					this.request += "&request.approvalStatus=" + approvalStatus;
				}
			};

			resolve = (): string => {
				this.applyExpiryDateFilter(this.args.expirationDateAmount);
				this.applyExpiryDateRangeFilter(this.args.expirationDateRange);
				this.applyStatusFilter(this.args.statusFilter);
				this.applyTags(this.args.tags);
				this.applyNetwork(this.args.network);
				this.applyOs(this.args.os);
				this.applyCountry(this.args.ioc);
				this.applyID(this.args.id);
				this.applyApprovalStatus(this.args.approvalStatus);
				this.applySort(this.args);
				return this.request;
			};
		}

		export class TrafficSourceFetchRequest extends FetchRequest {
			constructor(apiURL: string, args: FetchRequestArguments) {
				super(apiURL, args);
				this.request = apiURL + "trafficsources/?request.page=" + args.page + "&request.limit=" + args.limit;
			}
			resolve = (): string => {
				this.applySort(this.args);
				return this.request;
			};
		}
	}
}