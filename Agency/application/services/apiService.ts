﻿namespace Agency {
	"use strict";
	export namespace Services {
		"use strict";
		export class ApiService {
			static $inject = ["$http", "ENV_VARS", "localStorageService", "$location", "$q", "ISO3166"];
			constructor(private $http: ng.IHttpService,
				private ENV_VARS: any,
				private localStorageService: any,
				private $location: ng.ILocationService,
				private $q: ng.IQService,
				private ISO3166: any) { }
			FetchNetworks = (networkId: string = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) => {
				let token = "";
				const authData = this.localStorageService.get("authorizationData");
				if (authData) {
					token = "Bearer " + authData.token;
				}
				return this.$http({
					url: this.ENV_VARS.apiURL + "AffiliateNetworks/" + networkId,
					method: "GET",
					headers: {
						"Authorization": token,
						"Accept": "application/json",
						"Content-Type": "application/json",
					}
				} as ng.IRequestConfig);
			};

			SaveNewNetwork = (network: ViewModels.NetworkRequest) => {
				let p = 0;
				return this.$http({
					url: this.ENV_VARS.apiURL + "AffiliateNetworks/",
					method: "POST",
					data: network
				} as ng.IRequestConfig);
			};
			EditNetwork = (network: ViewModels.NetworkRequest) => {
				let n = (network as any);
				n.Active = network.Active ? 1 : 0;
				return this.$http({
					url: this.ENV_VARS.apiURL + "AffiliateNetworks/" + network.Id,
					method: "PUT",
					data: network
				} as ng.IRequestConfig);
			};
			DeleteNetworks = (networkIds: any[]) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "AffiliateNetworks/Delete",
					method: "POST",
					data: networkIds,
				} as ng.IRequestConfig);
			}

			FetchOffersByNetwork = (networkId: string) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "Offers/" + networkId,
					method: "GET"
				} as ng.IRequestConfig);
			};
			FetchOfferDetail = (networkId: any, offerId: any) => {
				const token = this.getLocalAuthToken();
				return this.$http({
					url: encodeURI(new OffersFetchDetailRequest(this.ENV_VARS.apiURL, networkId, offerId).resolve()),
					method: "GET",
					headers: {
						"Authorization": token,
						"Accept": "application/json",
						"Content-Type": "application/json",
					}
				} as ng.IRequestConfig).catch((rejection: any) => {
					if (rejection.status === 401) {
						this.$location.path("/login/");
					}
					return this.$q.reject(rejection);
				});
			};
			FetchOffers = (args: Agency.Services.FetchRequestArguments) => {
				const token = this.getLocalAuthToken();
				return this.$http({
					url: encodeURI(new OffersFetchRequest(this.ENV_VARS.apiURL, args).resolve()),
					method: "GET",
					headers: {
						"Authorization": token,
						"Accept": "application/json",
						"Content-Type": "application/json",
					}
				} as ng.IRequestConfig).catch((rejection: any) => {
					if (rejection.status === 401) {
						this.$location.path("/login/");
					}
					return this.$q.reject(rejection);
				});
			};

			FetchOfferCreativities = (networkId: number, offerId: number) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "offers/creativities/" + networkId + "/" + offerId,
					method: "GET"
				} as ng.IRequestConfig);
			};
			RequestOfferAccess = (networkId: number, offerId: number) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "offers/requestAccess/" + networkId + "/" + offerId,
					method: "GET"
				} as ng.IRequestConfig);
			};
			FetchOffersByName = (name: string) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "offers/?request.name=" + name,
					method: "GET"
				} as ng.IRequestConfig);
			};
			FetchTrafficSources = (args: Agency.Services.FetchRequestArguments) => {
				const token = this.getLocalAuthToken();
				return this.$http({
					url: encodeURI(new TrafficSourceFetchRequest(this.ENV_VARS.apiURL, args).resolve()),
					method: "GET",
					headers: {
						"Authorization": token,
						"Accept": "application/json",
						"Content-Type": "application/json",
					}
				} as ng.IRequestConfig).catch((rejection: any) => {
					if (rejection.status === 401) {
						this.$location.path("/login/");
					}
					return this.$q.reject(rejection);
				});
			};
			SaveNewTrafficSource = (trafficSource: ViewModels.TrafficSource) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "TrafficSources/",
					method: "POST",
					data: trafficSource
				} as ng.IRequestConfig);
			};
			EditTrafficSource = (trafficSource: ViewModels.TrafficSource) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "TrafficSources/" + trafficSource.Id,
					method: "PUT",
					data: trafficSource
				} as ng.IRequestConfig);
			};

			FetchCampains = (args: Agency.Services.CampainFetchRequestArguments) => {
				return this.$http({
					url: encodeURI(new CampainsFetchRequest(this.ENV_VARS.apiURL, args).resolve()),
					method: "GET"
				} as ng.IRequestConfig);
			};
			FetchCampain = (campainId: number) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "Campains/Requests/" + campainId,
					method: "GET"
				} as ng.IRequestConfig);
			};
			SaveNewCampain = (campain: ViewModels.CampainRequest) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "Campains/",
					method: "POST",
					data: campain
				} as ng.IRequestConfig);
			};
			EditCampain = (campain: ViewModels.CampainRequest) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "Campains/" + campain.Id,
					method: "PUT",
					data: campain
				} as ng.IRequestConfig);
			};
			DeleteCampain = (id: number) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "Campains/" + id,
					method: "DELETE",
				} as ng.IRequestConfig);
			};


			FetchHistoryCampains = (args: Agency.Services.HistoryCampainFetchRequestArguments) => {
				return this.$http({
					url: encodeURI(new HistoryCampainsFetchRequest(this.ENV_VARS.apiURL, args).resolve()),
				} as ng.IRequestConfig);
			};
			UpdateHistoryCampain = (campain: any) => {
				let nName = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				let nId = 0;
				if (campain.Network) {
					nName = campain.Network.Name;
					nId = campain.Network.Id;
				}
				let cOs = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Os) {
					cOs = campain.Os
						.map((item) => {
							return item.Value;
						})
						.reduce((p, c) => {
							return p + "|" + c;
						});
				}
				let cGeo = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Geo) {
					cGeo = campain.Geo
						.map((item) => {
							return this.ISO3166.countryToCode[item];
						})
						.reduce((p, c) => {
							return p + "|" + c;
						});
				}
				let csd = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.StartDate) {
					csd = (campain.StartDate as Date).toISOString();
				}
				let status = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Status) {
					status = campain.Status;
				}
				let delOn = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				let delOnId = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.DeliveredOn) {
					delOn = campain.DeliveredOn
						.map((item) => { return item.Name; })
						.reduce((p, c) => { return p + "|" + c; });

					delOnId = campain.DeliveredOn
						.map((item) => { return item.Id; })
						.reduce((p, c) => { return p + "|" + c; });
				}
				let kpi = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Kpi) {
					kpi = campain.Kpi;
				}
				let notes = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Notes) {
					notes = campain.Notes;
				}
				let managedBy = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.ManagedBy) {
					managedBy = campain.ManagedBy.UserName;
				}
				let dto = {
					Id: campain.Id,
					Name: campain.Name,
					NetworkName: nName,
					NetworkId: nId,
					Payout: campain.Payout,
					DailyCap: campain.DailyCap,
					TotalCap: campain.TotalCap,
					Os: cOs,
					Geo: cGeo,
					StartDate: csd,
					Status: status,
					DeliveredOn: delOn,
					DeliveredOnId: delOnId,
					ManagedBy: managedBy,
					Kpi: kpi,
					Notes: notes
				};
				return this.$http({
					url: this.ENV_VARS.apiURL + "historyCampain",
					method: "PUT",
					data: dto
				} as ng.IRequestConfig);
			}
			SaveNewHistoryCampain = (campain: any) => {
				let nName = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				let nId = 0;
				if (campain.Network) {
					nName = campain.Network.Name;
					nId = campain.Network.Id;
				}
				let cOs = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Os) {
					cOs = campain.Os
						.map((item) => {
							return item.Value;
						})
						.reduce((p, c) => {
							return p + "|" + c;
						});
				}
				let cGeo = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Geo) {
					cGeo = campain.Geo
						.map((item) => {
							return this.ISO3166.countryToCode[item];
						})
						.reduce((p, c) => {
							return p + "|" + c;
						});
				}
				let csd = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.StartDate) {
					csd = campain.StartDate.toLocaleDateString();
				}
				let status = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Status) {
					status = campain.Status;
				}
				let delOn = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				let delOnId = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.DeliveredOn) {
					delOn = campain.DeliveredOn
						.map((item) => { return item.Name; })
						.reduce((p, c) => { return p + "|" + c; });

					delOnId = campain.DeliveredOn
						.map((item) => { return item.Id; })
						.reduce((p, c) => { return p + "|" + c; });
				}
				let kpi = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Kpi) {
					kpi = campain.Kpi;
				}
				let notes = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.Notes) {
					notes = campain.Notes;
				}
				let managedBy = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (campain.ManagedBy) {
					managedBy = campain.ManagedBy.UserName;
				}
				let dto = {
					Name: campain.Name,
					NetworkName: nName,
					NetworkId: nId,
					Payout: campain.Payout,
					DailyCap: campain.DailyCap,
					TotalCap: campain.TotalCap,
					Os: cOs,
					Geo: cGeo,
					StartDate: csd,
					Status: status,
					DeliveredOn: delOn,
					DeliveredOnId: delOnId,
					ManagedBy: managedBy,
					Kpi: kpi,
					Notes: notes
				};
				return this.$http({
					url: this.ENV_VARS.apiURL + "historyCampain",
					method: "POST",
					data: dto
				} as ng.IRequestConfig);
			}
			DeleteHistoryCampains = (campaignIds: any[]) => {
				return this.$http({
					url: this.ENV_VARS.apiURL + "historyCampain/Delete",
					method: "POST",
					data: campaignIds,
				} as ng.IRequestConfig);
			}
			FetchUsers = () => {
				return this.$http({
					url: encodeURI(new UserFetchRequest(this.ENV_VARS.apiURL, {} as UserFetchRequestArguments).resolve()),

				} as ng.IRequestConfig);
			}
			private getLocalAuthToken = () => {
				let result = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				const authData = this.localStorageService.get("authorizationData");
				if (authData) {
					result = "Bearer " + authData.token;
				}
				return result;
			};
		}
	}
}
app.service("apiService", Agency.Services.ApiService);
