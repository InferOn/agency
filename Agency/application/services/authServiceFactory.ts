﻿namespace Agency {
	"use strict";
	export namespace Services {
		"use strict";
		export interface Iauthentication {
			isAuth: boolean;
			userName: string;
		}
		export class AuthServiceFactory {
			authentication = {
				isAuth: false,
				userName: ""
			} as Iauthentication;
			constructor(private ENV_VARS, private localStorageService, private $http: ng.IHttpService, private $q: ng.IQService, private Amplify: Agency.Services.IAmplify) {
			}
			logOut = () => {
				this.localStorageService.remove("authorizationData");
				this.authentication.isAuth = false;
				this.authentication.userName = "";
			}
			saveRegistration = (registration) => {
				this.logOut();
				return this.$http.post(this.ENV_VARS.apiURL + "Auth/register", registration).then((response) => {
					return response;
				});
			};
			login = (loginData) => {
				let data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
				let deferred = this.$q.defer();
				this.$http.post(this.ENV_VARS.apiURL + "Auth/token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } })
					.success((response) => {
						this.localStorageService.set("authorizationData", { token: (response as any).access_token, userName: loginData.userName });
						this.authentication.isAuth = true;
						this.authentication.userName = loginData.userName;
						this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_SUCCESSFULLY_LOGGED_IN, [this.authentication]);

						deferred.resolve(response);
					}).error((err, status) => {
						this.logOut();
						deferred.reject(err);
					});
				return deferred.promise;
			}
			fillAuthData = () => {
				const authData = this.localStorageService.get("authorizationData");
				if (authData) {
					this.authentication.isAuth = true;
					this.authentication.userName = authData.userName;
				}
			}
		}
	}
}
app.factory("authService", ["$http", "$q", "localStorageService", "ENV_VARS", "Amplify",
		($http: ng.IHttpService,
			$q: ng.IQService,
			localStorageService,
			ENV_VARS,
			Amplify: Agency.Services.IAmplify) => {
		let authServiceFactory = new Agency.Services.AuthServiceFactory(ENV_VARS, localStorageService, $http, $q, Amplify); //SCORE: 20
		return authServiceFactory;
}]);
