﻿namespace Agency {
	"use strict";

	export namespace Services {
		export interface IAmplify {
			publish(topic: string, args: any[]): boolean;
			subscribe(topic: string, context: any, callback: Function, priority?: number): void;
			unsubscribe(topic: string, callback: Function): void;
		}
	}
}