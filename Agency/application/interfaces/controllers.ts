﻿namespace Agency {
	"use strict";

	export interface IColumnDefinition {
		headerName?: string;
		width?: number;
		checkboxSelection?: boolean;
		suppressSorting?: boolean;
		suppressMenu?: boolean;
		pinned?: boolean;
		field?: string;
		headerClass?: string;
		cellClass?: string;
	}
	export namespace Controllers {
		"use strict";

		export namespace Scopes {
			export interface IBaseControllerScope extends ng.IScope {
				onCopySuccess: Function;
				name: string;
			}

			export interface IFiltersControllerScope extends IBaseControllerScope { }
			export interface ICloseFiltersControllerScope extends IBaseControllerScope { }
			export interface IListFiltersControllerScope extends IBaseControllerScope { }
			export interface IBackFiltersControllerScope extends IBaseControllerScope { expirationDateLf: any; }
			export interface ICalendarFiltersControllerScope extends IBaseControllerScope { }
			export interface IDateControllerScope extends IBaseControllerScope { }
			export interface IDateTimeFilterContainer {
				from: Date;
				to: Date;
				ExpirationDateAmount: any[];
			}
			export interface IHeaderControllerScope extends IBaseControllerScope { }
			export interface IMainControllerScope extends IBaseControllerScope {
				Offers: any[];
			}
			export interface ILoginControllerScope extends IBaseControllerScope {
				authentication: Agency.Services.Iauthentication;
				loginInfo: any;
				clientLogin: any;
			}
			export interface IMenuControllerScope extends IBaseControllerScope {
				isAuth: boolean;
				userName: string;
				location: any;

			}
			export interface INavBarControllerScope extends IBaseControllerScope { }

			export interface IBaseTableControllerScope extends IBaseControllerScope {
				rowCount: string;
				statusFilter: string;
				Search_ID: any;
				Tags: any[];
				CreativityRequest: any;
				expirationDateFiltersContainer: IDateTimeFilterContainer;
				paginatorCallback: Function;
				Search_Network: any;
				Search_OS: any;
				Search_Country: any;
				Search_StartedDateFrom: any;
				Search_StartedDateTo: any;
				Search_Statuses: any;
				getDetailValue: Function;
				requiredOfferAccess: Function;
				requestOfferAccess: Function;
				requestedOfferAccessIcon: Function;
				requestedOfferAccessColor: Function;
				requestOfferAccessStatus: Function;
				getCreativities: Function;
				Creativities: any;
				CreativitiesLoad: boolean;
				ShowRetryCreativitiesLoad: boolean;
				CreativitiesErrorMessage: string;
				Search_Bid: any;
				Search_Daily_budget: any;
				Search_Pricing_model: any;
				Search_RequestAccessStatus: any;
				rowSelectionClick: Function;
			}
			export interface IOfferTableControllerScope extends IBaseTableControllerScope {
				expirationDateFiltersContainer: IDateTimeFilterContainer;
				paginatorCallback: Function;
				Search_Network: any;
				Search_OS: any;
				Search_Country: any;
				getDetailValue: Function;
				requiredOfferAccess: Function;
				requestOfferAccess: Function;
				requestedOfferAccessIcon: Function;
				requestedOfferAccessColor: Function;
				requestOfferAccessStatus: Function;
				getCreativities: Function;
				Creativities: any;
				CreativitiesLoad: boolean;
				ShowRetryCreativitiesLoad: boolean;
				CreativitiesErrorMessage: string;
			}
			export interface ICampainTableControllerScope extends IBaseTableControllerScope {
				Search_Bid: any;
				Search_Daily_budget: any;
				Search_Pricing_model: any;
			}
			export interface ISearchOffersControllerScope extends IBaseControllerScope {
				Networks: ViewModels.AffiliateNetwork[];
				OSS: any[];
				Countries: any[];
				OfferRequestStatuses: any[];
			}
			export interface ISearchCampainControllerScope extends IBaseControllerScope {
				PricingModels: any[];
				Categories: any[];
				Platforms: any[];
				Countries: any[];
				Statuses: any[];
			}
			export interface IHistoryCampainTableControllerScope extends IBaseTableControllerScope {
				historyCampains: any[];
				rowSelectionClick: Function;
			}
			export interface IHistoryCampainModalControllerScope extends ng.IScope {
				historyCampain: any;
				isUpdate: boolean;
				Title: string;
				Countries: any[];
				Statuses: any[];
				Platforms: any[];
				Users: any[];
				Networks: any;
				TrafficSources: ViewModels.TrafficSource[];
				closeDialog: Function;
				saveAndCloseDialog: Function;
				update: Function;
			}
			export interface INetworkModalControllerScope extends ng.IScope {
				network: any;
				isUpdate: boolean;	
				Title: string;
				saveAndCloseDialog: Function;
				update: Function;
				closeDialog: Function;
			}
		}
	}
}