﻿namespace Agency {
	"use strict";

	export class menuController {
		static $inject = ["$scope", "$timeout", "$mdSidenav", "Amplify", "$location", "authService"];
		Title: string = "Campaign manager";
		constructor(
			private $scope: Agency.Controllers.Scopes.IMenuControllerScope,
			private $timeout,
			private $mdSidenav: ng.material.ISidenavService,
			private Amplify: Agency.Services.IAmplify,
			private $location: ng.ILocationService,
			private authService: Agency.Services.AuthServiceFactory
		) {
			this.$scope.isAuth = this.authService.authentication.isAuth;
			this.$scope.userName = this.authService.authentication.userName;
			this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_SUCCESSFULLY_LOGGED_IN, this,
				(args: any) => {
					this.$scope.isAuth = args[0].isAuth;
					this.$scope.userName = args[0].userName;
				}
			);
		}

		logout = () => {
			this.authService.logOut();
			this.$scope.isAuth = this.authService.authentication.isAuth;
			this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_SUCCESSFULLY_LOGGED_OUT, []);
			this.$location.path("/login/");
		}

		goto = (location: string) => {
			this.$location.path(location);
		}

		openLeftNavPanel = () => {
			this.$mdSidenav("left").open();
		};
		closeLeftNavPanel = () => {
			this.$mdSidenav("left").close();
		};
		openRightNavPanel = () => {
			this.$mdSidenav("right").open();
		};
		closeRightNavPanel = () => {
			this.$mdSidenav("right").close();
		};
	}
}
app.controller("menuController", Agency.menuController);

namespace Agency {

	export class sidenavController {
		static $inject = ["$scope", "$timeout", "$mdSidenav", "Amplify", "$location", "authService", "$rootScope"];
		constructor(
			private $scope: Agency.Controllers.Scopes.IMenuControllerScope,
			private $timeout,
			private $mdSidenav: ng.material.ISidenavService,
			private Amplify: Agency.Services.IAmplify,
			private $location: ng.ILocationService,
			private authService: Agency.Services.AuthServiceFactory,
			private $rootScope: ng.IRootScopeService
		) {
		}

		goto = (location: string) => {
			this.$location.path(location);
			this.$scope.location = location;
		}
	}
}
app.controller("sidenavController", Agency.sidenavController);
