﻿namespace Agency {
	"use strict";

	export class navigationbarController {
		static $inject = ["$timeout", "Amplify", "$location"];
		constructor(
			private $timeout,
			private Amplify: Agency.Services.IAmplify,
			private $location: ng.ILocationService,
		) {
		}
	}
}

app.controller("navbarController", Agency.navigationbarController);
