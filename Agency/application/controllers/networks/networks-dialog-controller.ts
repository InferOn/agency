﻿namespace Agency {
	"use strict";
	export namespace Networks {
		export class NetworksDialogController {
			private fillDetail = (args): void => {
				this.$scope.network.Id = args.items[0].filter((current) => {
					return current.columnKey === "Id";
				})[0].value;
				this.$scope.network.Name = args.items[0].filter((current) => {
					return current.columnKey === "Name";
				})[0].value;
				this.$scope.network.Note = args.items[0].filter((current) => {
					return current.columnKey === "Note";
				})[0].value;
				this.$scope.network.LastGet = new Date(args.items[0].filter((current) => {
					return current.columnKey === "LastGet";
				})[0].value);
				this.$scope.network.Active = args.items[0].filter((current) => {
					return current.columnKey === "Active";
				})[0].value === 1;
			}
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166", "$mdDialog", "locals", "$moment", "$q"];
			constructor(
				private $scope: Agency.Controllers.Scopes.INetworkModalControllerScope,
				private $mdToast: any,
				private apiService: Agency.Services.ApiService,
				private Amplify: Services.IAmplify,
				private ISO3166: any,
				private $mdDialog: ng.material.IDialogService,
				private locals: any,
				private $moment: any,
				private $q: angular.IQService
			) {
				this.$scope.network = {};
				this.$scope.isUpdate = false;
				this.$scope.update = this.update;
				this.$scope.closeDialog = this.closeDialog;
				this.$scope.saveAndCloseDialog = this.saveAndCloseDialog;
				if (locals) {
					if (locals.title) {
						this.$scope.Title = locals.title;
					}
					if (locals.items && locals.items[0].length > 0) {
						this.$scope.isUpdate = true;
						this.fillDetail(locals);
					}
				}
			} 
			closeDialog = () => {
				this.$scope.network = {};
				return this.$mdDialog.hide();
			}
			update = () => {
				return this.apiService.EditNetwork(this.$scope.network)
					.then((result: any) => {
						this.Amplify.publish(Agency.Utils.Constants.Global.Events.REFRESH_GRID, []);
					});
			}
			saveAndCloseDialog = () => {
				this.apiService.SaveNewNetwork(this.$scope.network)
					.then((result: any) => {
						this.$scope.network = {};
						this.Amplify.publish(Agency.Utils.Constants.Global.Events.REFRESH_GRID, []);
						return this.$mdDialog.hide();
					});
			}
		}
	}
}
app.controller("NetworksDialogController", Agency.Networks.NetworksDialogController);
