﻿namespace Agency {
	"use strict";
	export namespace Networks {
		export class tableController extends Agency.baseTableController {
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166", "$mdDialog"];
			constructor(
				$scope: Agency.Controllers.Scopes.IBaseTableControllerScope,
				$mdToast: any,
				apiService: Agency.Services.ApiService,
				Amplify: Services.IAmplify,
				ISO3166: any,
				private $mdDialog: ng.material.IDialogService
			) {
				super($scope, $mdToast, apiService, Amplify, ISO3166);
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_ROW_ADDCLICK, this, this.openCreateNetworkModal);
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.REFRESH_GRID, this, this.loadPage);
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_ROW_CLICK, this, this.openEditNetworkModal);
				const amp = this.Amplify;
				this.$scope.$on("$destroy", function handler() {
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_ROW_ADDCLICK, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.REFRESH_GRID, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_ROW_CLICK, () => { });
				});
			}
			openCreateNetworkModal = (args) => {
				this.$mdDialog.show({
					locals: { items: null, title: "add new network" },
					clickOutsideToClose: false,
					escapeToClose: false,
					controller: Agency.Networks.NetworksDialogController,
					controllerAs: "ndCtrl",
					templateUrl: "./app/templates/networks/dialog.html",
				} as ng.material.IDialogOptions);
			}
			openEditNetworkModal = (args) => {
				this.$mdDialog.show({
					locals: { items: args, title: "edit network" },
					clickOutsideToClose: false,
					escapeToClose: false,
					controller: Agency.Networks.NetworksDialogController,
					controllerAs: "ndCtrl",
					templateUrl: "./app/templates/networks/dialog.html",
				} as ng.material.IDialogOptions);
			}
			deleteRowCallBack(rowIds: any[]) {
				if (rowIds && rowIds.length > 0) {
					this.apiService.DeleteNetworks(rowIds);
				}
			}
			paginatorCallback(page: number, limit: number, options: any) {
				const fields = [
					"Name",
					"Id",
					"LastGet",
					"Note",
					"Active"
				];
				let sortString = "";
				if (options && options.columnSort) {
					const sortmap = options.columnSort
						.map((mi, idx) => {
							if (mi.sort) {
								return {
									text: fields[idx] + " " + mi.sort
								};
							} else {
								return {
									text: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY
								};
							}
						})
						.filter((fi) => { return fi.text !== ""; });
					sortString = (sortmap.length > 0 ? sortmap : [""]).reduce((p, c) => { if (p !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) { return p + "," + c; }; });
				}
				return this.apiService.FetchNetworks()
					.then((response?: ng.IRequestConfig) => {
						this.loaded = true;
						return {
							results: response.data.Networks,
							totalResultCount: response.data.RowCount
						};
					});
			}
			selectedRowCallback = (rows: number[]) => {
				this.selectedItems = rows;
			}
		}
	}
}
app.controller("networksTableController", Agency.Networks.tableController);
