﻿namespace Agency {
	export namespace HistoryCampains {
		export class TableController extends Agency.baseTableController {
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166", "$mdDialog"];
			historyCampains: any[] = [];
			
			constructor(
				$scope: Agency.Controllers.Scopes.IHistoryCampainTableControllerScope,
				$mdToast: any,
				apiService: Agency.Services.ApiService,
				Amplify: Services.IAmplify,
				ISO3166: any,
				private $mdDialog: ng.material.IDialogService
			) {
					super($scope, $mdToast, apiService, Amplify, ISO3166);
					this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_HISTORY_CAMPAIN_ROW_ADDCLICK, null, this.openCreateHistoryCampainModal);
					this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.REFRESH_GRID, null, this.loadPage);
					this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_HISTORY_CAMPAIN_ROW_CLICK, null, this.openEditHistoryCampainModal);
					const amp = this.Amplify;
					this.$scope.$on("$destroy", function handler() {
						amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_HISTORY_CAMPAIN_ROW_CLICK, () => { });
						amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_HISTORY_CAMPAIN_ROW_ADDCLICK, () => { });
						amp.unsubscribe(Agency.Utils.Constants.Global.Events.REFRESH_GRID, () => { });
					});
			}
			openCreateHistoryCampainModal = () => {
				this.$mdDialog.show({
					locals: { items: null, title: "add new campain" },
					clickOutsideToClose: false,
					escapeToClose: false,
					controller: Agency.HistoryCampains.HistoryCampainDialogController,
					controllerAs: "hcaCtrl",
					templateUrl: "./app/templates/historycampaigns/dialog.html",
				} as ng.material.IDialogOptions);
			}
			openEditHistoryCampainModal = (args) => {
				this.$mdDialog.show({
					locals: { items: args, title: "edit campain" },
					clickOutsideToClose: false,
					escapeToClose: false,
					controller: Agency.HistoryCampains.HistoryCampainDialogController,
					controllerAs: "hcaCtrl",
					templateUrl: "./app/templates/historycampaigns/dialog.html",
				} as ng.material.IDialogOptions);
			}
			deleteRowCallBack(rowIds: any[]) {
				if (rowIds && rowIds.length > 0) {
					this.apiService.DeleteHistoryCampains(rowIds);
				}
			}
			paginatorCallback(page: number, limit: number, options: any) {
				const fields = [
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.ID,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.STATUS,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.NETWORK_NAME,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.NAME,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.OS,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.GEO,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.PAYOUT,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.DAILY_CAP,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.TOTAL_CAP,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.START_DATE,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.DELIVERED_ON,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.MANAGED_BY,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.KPI,
					Agency.Utils.FieldDefinitions.CampainHistoryDefinition.NOTES,
				];
				let sortString = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (options && options.columnSort) {
					const sortmap = options.columnSort
						.map((mi: any, idx: any) => {
							if (mi.sort) {
								return {
									text: fields[idx] + Agency.Utils.Constants.Global.Recurrent.BLANK + mi.sort
								};
							} else {
								return {
									text: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY
								};
							}
						})
						.filter((fi: any) => { return fi.text !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY; });
					sortString = (sortmap.length > 0 ? sortmap : [Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY])
						.reduce((p: any, c: any) => {
							if (p !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) {
								return p + Agency.Utils.Constants.Global.Recurrent.COMMA + c;
							};
						});
				}
				return this.apiService.FetchHistoryCampains({} as Agency.Services.HistoryCampainFetchRequestArguments)
										.then((response?: ng.IRequestConfig) => {
						this.loaded = true;
						return {
							results: response.data.HistoryCampains,
							totalResultCount: response.data.RowCount
						};
					});
			}
			selectedRowCallback = (rows: any) => {}
		}
	}
}
app.controller("historyCampainsTableController", Agency.HistoryCampains.TableController);

