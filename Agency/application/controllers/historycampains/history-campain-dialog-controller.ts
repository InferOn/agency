﻿namespace Agency {
	"use strict";
	export namespace HistoryCampains {
		export class HistoryCampainDialogController {
			private fillDetail = (args): void => {
				this.$scope.historyCampain.Id = args.items[0].filter((current) => {
					return current.columnKey === "Id";
				})[0].value;
				this.$scope.historyCampain.Name = args.items[0].filter((current) => {
					return current.columnKey === "Name";
				})[0].value;
				this.$scope.historyCampain.Payout = args.items[0].filter((current) => {
					return current.columnKey === "Payout";
				})[0].value;
				this.$scope.historyCampain.DailyCap = args.items[0].filter((current) => {
					return current.columnKey === "DailyCap";
				})[0].value;
				this.$scope.historyCampain.TotalCap = args.items[0].filter((current) => {
					return current.columnKey === "TotalCap";
				})[0].value;
				this.$scope.historyCampain.Status = args.items[0].filter((current) => {
					return current.columnKey === "Status";
				})[0].value;
				this.$scope.historyCampain.Kpi = args.items[0].filter((current) => {
					return current.columnKey === "Kpi";
				})[0].value;
				this.$scope.historyCampain.Notes = args.items[0].filter((current) => {
					return current.columnKey === "Notes";
				})[0].value;

				let startDate = new Date(args.items[0].filter((current) => {
					return current.columnKey === "StartDate";
				})[0].value);
				this.$scope.historyCampain.StartDate = startDate;
				let username = args.items[0].filter((current) => {
					return current.columnKey === "ManagedBy";
				})[0].value;
				this.$scope.historyCampain.ManagedBy = this.$scope.Users.filter((u) => {
					return u.UserName === username;
				})[0];
				let networkName = args.items[0].filter((current) => {
					return current.columnKey === "NetworkName";
				})[0].value;
				this.$scope.historyCampain.Network = this.$scope.Networks.Networks.filter((n) => {
					return n.Name === networkName;
				})[0];
				this.$scope.historyCampain.DeliveredOn = this.$scope.TrafficSources.filter((t) => {
					return args.items[0].filter((current) => {
						return current.columnKey === "DeliveredOn";
					})[0].value.split("|").indexOf(t.Name) >= 0;
				});
				this.$scope.historyCampain.Geo = args.items[0].filter((current) => {
					return current.columnKey === "Geo";
				})[0].value.split("|").map((ioc) => {
					return this.ISO3166.getCountryName(ioc);
				});
				this.$scope.historyCampain.Os = this.$scope.Platforms.filter((p) => {
					return args.items[0].filter((current) => {
						return current.columnKey === "Os";
					})[0].value.split("|").indexOf(p.Value) >= 0;
				});
			}
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166", "$mdDialog", "locals", "$moment", "$q"];
			constructor(
				private $scope: Agency.Controllers.Scopes.IHistoryCampainModalControllerScope,
				private $mdToast: any,
				private apiService: Agency.Services.ApiService,
				private Amplify: Services.IAmplify,
				private ISO3166: any,
				private $mdDialog: ng.material.IDialogService,
				private locals: any,
				private $moment: any,
				private $q: angular.IQService
			) {
				this.$scope.isUpdate = false;
				this.$scope.historyCampain = {};
				this.$scope.update = this.update;
				this.$scope.Countries = ISO3166.codeToCountry;
				this.$scope.Statuses = Agency.Utils.Constants.Global.Enums.CampainStatuses;
				this.$scope.Platforms = Agency.Utils.Constants.Global.Enums.OSs;
				let networksPromise = this.apiService.FetchNetworks()
					.then((response?: ng.IRequestConfig) => {
						this.$scope.Networks = response.data;
					});
				let trafficSourcesPromise = this.apiService.FetchTrafficSources({} as Services.FetchRequestArguments)
					.then((response?: ng.IRequestConfig) => {
						this.$scope.TrafficSources = response.data.TrafficSources;
					});
				let usersPromise = this.apiService.FetchUsers()
					.then((response?: ng.IRequestConfig) => {
						this.$scope.Users = response.data;
					});
				$q.all([networksPromise, trafficSourcesPromise, usersPromise]).then(() => {
					if (locals) {
						if (locals.title) {
							this.$scope.Title = locals.title;
						}
						if (locals.items && locals.items[0].length > 0) {
							this.$scope.isUpdate = true;
							this.fillDetail(locals);
						}
					}
				});
				this.$scope.closeDialog = this.closeDialog;
				this.$scope.saveAndCloseDialog = this.saveAndCloseDialog;
			}
			closeDialog = () => {
				this.$scope.historyCampain = {};
				return this.$mdDialog.hide();
			}
			update = () => {
				return this.apiService.UpdateHistoryCampain(this.$scope.historyCampain)
					.then((result: any) => {
						this.Amplify.publish(Agency.Utils.Constants.Global.Events.REFRESH_GRID, []);
					});
			}
			saveAndCloseDialog = () => {
				this.apiService.SaveNewHistoryCampain(this.$scope.historyCampain)
					.then((result: any) => {
						this.$scope.historyCampain = {};
						this.Amplify.publish(Agency.Utils.Constants.Global.Events.REFRESH_GRID, []);
						return this.$mdDialog.hide();
					});
			}
		}
	}
}
app.controller("HistoryCampainDialogController", Agency.HistoryCampains.HistoryCampainDialogController);
