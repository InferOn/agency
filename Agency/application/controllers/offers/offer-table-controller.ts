namespace Agency {
	"use strict";
	export namespace Offers {
		export class offertableController extends Agency.baseTableController {
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166"];
			constructor(
				$scope: Agency.Controllers.Scopes.IOfferTableControllerScope,
				$mdToast: any,
				apiService: Agency.Services.ApiService,
				Amplify: Services.IAmplify,
				ISO3166: any
			) {
				super($scope, $mdToast, apiService, Amplify, ISO3166);
				this.loaded = false;
				this.$scope.expirationDateFiltersContainer = {} as Agency.Controllers.Scopes.IDateTimeFilterContainer;
				this.$scope.getDetailValue = this.getDetailValue;
				this.$scope.requiredOfferAccess = this.requiredOfferAccess;
				this.$scope.requestOfferAccess = this.requestOfferAccess;
				this.$scope.requestedOfferAccessIcon = this.requestedOfferAccessIcon;
				this.$scope.requestedOfferAccessColor = this.requestedOfferAccessColor;
				this.$scope.requestOfferAccessStatus = this.requestOfferAccessStatus;
				this.$scope.getCreativities = this.getCreativities;
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_CHANGE_SEARCH, this, (args: any[]) => {
					this.$scope.Search_Network = args[0];
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, this, (args: any[]) => {
					this.$scope.Search_OS = args[0];
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, this, (args: any[]) => {
					this.$scope.Search_Country = this.ISO3166.countryToCode[args[0]];
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, this, (args: any[]) => {
					this.$scope.Search_ID = args[0];
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this, (args: any[]) => {
					this.$scope.Tags = args;
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_DATE_SEARCH, this, (amount: any[]) => {
					if (amount) {
						this.$scope.expirationDateFiltersContainer.from = undefined;
						this.$scope.expirationDateFiltersContainer.to = undefined;
					}
					this.$scope.expirationDateFiltersContainer.ExpirationDateAmount = amount;
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_CUSTOMRANGE_DATE_SEARCH, this, (range: any[]) => {
					if (range) {
						this.$scope.expirationDateFiltersContainer.ExpirationDateAmount = undefined;
					}
					this.$scope.expirationDateFiltersContainer.from = range[0].from;
					this.$scope.expirationDateFiltersContainer.to = range[0].to;
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_STATUS_SEARCH, this, (val: any[]) => {
					this.$scope.statusFilter = val[0];
					this.loadPage();
				});
				Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_REQUEST_STATUS_CHANGE_SEARCH, this, (val: any[]) => {
					if (val[0].length && val[0].length > 0) {
						this.$scope.Search_RequestAccessStatus = val[0].map((item) => {
							return item.Value;
						}).reduce((p: any, c: any, cidx: number, src: any[]) => {
							return p + "," + c;
						});
					} else {
						this.$scope.Search_RequestAccessStatus = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
					}
					this.loadPage();
				});
				const amp = this.Amplify;
				this.$scope.$on("$destroy", function handler() {
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_NETWORK_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_DATE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_CUSTOMRANGE_DATE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_EXPIRY_STATUS_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_REQUEST_STATUS_CHANGE_SEARCH, () => { });
				});
			}
			paginatorCallback(page: number, limit: number, options: any) {
				this.$scope.ShowRetryCreativitiesLoad = false;
				this.$scope.CreativitiesLoad = false;
				const fields = [
					Agency.Utils.FieldDefinitions.OfferDefinition.NETWORK_ID,
					Agency.Utils.FieldDefinitions.OfferDefinition.OFFER_ID,
					Agency.Utils.FieldDefinitions.OfferDefinition.NETWORK_NAME,
					Agency.Utils.FieldDefinitions.OfferDefinition.NAME,
					Agency.Utils.FieldDefinitions.OfferDefinition.DESCRIPTION,
					Agency.Utils.FieldDefinitions.OfferDefinition.COUNTRIES,
					Agency.Utils.FieldDefinitions.OfferDefinition.DATE_EXPIRATION,
					Agency.Utils.FieldDefinitions.OfferDefinition.CURRENCY,
					Agency.Utils.FieldDefinitions.OfferDefinition.CONVERSION_TYPE,
					Agency.Utils.FieldDefinitions.OfferDefinition.STATUS,
					Agency.Utils.FieldDefinitions.OfferDefinition.INITIAL_PAYOUT_CAP,
					Agency.Utils.FieldDefinitions.OfferDefinition.INITIAL_CONVERSION_CAP,
					Agency.Utils.FieldDefinitions.OfferDefinition.PAYOUT_CAP,
					Agency.Utils.FieldDefinitions.OfferDefinition.CONVERSION_CAP,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_RATING,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_OS,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_PRICE,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_ID,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_DESCRIPTION,
					Agency.Utils.FieldDefinitions.OfferDefinition.APP_CATEGORIES,
					Agency.Utils.FieldDefinitions.OfferDefinition.PAYOUTS,
					Agency.Utils.FieldDefinitions.OfferDefinition.TRACKING_ID,
					Agency.Utils.FieldDefinitions.OfferDefinition.TRACKING_URL,
					Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS
				];
				let sortString = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (options && options.columnSort) {
					const sortmap = options.columnSort
						.map((mi, idx) => {
							if (mi.sort) {
								return {
									text: fields[idx] + Agency.Utils.Constants.Global.Recurrent.BLANK + mi.sort
								};
							} else {
								return {
									text: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY
								};
							}
						})
						.filter((fi) => { return fi.text !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY; });
					sortString = (sortmap.length > 0 ? sortmap : [Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY])
						.reduce((p, c) => {
							if (p !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) {
								return p + Agency.Utils.Constants.Global.Recurrent.COMMA + c;
							};
						});
				}
				return this.apiService.FetchOffers({
					page: (page + 0) - 1,
					limit: limit,
					tags: this.$scope.Tags,
					expirationDateAmount: this.$scope.expirationDateFiltersContainer.ExpirationDateAmount,
					expirationDateRange: { from: this.$scope.expirationDateFiltersContainer.from, to: this.$scope.expirationDateFiltersContainer.to },
					statusFilter: this.$scope.statusFilter,
					network: this.$scope.Search_Network,
					os: this.$scope.Search_OS,
					country: this.$scope.Search_Country,
					sort: sortString,
					ioc: this.$scope.Search_Country,
					id: this.$scope.Search_ID,
					approvalStatus: this.$scope.Search_RequestAccessStatus
				} as Agency.Services.FetchRequestArguments)
					.then((response?: ng.IRequestConfig) => {
						this.loaded = true;
						return {
							results: response.data.Offers,
							totalResultCount: response.data.RowCount
						};
					});
			}
			getCreativities = (src, data, id) => {
				this.$scope.CreativitiesLoad = true;
				this.$scope.ShowRetryCreativitiesLoad = false;
				let nid = data.filter((d, idx, src) => {
					return d.columnKey === Agency.Utils.FieldDefinitions.OfferDefinition.NETWORK_ID;
				});
				this.$scope.Creativities = this.$scope.Creativities || [];
				this.apiService.FetchOfferCreativities(nid[0].value, nid[0].rowId)
					.then((response) => {
						if ((response.data as any).Results && (response.data as any).Results.length > 0) {
							this.$scope.Creativities[id] = (response.data as any).Results;
							this.$scope.CreativitiesLoad = false;
						} else {
							this.$scope.Creativities[id] = (response.data as any).Results;
							this.$scope.CreativitiesLoad = false;
							this.$scope.ShowRetryCreativitiesLoad = true;
							this.$scope.CreativitiesErrorMessage = (response.data as any).ErrorMessage;
						}
						this.$scope.CreativitiesLoad = false;
						setTimeout(() => { this.$scope.$digest(); }, 100);
					});
			}
			requiredOfferAccess = (data: any[]): boolean => {
				let t = data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.TRACKING_URL;
				});
				let result = t[0].value && t[0].value !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				return !result;
			}
			requestedOfferAccessIcon = (data: any[], value?: string) => {
				if (data === null && value === null) {
					return;
				}
				const approvalStatus = value || data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS;
				})[0].value;
				let result = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				switch (approvalStatus) {
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_APPROVED:
						result = "check_circle";
						break;
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_PENDING:
						result = "warning";
						break;
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_REJECTED:
						result = "error";
						break;
				}
				return result;
			}
			requestedOfferAccessColor = (data: any[], value?: string) => {
				if (data === null && value === null) {
					return;
				}
				const approvalStatus = value || data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS;
				})[0].value;
				let result = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				switch (approvalStatus) {
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_APPROVED:
						result = this.GREEN;
						break;
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_PENDING:
						result = this.ORANGE;
						break;
					case Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_REJECTED:
						result = this.RED;
						break;
				}
				return result;
			}
			requestOfferAccessStatus = (data: any[], value?: string) => {
				if (data === null && value === null) {
					return;
				}
				let result = value || data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS;
				})[0].value;
				if (result && result !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) {
					result = "ok";
				}
			}
			showRequestOfferAccess = (data: any[], value?: string): boolean => {
				if (data === null && value === null) {
					return;
				}
				const approvalStatus = value || data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS;
				})[0].value;
				return approvalStatus !== Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_APPROVED
					&& approvalStatus !== Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_PENDING
					&& approvalStatus !== Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_REJECTED;
			}
			requestOfferAccess = (data: any[], source: any) => {
				const offerId = data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.OFFER_ID;
				})[0].value;
				const networkId = data.filter((item) => {
					return item[this.COLUMN_KEY] === Agency.Utils.FieldDefinitions.OfferDefinition.NETWORK_ID;
				})[0].value;
				this.apiService.RequestOfferAccess(networkId, offerId)
					.then((result) => {
					});
			}
			rowClassCallback = (row) => {
				return row.approval_status === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_REJECTED
					? "row-rejected"
					: row.approval_status === Agency.Utils.FieldDefinitions.OfferDefinition.APPROVAL_STATUS_PENDING
						? "row-pending"
						: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
			}
		}
	}
}
app.controller("offerTableController", Agency.Offers.offertableController);
