namespace Agency {
	"use strict";
	export class backFiltersController {
		static $inject = ["$scope", "$timeout", "$mdBottomSheet", "Amplify"];
		constructor(
			private $scope: Agency.Controllers.Scopes.IBackFiltersControllerScope,
			private $timeout,
			private $mdBottomSheet: ng.material.IBottomSheetService,
			private Amplify: Agency.Services.IAmplify) {
		}
		hide = () => {
			this.$mdBottomSheet.hide();
		};
		listItemClick = ($index) => {
			let index = $index;
			this.$mdBottomSheet.show({
				clickOutsideToClose: false,
				parent: angular.element(document.getElementById("content-filter")),
				disableBackdrop: true,
				disableParentScroll: false,
				templateUrl: Agency.Utils.Constants.Global.Recurrent.TEMPLATES_BASE_URL + "offers/partials/filters-datarange-list.html"
			});
		};
		showCalendarBottomSheet = () => {
			this.$mdBottomSheet.show({
				clickOutsideToClose: false,
				parent: angular.element(document.getElementById("content-filter")),
				disableBackdrop: true,
				disableParentScroll: false,
				templateUrl: Agency.Utils.Constants.Global.Recurrent.TEMPLATES_BASE_URL + "offers/partials/filters-datarange-calendar.html"
			});
		};
		onChangeExpiryDateFilter = (amount: number) => {
			this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_EXPIRY_DATE_SEARCH, [amount]);
		}
		onChangeCustomRangeExpiryDateFilter = () => {
			const value = (this.$scope.expirationDateLf as any);
			this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_EXPIRY_CUSTOMRANGE_DATE_SEARCH, [value]);
		}
		onChangeActiveFilter = (status: string) => {
			this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_EXPIRY_STATUS_SEARCH, [status]);
		} 
	}
}
app.controller("backFiltersController", Agency.backFiltersController);