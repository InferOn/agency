namespace Agency {
	"use strict";
	export class filtersController {
		static $inject = ["$scope", "$timeout", "$mdBottomSheet", "Amplify"];
		constructor(
			private $scope: Agency.Controllers.Scopes.IFiltersControllerScope,
			private $timeout,
			private $mdBottomSheet: ng.material.IBottomSheetService,
			private Amplify: Agency.Services.IAmplify) {
			Amplify.subscribe("closeFiltersControllerCancel", this, () => { this.showListBottomSheet(); });
			const amp = this.Amplify;
			this.$scope.$on("$destroy", function handler() {
				amp.unsubscribe("closeFiltersControllerCancel", () => { });
			});
		}
		showListBottomSheet = () => {
			this.$mdBottomSheet.show({
				controller: closeFiltersController,
				controllerAs: "lfCtrl",
				clickOutsideToClose: false,
				parent: angular.element(document.getElementById("content-filter")),
				disableBackdrop: true,
				disableParentScroll: false,
				templateUrl: Agency.Utils.Constants.Global.Recurrent.TEMPLATES_BASE_URL + "offers/partials/filters-list.html",
			});
		};
	}
}
app.controller("filtersController", Agency.filtersController);
