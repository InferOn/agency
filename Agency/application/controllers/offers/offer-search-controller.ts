﻿namespace Agency {
	"use strict";
	
	export namespace SearchPanels {
		export namespace Offers {
			export class searchPanelController {
				OSS = Agency.Utils.Constants.Global.Enums.OSs;
				OfferRequestStatuses = Agency.Utils.Constants.Global.Enums.OfferRequestStatuses;
				Title: string = "Type and press enter";
				Tags: string[] = [];
				preferredNetwork: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredCountry: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredOS: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredRequestStatus: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredID: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				static $inject = ["$scope", "$timeout", "$q", "Amplify", "apiService", "ISO3166"];
				constructor(private $scope: Agency.Controllers.Scopes.ISearchOffersControllerScope,
					private $timeout: angular.ITimeoutService,
					private $q: angular.IQService,
					private Amplify: Agency.Services.IAmplify,
					private apiService: Agency.Services.ApiService,
					private ISO3166: any
				) {
					this.$scope.OSS = this.OSS;
					this.$scope.Countries = ISO3166.codeToCountry;
					this.$scope.OfferRequestStatuses = this.OfferRequestStatuses;


					this.apiService.FetchNetworks()
						.then((response?: ng.IRequestConfig) => {
							this.$scope.Networks = response.data;
						});
				}
				onAdd = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this.Tags);
				}
				onRemove = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this.Tags);
				}
				onNetworkChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_NETWORK_CHANGE_SEARCH, [this.preferredNetwork]);
				}
				onCountryChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, [this.preferredCountry]);
				}
				onIDChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, [this.preferredID]);
				}
				onOSChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, [this.preferredOS]);
				}
				onRequestStatusChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_REQUEST_STATUS_CHANGE_SEARCH, [this.preferredRequestStatus]);
				}
			}
		}
	}
}

app.controller("searchPanelOffersController", Agency.SearchPanels.Offers.searchPanelController);

