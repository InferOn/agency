namespace Agency {
	"use strict";
	export class closeFiltersController {
		static $inject = ["$scope", "$timeout", "$mdBottomSheet", "Amplify"];
		items: any[] = [{ name: "Status" }, { name: "Date range"}];
		constructor(
			private $scope: Agency.Controllers.Scopes.ICloseFiltersControllerScope,
			private $timeout: ng.ITimeoutService,
			private $mdBottomSheet: ng.material.IBottomSheetService,
			private Amplify: Agency.Services.IAmplify
		) { }
		closeListBottomSheet = () => {
			this.$mdBottomSheet.hide();
		};
		listItemClick = ($index) => {
			let index = $index;
			this.$mdBottomSheet.show({
				clickOutsideToClose: false,
				parent: angular.element(document.getElementById("content-filter")),
				disableBackdrop: true,
				disableParentScroll: false,
				templateUrl: this.resolveTemplateURL(index) 
			}).then(() => {
				this.Amplify.publish("closeFiltersControllerCancel", []);
			}, () => {});
		};
		private resolveTemplateURL= (index) => {
			return index === 1 ? Agency.Utils.Constants.Global.Recurrent.TEMPLATES_BASE_URL + "offers/partials/filters-datarange-list.html"
				: Agency.Utils.Constants.Global.Recurrent.TEMPLATES_BASE_URL + "offers/partials/filters-status-list.html";
		}
	}
}
app.controller("closeFiltersController", Agency.closeFiltersController);
