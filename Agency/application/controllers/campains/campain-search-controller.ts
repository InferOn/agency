﻿namespace Agency {
	"use strict";
	export namespace SearchPanels {
		export namespace Campains {
			export class searchPanelController {
				Platforms = Agency.Utils.Constants.Global.Enums.OSs;
				Tags: string[] = [];
				preferredID: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredBid: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredDailyBudget: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredPricingModel: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredCategory: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredPlatform: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredCountries: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredCreatedDate: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredStartedDate: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredUpdatedDate: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				preferredStatuses: any = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				PreferredStartedDateFrom: any;
				PreferredStartedDateTo: any;

				static $inject = ["$scope", "$timeout", "$q", "Amplify", "apiService", "ISO3166"];
				constructor(private $scope: Agency.Controllers.Scopes.ISearchCampainControllerScope,
					private $timeout: angular.ITimeoutService,
					private $q: angular.IQService,
					private Amplify: Agency.Services.IAmplify,
					private apiService: Agency.Services.ApiService,
					private ISO3166: any
				) {
					this.$scope.PricingModels = Agency.Utils.Constants.Global.Enums.PricingModels;
					this.$scope.Categories = Agency.Utils.Constants.Global.Enums.Categories;
					this.$scope.Platforms = this.Platforms;
					this.$scope.Countries = ISO3166.codeToCountry;
					this.$scope.Statuses = ["active", "paused", "stopped", "denied", "pendingTracker", "draft", "archived"];
				}
				onAdd = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this.Tags);
				}
				onRemove = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this.Tags);
				}
				onIDChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, [this.preferredID]);
				}
				onBidChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_BID_CHANGE_SEARCH, [this.preferredBid]);
				}
				onDailyBudgetChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_DAILY_BUDGET_CHANGE_SEARCH, [this.preferredDailyBudget]);
				}
				onPricingModelChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_PRICING_MODEL_CHANGE_SEARCH, [this.preferredPricingModel]);
				}
				onCategoryChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_CATEGORY_CHANGE_SEARCH, [this.preferredCategory]);
				}
				onPlatformChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, [this.preferredPlatform]);
				}
				onCountryChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, [this.preferredCountries]);
				}
				onCreatedDateChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_CREATED_DATE_CHANGE_SEARCH, [this.preferredCreatedDate]);
				}
				onStartedDateChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_CHANGE_SEARCH, [this.preferredStartedDate]);
				}

				onStartedDateFromChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_FROM_CHANGE_SEARCH, [this.PreferredStartedDateFrom]);
				}
				onStartedDateToChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_TO_CHANGE_SEARCH, [this.PreferredStartedDateTo]);
				}

				onUpdatedDateChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_UPDATED_DATE_CHANGE_SEARCH, [this.preferredUpdatedDate]);
				}

				onStatusChange = () => {
					this.Amplify.publish(Agency.Utils.Constants.Global.Events.ON_STATUS_CHANGE_SEARCH, [this.preferredStatuses]);
				}
			}
		}
	}
}
app.controller("searchPanelCampainsController", Agency.SearchPanels.Campains.searchPanelController);

