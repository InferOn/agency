﻿namespace Agency {
	"use strict";
	export namespace Campains {
		export class TableController extends Agency.baseTableController {
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166"];
			constructor(
				$scope: Agency.Controllers.Scopes.ICampainTableControllerScope,
				$mdToast: any,
				apiService: Agency.Services.ApiService,
				Amplify: Services.IAmplify,
				ISO3166: any
			) {
				super($scope, $mdToast, apiService, Amplify, ISO3166);
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, this,
					(args: any[]) => { this.OnKeywordSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnIdChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_BID_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnBidChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_DAILY_BUDGET_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnDailyBudgetChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_PRICING_MODEL_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnPricingModelChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_CATEGORY_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnCategoryChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnOsChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnCountryChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_CREATED_DATE_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnCreatedDateChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnStartedDateChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_UPDATED_DATE_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnUpdatedDateChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_FROM_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnStartedDateFromChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_TO_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnStartedDateToChangeSearch(args); });
				this.Amplify.subscribe(Agency.Utils.Constants.Global.Events.ON_STATUS_CHANGE_SEARCH, this,
					(args: any[]) => { this.OnStatusesChangeSearch(args); });
				this.loaded = false;
				const amp = this.Amplify;
				this.$scope.$on("$destroy", function handler() {
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_KEYWORD_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_ID_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_BID_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_DAILY_BUDGET_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_PRICING_MODEL_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_CATEGORY_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_OS_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_COUNTRY_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_CREATED_DATE_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_UPDATED_DATE_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_FROM_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_STARTED_DATE_TO_CHANGE_SEARCH, () => { });
					amp.unsubscribe(Agency.Utils.Constants.Global.Events.ON_STATUS_CHANGE_SEARCH, () => { });
				});
			}
			paginatorCallback(page: number, limit: number, options: any) {
				const fields = [
					Agency.Utils.FieldDefinitions.CampainDefinition.TRAFFICSOURCE_ID,
					Agency.Utils.FieldDefinitions.CampainDefinition.CAMPAIN_ID,
					Agency.Utils.FieldDefinitions.CampainDefinition.NAME,
					Agency.Utils.FieldDefinitions.CampainDefinition.STATUS,
					Agency.Utils.FieldDefinitions.CampainDefinition.BID_PRICE,
					Agency.Utils.FieldDefinitions.CampainDefinition.DAILY_BUDGET,
					Agency.Utils.FieldDefinitions.CampainDefinition.PRICING_MODEL,
					Agency.Utils.FieldDefinitions.CampainDefinition.TOTAL_BUDGET,
					Agency.Utils.FieldDefinitions.CampainDefinition.CREATED_DATE,
					Agency.Utils.FieldDefinitions.CampainDefinition.START_DATE,
					Agency.Utils.FieldDefinitions.CampainDefinition.UPDATE_DATE,
					Agency.Utils.FieldDefinitions.CampainDefinition.APP_NAME,
					Agency.Utils.FieldDefinitions.CampainDefinition.CATEGORY,
					Agency.Utils.FieldDefinitions.CampainDefinition.DESTINATION_URL,
					Agency.Utils.FieldDefinitions.CampainDefinition.PLATFORM,
					Agency.Utils.FieldDefinitions.CampainDefinition.COUNTRIES,
				];
				let sortString = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (options && options.columnSort) {
					const sortmap = options.columnSort
						.map((mi: any, idx: any) => {
							if (mi.sort) {
								return {
									text: fields[idx] + Agency.Utils.Constants.Global.Recurrent.BLANK + mi.sort
								};
							} else {
								return {
									text: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY
								};
							}
						})
						.filter((fi: any) => { return fi.text !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY; });
					sortString = (sortmap.length > 0 ? sortmap : [Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY])
						.reduce((p: any, c: any) => {
							if (p !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) {
								return p + Agency.Utils.Constants.Global.Recurrent.COMMA + c;
							};
						});
				}
				return this.apiService.FetchCampains({
					page: (page + 0) - 1,
					limit: limit,
					tags: this.$scope.Tags,
					os: this.$scope.Search_OS,
					id: this.$scope.Search_ID,
					ioc: this.$scope.Search_Country, // ???
					sort: sortString,
					bid: this.$scope.Search_Bid,
					dailyBudget: this.$scope.Search_Daily_budget,
					pricingModel: this.$scope.Search_Pricing_model,
					platform: this.$scope.Search_OS,
					countries: this.$scope.Search_Country, // ???
					startedDateFrom: this.$scope.Search_StartedDateFrom,
					startedDateTo: this.$scope.Search_StartedDateTo,
					statuses: this.$scope.Search_Statuses,
				} as Agency.Services.CampainFetchRequestArguments)
					.then((response?: ng.IRequestConfig) => {
						this.loaded = true;
						return {
							results: response.data.Campains,
							totalResultCount: response.data.RowCount
						};
					});
			};
			private OnKeywordSearch = (args: any) => {
				this.$scope.Tags = args;
				this.loadPage();
			};
			private OnIdChangeSearch = (args: any) => {
				this.$scope.Search_ID = args[0];
				this.loadPage();
			};
			private OnBidChangeSearch = (args: any) => {
				this.$scope.Search_Bid = args[0];
				this.loadPage();
			};
			private OnDailyBudgetChangeSearch = (args: any) => {
				this.$scope.Search_Daily_budget = args[0];
				this.loadPage();
			};
			private OnPricingModelChangeSearch = (args: any) => {
				this.$scope.Search_Pricing_model = args[0];
				this.loadPage();
			};
			private OnCategoryChangeSearch = (args: any) => {
				this.loadPage();
			};
			private OnOsChangeSearch = (args: any) => {
				this.$scope.Search_OS = args[0];
				this.loadPage();
			};
			private OnCountryChangeSearch = (args: any) => {
				let results = Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
				if (args[0] && (args[0] as any[]).length > 0) {
					results = args[0].map((item: any) => {
						return this.ISO3166.countryToCode[item];
					})
						.reduce((p: any, c: any) => {
							return p + "|" + c;
						});
				}
				this.$scope.Search_Country = results;
				this.loadPage();
			};
			private OnStartedDateFromChangeSearch = (args: any) => {
				if (args && args[0]) {
					this.$scope.Search_StartedDateFrom = args[0];
				}
				this.loadPage();
			};
			private OnStartedDateToChangeSearch = (args: any) => {
				if (args && args[0]) {
					this.$scope.Search_StartedDateTo = args[0];
				}
				this.loadPage();
			};
			private OnStatusesChangeSearch = (args: any) => {
				if (args && args[0]) {
					this.$scope.Search_Statuses = args[0];
				}
				this.loadPage();
			};
			private OnStartedDateChangeSearch = (args: any) => {
				this.loadPage();
			};
			private OnCreatedDateChangeSearch = (args: any) => {
				this.loadPage();
			};
			private OnUpdatedDateChangeSearch = (args: any) => {
				this.loadPage();
			};
		}
	}
}
app.controller("campainsTableController", Agency.Campains.TableController);
