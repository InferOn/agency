﻿namespace Agency {
	"use strict";
	export class mainController {
		static $inject = ["$scope", "apiService", "$location", "authService", "Amplify"];
		constructor(private $scope: Agency.Controllers.Scopes.IMainControllerScope,
			private apiService: Agency.Services.ApiService,
			private $location: ng.ILocationService,
			private authService: Agency.Services.AuthServiceFactory,
			private Amplify: Agency.Services.IAmplify
		) {
			this.$location.path("/");
		}
	}

	export class loginController {
		static $inject = ["$scope", "apiService", "$location", "authService"];
		constructor(private $scope: Agency.Controllers.Scopes.ILoginControllerScope,
			private apiService: Agency.Services.ApiService,
			private $location: ng.ILocationService,
			private authService: Agency.Services.AuthServiceFactory
		) { 
			this.$scope.authentication = this.authService.authentication;
		}

		login = () => {
			this.authService.login(this.$scope.loginInfo)
				.then((response) => {
					this.$location.path("/offers/");
				});
		}
	}
}
app.controller("loginController", Agency.loginController);
app.controller("mainController", Agency.mainController);
