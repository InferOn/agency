﻿namespace Agency {
	"use strict";
		export abstract class baseTableController {
			protected selectedItems: number[];
			protected loaded: boolean = false;
			protected gridOptions: any;
			protected COLUMN_KEY: string = "columnKey";
			protected GREEN: string = "green";
			protected ORANGE: string = "orange";
			protected RED: string = "red";
			protected loadPageCallbackDeferred: Function = null;
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166"];
			constructor(
				protected $scope: Agency.Controllers.Scopes.IBaseTableControllerScope,
				protected $mdToast: any,
				protected apiService: Agency.Services.ApiService,
				protected Amplify: Services.IAmplify,
				protected ISO3166: any
			) {
				this.$scope.onCopySuccess = this.onCopySuccess;
				this.loaded = false;
			}
			abstract paginatorCallback(page: number, limit: number, options: any);
			protected initGrid(columnDefinitions: Array<IColumnDefinition>): void {
				this.gridOptions = {
					columnDefs: columnDefinitions,
					rowData: [],
					rowSelection: "multiple",
					enableColResize: true,
					enableSorting: true,
					enableFilter: true,
					rowHeight: 50,
					suppressRowClickSelection: true,
					onModelUpdated: () => {
						const model = this.gridOptions.api.getModel();
						const totalRows = this.gridOptions.rowData.length;
						const processedRows = model.getRowCount();
						this.$scope.rowCount = processedRows.toLocaleString() + " / " + totalRows.toLocaleString();
					}
				};
			}
			protected rowClassCallback = (row) => {
				return Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY;
			}
			protected loadPage = () => {
				if (this.loadPageCallbackDeferred) {
					this.loaded = false;
					this.loadPageCallbackDeferred();
				}
			}
			onCopySuccess = (e) => {
				$(e.trigger).stop().css({ "background-color": "#01579b", color: "#fff" })
					.animate({ backgroundColor: "transparent", color: "#000" }, 500, "linear");
				this.$mdToast.show({
					hideDelay: 1000,
					position: "top right",
					templateUrl: "toast-template.html"
				});
			}
			onCopyError = (e) => { }
			getDetailValue = (name: string, src: any[]) => {
				let t = src.filter((item) => {
					return item[this.COLUMN_KEY] === name;
				});
				return t[0].value;
			}
			getLoadResultsCallback(loadPageCallback) {
				this.loadPageCallbackDeferred = loadPageCallback;
			}
		}
}
