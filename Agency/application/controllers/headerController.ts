﻿namespace Agency {
	"use strict";
	export class headerController {
		static $inject = ["$scope"];
		Title: string = "Campaign manager";
		constructor(private $scope: Agency.Controllers.Scopes.IHeaderControllerScope) {
		}
	}
}
app.controller("headerController", Agency.headerController);
