﻿namespace Agency {
	"use strict";
	export namespace TrafficSources {
		export class tableController extends Agency.baseTableController {
			static $inject = ["$scope", "$mdToast", "apiService", "Amplify", "ISO3166"];
			constructor(
				$scope: Agency.Controllers.Scopes.IBaseTableControllerScope,
				$mdToast: any,
				apiService: Agency.Services.ApiService,
				Amplify: Services.IAmplify,
				ISO3166: any
			) {
				super($scope, $mdToast, apiService, Amplify, ISO3166);
			}
			paginatorCallback(page: number, limit: number, options: any) {
				const fields = [
					"Name",
				];
				let sortString = "";
				if (options && options.columnSort) {
					const sortmap = options.columnSort
						.map((mi, idx) => {
							if (mi.sort) {
								return {
									text: fields[idx] + " " + mi.sort
								};
							} else {
								return {
									text: Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY
								};
							}
						})
						.filter((fi) => { return fi.text !== ""; });
					sortString = (sortmap.length > 0 ? sortmap : [""]).reduce((p, c) => { if (p !== Agency.Utils.Constants.Global.Recurrent.STRING_EMPTY) { return p + "," + c; }; });
				}
				return this.apiService.FetchTrafficSources({
					page: (page + 0) - 1,
					limit: limit,
					statusFilter: this.$scope.statusFilter,
					sort: sortString,
					id: this.$scope.Search_ID
				} as Agency.Services.FetchRequestArguments)
					.then((response?: ng.IRequestConfig) => {
						this.loaded = true;
						return {
							results: response.data.TrafficSources,
							totalResultCount: response.data.RowCount
						};
					});
			}
			selectedRowCallback = (rows: number[]) => {
				this.selectedItems = rows;
			}
		}
	}
}
app.controller("trafficsourceTableController", Agency.TrafficSources.tableController);
