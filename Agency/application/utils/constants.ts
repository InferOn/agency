﻿namespace Agency {
	"use strict";
	export namespace Utils {
		"use strict";
		export namespace Constants {
			export namespace Global {
				export class Enums {
					public static OSs: any[] = [{ "Name": "iOS", "Value": "iOS" }, { "Name": "Android", "Value": "Android" }];
					public static PricingModels: any[] = [{ "Name": "CPI", "Value": "CPI" }, { "Name": "CPC", "Value": "CPC" }, { "Name": "CPM", "Value": "CPM" }];
					public static Categories: any[] = [];
					public static CampainStatuses: any[] = ["active", "paused", "stopped", "denied", "pendingTracker", "draft", "archived"];
					public static OfferRequestStatuses: any[] = [{ "Name": "Approved", "Value": "Approved" }, { "Name": "Pending", "Value": "Pending" }, { "Name": "Reject", "Value": "Reject" }];
				}
				export class Recurrent {
					public static STRING_EMPTY: string = "";
					public static COMMA: string = ",";
					public static BLANK: string = " ";

					public static TEMPLATES_BASE_URL: string = "./app/templates/";
				}
				export class Events {
					public static ON_KEYWORD_SEARCH: string = "ON_KEYWORD_SEARCH";
					public static ON_EXPIRY_DATE_SEARCH: string = "ON_EXPIRY_DATE_SEARCH";
					public static ON_EXPIRY_CUSTOMRANGE_DATE_SEARCH: string = "ON_EXPIRY_CUSTOMRANGE_DATE_SEARCH";

					public static ON_NETWORK_CHANGE_SEARCH: string = "ON_NETWORK_CHANGE_SEARCH";
					public static ON_COUNTRY_CHANGE_SEARCH: string = "ON_COUNTRY_CHANGE_SEARCH";
					public static ON_OS_CHANGE_SEARCH: string = "ON_OS_CHANGE_SEARCH";
					public static ON_ID_CHANGE_SEARCH: string = "ON_ID_CHANGE_SEARCH";
					public static ON_BID_CHANGE_SEARCH: string = "ON_BID_CHANGE_SEARCH";
					public static ON_DAILY_BUDGET_CHANGE_SEARCH: string = "ON_DAILY_BUDGET_CHANGE_SEARCH";
					public static ON_CATEGORY_CHANGE_SEARCH: string = "ON_CATEGORY_CHANGE_SEARCH";
					public static ON_PRICING_MODEL_CHANGE_SEARCH: string = "ON_PRICING_MODEL_CHANGE_SEARCH";
					public static ON_REQUEST_STATUS_CHANGE_SEARCH: string = "ON_REQUEST_STATUS_CHANGE_SEARCH";
					public static ON_CREATED_DATE_CHANGE_SEARCH: string = "ON_CREATED_DATE_CHANGE_SEARCH";
					public static ON_STARTED_DATE_CHANGE_SEARCH: string = "ON_STARTED_DATE_CHANGE_SEARCH";

					public static ON_STATUS_CHANGE_SEARCH: string = "ON_STATUS_CHANGE_SEARCH";
					
					public static ON_UPDATED_DATE_CHANGE_SEARCH: string = "ON_UPDATED_DATE_CHANGE_SEARCH";
					public static ON_STARTED_DATE_FROM_CHANGE_SEARCH: string = "ON_STARTED_DATE_FROM_CHANGE_SEARCH";
					public static ON_STARTED_DATE_TO_CHANGE_SEARCH: string = "ON_STARTED_DATE_TO_CHANGE_SEARCH";
					public static ON_EXPIRY_STATUS_SEARCH: string = "ON_EXPIRY_STATUS_SEARCH";
					public static ON_SUCCESSFULLY_LOGGED_IN: string = "ON_SUCCESSFULLY_LOGGED_IN";
					public static ON_SUCCESSFULLY_LOGGED_OUT: string = "ON_SUCCESSFULLY_LOGGED_OUT";


					
					public static ON_HISTORY_CAMPAIN_ROW_ADDCLICK: string = "ON_HISTORY_CAMPAIN_ROW_ADDCLICK";
					public static ON_NETWORK_ROW_ADDCLICK: string = "ON_NETWORK_ROW_ADDCLICK";
					
					public static REFRESH_GRID: string = "REFRESH_GRID";
					public static ON_HISTORY_CAMPAIN_ROW_CLICK: string = "ON_HISTORY_CAMPAIN_ROW_CLICK";
					public static ON_NETWORK_ROW_CLICK: string = "ON_NETWORK_ROW_CLICK";
				}
			}
		}
		export namespace FieldDefinitions {
			export class CampainDefinition {
				public static TRAFFICSOURCE_ID: string = "trafficSourceId";
				public static CAMPAIN_ID: string = "campaignId";
				public static NAME: string = "name";
				public static STATUS: string = "status";
				public static BID_PRICE: string = "bidPrice";
				public static DAILY_BUDGET: string = "dailyBudget";
				public static PRICING_MODEL: string = "pricingModel";
				public static TOTAL_BUDGET: string = "totalBudget";
				public static CREATED_DATE: string = "createdDate";
				public static START_DATE: string = "startDate";
				public static UPDATE_DATE: string = "updateDate";
				public static APP_NAME: string = "appName";
				public static CATEGORY: string = "category";
				public static DESTINATION_URL: string = "destinationUrl";
				public static PLATFORM: string = "platform";
				public static COUNTRIES: string = "countries";
			}
			export class CampainHistoryDefinition {
				public static ID: string = "Id";
				public static STATUS: string = "Status";
				public static NETWORK_NAME: string = "NetworkName";
				public static NAME: string = "Name";
				public static OS: string = "Os";
				public static GEO: string = "Geo";
				public static PAYOUT: string = "Payout";
				public static DAILY_CAP: string = "DailyCap";
				public static TOTAL_CAP: string = "TotalCap";
				public static START_DATE: string = "StartDate";
				public static DELIVERED_ON: string = "DeliveredOn";
				public static MANAGED_BY: string = "ManagedBy";
				public static KPI: string = "Kpi";
				public static NOTES: string = "Notes";
			}
			export class OfferDefinition {
				public static APPROVAL_STATUS: string = "approval_status";
				public static TRACKING_URL: string = "TrackingURL";
				public static TRACKING_ID: string = "TrackingId";
				public static PAYOUTS: string = "Payouts";
				public static APP_CATEGORIES: string = "AppCategories";
				public static APP_DESCRIPTION: string = "AppDescription";
				public static APP_ID: string = "AppID";
				public static APP_PRICE: string = "AppPrice";
				public static APP_OS: string = "AppOS";
				public static APP_RATING: string = "AppRating";
				public static CONVERSION_CAP: string = "ConversionCap";
				public static PAYOUT_CAP: string = "PayoutCap";
				public static INITIAL_CONVERSION_CAP: string = "InitialConversionCap";
				public static INITIAL_PAYOUT_CAP: string = "InitialPayoutCap";
				public static STATUS: string = "Status";
				public static CONVERSION_TYPE: string = "ConversionType";
				public static CURRENCY: string = "Currency";
				public static DATE_EXPIRATION: string = "DateExpiration";
				public static COUNTRIES: string = "Countries";
				public static DESCRIPTION: string = "Description";
				public static NAME: string = "Name";
				public static NETWORK_NAME: string = "NetworkName";
				public static OFFER_ID: string = "OfferId";
				public static NETWORK_ID: string = "NetworkId";

				public static APPROVAL_STATUS_APPROVED: string = "approved";
				public static APPROVAL_STATUS_PENDING: string = "pending";
				public static APPROVAL_STATUS_REJECTED: string = "rejected";
			}
		}
	}
}