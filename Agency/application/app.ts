﻿///<reference path="../Scripts/typings/all.d.ts" />
"use strict";
declare var moment: any;
let app = angular.module("app", [
	"ngMaterial",
	"ngMessages",
	"mdDataTable",
	"appConfig",
	"ui.router",
	"LocalStorageModule",
	"iso-3166-country-codes",
	"ngclipboard",
	"ui.grid",
	"fixed.table.header",
	"angular-momentjs"
])
	.factory("Amplify", () => {
		return {
			publish: (topic: string, args: any[]): boolean => {
				return amplify.publish(topic, args);
			},
			subscribe: (topic: string, context: any, callback: Function, priority?: number): void => {
				return amplify.subscribe(topic, context, callback, priority);
			},
			unsubscribe: (topic: string, callback: Function): void => {
				return amplify.unsubscribe(topic, callback);
			}
		} as Agency.Services.IAmplify;
	})
	.config(["$mdThemingProvider", "$mdIconProvider", "$stateProvider", "$urlRouterProvider", "$httpProvider", "$momentProvider", "$mdDateLocaleProvider",
		($mdThemingProvider: angular.material.IThemingProvider,
			$mdIconProvider: angular.material.IIconProvider,
			$stateProvider: angular.ui.IStateProvider,
			$urlRouterProvider: angular.ui.IUrlRouterProvider,
			$httpProvider: angular.IHttpProvider,
			$momentProvider: any,
			$mdDateLocaleProvider: angular.material.IDateLocaleProvider) => {
			$mdDateLocaleProvider.formatDate = function (date) {
				return moment(date).format("DD/MM/YYYY");
			};
			const pls = ["agencyBlue"];
			$mdThemingProvider.alwaysWatchTheme(true);
			let rndPlt = Math.floor(Math.random() * (pls.length - 0)) + 0;
			$mdThemingProvider.definePalette("agencyBlue",
				$mdThemingProvider.extendPalette("blue", {
					"800": "#01579b",
					"500": "#01579b",
					"contrastDefaultColor": "dark",
					"contrastDarkColors": ["50", "100", "200", "300", "400", "A100"],
				}));
			$mdThemingProvider.theme("default")
				.primaryPalette(pls[rndPlt])
				.accentPalette(pls[rndPlt])
				.warnPalette(pls[rndPlt]);

			//TODO: why intercept does not work as explicitly set the request header in apiservice?
			//TODO: make it works
			//$httpProvider.interceptors.push("authInterceptorService");
			$stateProvider.state({
				name: "login",
				url: "/login/",
				templateUrl: "./app/templates/login.html",
			});

			$stateProvider.state("offers", {
				name: "offers",
				url: "/offers/",
				views: {
					"": {
						templateUrl: "./app/templates/offers/offers.html",
					},
					"searchPanel@offers": {
						templateUrl: "./app/templates/offers/search.html",
					},
				},
			});
			$stateProvider.state("networks", {
				name: "networks",
				url: "/networks/",
				views: {
					"": {
						templateUrl: "./app/templates/networks/networks.html",
					},
					"searchPanel@networks": {
						templateUrl: "./app/templates/networks/search.html",
					},
				},
			});
			$stateProvider.state("trafficsources", {
				name: "trafficsources",
				url: "/trafficsources/",
				views: {
					"": {
						templateUrl: "./app/templates/trafficsources/trafficsources.html",
					},
					"searchPanel@trafficsources": {
						templateUrl: "./app/templates/trafficsources/search.html",
					},
				},
			});
			$stateProvider.state("campaigns", {
				name: "campaigns",
				url: "/campaigns/",
				views: {
					"": {
						templateUrl: "./app/templates/campaigns/campaigns.html",
					},
					"searchPanel@campaigns": {
						templateUrl: "./app/templates/campaigns/search.html",
					},
				},
			});
			$stateProvider.state("historycampaigns", {
				name: "historycampaigns",
				url: "/historycampaigns/",
				views: {
					"": {
						templateUrl: "./app/templates/historycampaigns/campaigns.html",
					},
					"searchPanel@historycampaigns": {
						templateUrl: "./app/templates/historycampaigns/search.html",
					},
				},
			});

			$urlRouterProvider.when("/", "/historycampaigns/");
			$urlRouterProvider.when("/trafficsources", "/trafficsources/");
			$urlRouterProvider.when("/networks", "/networks/");
			$urlRouterProvider.when("/offers", "/offers/");
			$urlRouterProvider.when("/campaigns", "/campaigns/");
			$urlRouterProvider.when("/historycampaigns", "/historycampaigns/");
			$urlRouterProvider.otherwise("/login"); // temp ==> dashboard
		}])
	.run(["authService", (authService) => {
		authService.fillAuthData();
	}]);



