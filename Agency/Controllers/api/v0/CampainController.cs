﻿using AgencyLib;
using Data = AgencyLib.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;
using System;

namespace Agency.Controllers.api {
	/// <summary>
	/// Campain CRUD API Controller 
	/// </summary>
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
	public partial class CampainController : ApiController {

		/// <summary>
		/// GET every Campain instance
		/// </summary>
		/// <example>api/Campain/</example>
		/// <returns><see cref="IEnumerable{ViewModels.Campain}"/></returns>
		[Route("api/v0/Campains/")]
		public IEnumerable<ViewModels.Campain> GetV0() {
			return new List<ViewModels.Campain>() {
				//new ViewModels.Campain() {
				//	Id = 49,
				//	TrackingId = "123",
				//	Name = "campain for offer test 4",
				//	Payout = new decimal(0.16),
				//	Status = 1,
				//},
				//new ViewModels.Campain() {
				//	Id = 51,
				//	TrackingId = "test trk",
				//	Name = "campain for offer test 3",
				//	Payout = new decimal(0.6),
				//	Status = 1,
				//},
			};
		}
	}
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
}