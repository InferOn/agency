﻿using AgencyLib;
using Data = AgencyLib.Data;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;

namespace Agency.Controllers.api {
	/// <summary>
	/// Offers CRUD API Controller 
	/// </summary>
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning disable CS1658 // Warning is overriding an error
	public partial class OffersController : ApiController {

		/// <summary>
		/// GET every Offer instance
		/// </summary>
		/// <example>api/Offer/</example>
		/// <returns><see cref="IEnumerable{ViewModels.Offer}"/></returns>
		[Route("api/v0/offers/")]
		public IEnumerable<ViewModels.Offer> GetV0() {
			return new List<ViewModels.Offer>() {
				new ViewModels.Offer() {
					NetworkId= 2,
					OfferId= 2,
					Name= "test offer 1",
					Description= "test offer descr 1",
					Countries= "IT",
					DateExpiration= new System.DateTime(2017,03,02),
					Currency= "USD",
					ConversionType= "1",
					Status= "active",
					InitialPayoutCap= 500,
					InitialConversionCap= 100,
					PayoutCap= 0,
					ConversionCap= 500,
					AppRating= 3,
					AppOS= "IOS",
					AppPrice= "1.50",
					AppID= 123,
					AppDescription= "app description",
					AppCategories= "game",
					Network = new ViewModels.AffiliateNetwork() {
						Id= 2,
						Name= "Affiliate Source A",
						AccountSettings= "{key:\"networkA_key\"}",
						TrackingId= "trk_id_1"
					},
					Payouts= new List<ViewModels.OfferPayout>(),
					TrackingId= "123wer1"
				},
				new ViewModels.Offer() {
					NetworkId= 4,
					OfferId= 3,
					Name= "offer test 2",
					Description= "offer description test",
					Countries= "FR",
					DateExpiration= new System.DateTime(2017,03,02),
					Currency= "USD",
					ConversionType= "1",
					Status= "active",
					InitialPayoutCap= 500,
					InitialConversionCap= 100,
					PayoutCap= 50,
					ConversionCap= 10,
					AppRating= 1,
					AppOS= "IOS",
					AppPrice= "1",
					AppID= 12345,
					AppDescription= "app description test",
					AppCategories= "game",
					Network = new ViewModels.AffiliateNetwork() {
						Id= 4,
						Name= "Affiliate Source B",
						AccountSettings= "{key:\"networkB_key\"}",
						TrackingId= "trk_id_2"
					},
					Payouts= new List<ViewModels.OfferPayout>(),
					TrackingId= "trk_test2"
				},
				new ViewModels.Offer() {
				NetworkId= 5,
				OfferId= 1,
				Name= "offer test 4",
				Description= "offer description test 4",
				Countries= "FR",
				DateExpiration= new System.DateTime(2017,03,02),
				Currency= "EUR",
				ConversionType= "1",
				Status= "active",
				InitialPayoutCap= 100,
				InitialConversionCap= 10,
				PayoutCap= 10,
				ConversionCap= 1,
				AppRating= new decimal(0.5),
				AppOS= "Android",
				AppPrice= "0.50",
				AppID= 1234,
				AppDescription= "app description test 5",
				AppCategories= "business",
				Network = new ViewModels.AffiliateNetwork() {
						Id= 5,
						Name= "Affiliate Source C updated",
						AccountSettings= "{key:\"networkC_key\"}",
						TrackingId= "trk_id_3"
					},
					Payouts= new List<ViewModels.OfferPayout>(),
					TrackingId= "trk_test5"
				},
			};
		}
	}
}