﻿using AgencyLib;
using AgencyLib.Data;
using AutoMapper;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using log4net;
using System;

namespace Agency.Controllers.api {
#pragma warning disable CS1658 // Warning is overriding an error
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
	/// <summary>
	/// AffiliateNetwork CRUD API Controller 
	/// </summary>
	//public partial class AffiliateNetworkController : ApiController {
	//	/// <summary>
	//	/// GET every Affiliate Network instance
	//	/// </summary>
	//	/// <example>api/AffiliateNetwork/</example>
	//	/// <returns><see cref="IEnumerable{ViewModels.AffiliateNetwork}"/></returns>
	//	[Route("api/v0/AffiliateNetworks/")]
	//	public IEnumerable<ViewModels.AffiliateNetwork> Getv0() {
	//		return new List<ViewModels.AffiliateNetwork>() {
	//			new ViewModels.AffiliateNetwork() {
	//				Id= 2,
	//				Name="Affiliate Source A",
	//				AccountSettings= "{key:\"networkA_key\"}",
	//				TrackingId="trk_id_1"
	//			},
	//			new ViewModels.AffiliateNetwork() {
	//				Id= 4,
	//				Name="Affiliate Source B",
	//				AccountSettings= "{key:\"keynetworkB_key\"}",
	//				TrackingId="trk_id_2"
	//			},
	//			new ViewModels.AffiliateNetwork() {
	//				Id= 5,
	//				Name="Affiliate Source C",
	//				AccountSettings= "{key:\"networkC_key\"}",
	//				TrackingId="trk_id_2"
	//			},
	//			new ViewModels.AffiliateNetwork() {
	//				Id= 6,
	//				Name="Affiliate Source D",
	//				AccountSettings= "{key:\"networkD_key\"}",
	//				TrackingId="trk_a"
	//			},
	//		};
	//	}
	//}
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning restore CS1658 // Warning is overriding an error
}