﻿using AgencyLib;
using AgencyLib.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;

namespace Agency.Controllers.api {
	/// <summary>
	/// TrafficSource CRUD API Controller 
	/// </summary>
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning disable CS1658 // Warning is overriding an error
	public partial class TrafficSourceController : ApiController {
	
		/// <summary>
		/// GET every TrafficSource instance
		/// </summary>
		/// <returns><see cref="IEnumerable{ViewModels.TrafficSource}"/></returns>
		[Route("api/v0/trafficSources/")]
		public IEnumerable<ViewModels.TrafficSource> GetV0() {
			return new List<ViewModels.TrafficSource>() { 
				new ViewModels.TrafficSource() { 
					Id = 1,
					TrackingId = "trk_id_1",
					Name = "Fake Traffic Source"
				},
				new ViewModels.TrafficSource() {
					Id = 7,
					TrackingId = "trk_id_2",
					Name = "Fake Traffic Source 2 updated"
				},
				new ViewModels.TrafficSource() {
					Id = 8,
					TrackingId = "trk_id_3",
					Name = "Fake Traffic Source 3 updated"
				},
				new ViewModels.TrafficSource() {
					Id = 9,
					TrackingId = "trk_000",
					Name = "Fake Traffic Source 4 updated"
				},
			};
		}
	}
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
}