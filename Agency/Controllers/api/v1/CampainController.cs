﻿using AgencyLib;
using Data = AgencyLib.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;
using System;

namespace Agency.Controllers.api {
	/// <summary>
	/// Campain CRUD API Controller 
	/// </summary>
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
	public partial class CampainController : ApiController {
		private readonly ICampainRepository _campainRepository;
		private readonly IOfferRepository _offerRepository;
		private readonly ITrafficSourceRepository _trafficSourceRepository;
		private readonly IMapper _mapper;

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="campainRepository"><see cref="ICampainRepository"/></param>
		/// <param name="offerRepository"><see cref="IOfferRepository"/></param>
		/// <param name="trafficSourceRepository"><see cref="ITrafficSourceRepository"/></param>
		/// <param name="mapper"><see cref="IMapper"/></param>
		public CampainController(ICampainRepository campainRepository,
															IOfferRepository offerRepository,
															ITrafficSourceRepository trafficSourceRepository,
															IMapper mapper) {
			_campainRepository = campainRepository;
			_offerRepository = offerRepository;
			_trafficSourceRepository = trafficSourceRepository;
			_mapper = mapper;
		}
		[Route("api/v1/Campains/watch")]
		[HttpGet()]
		public void Watch() {
			_campainRepository.WatchRemoteAPI();
		}

		/// <summary>
		/// Get all instance of campain
		/// </summary>
		/// <param name="request"></param>
		/// <returns>List of Campain</returns>
		[Route("api/v1/Campains/")]
		public ViewModels.CampainResponse Get([FromUri]ViewModels.CampainRequest request) {
			request = request ?? new ViewModels.CampainRequest() { Limit = 10 };
			var clf = request.ToCampainListFilter(_mapper);
			IEnumerable<Data.Campain> cmps = _campainRepository.Get(clf);
			var results = cmps.ToViewModel(_mapper);
			var rowCount = cmps.Count();
			return new ViewModels.CampainResponse() {
				Campains = results.Skip(request.Page * request.Limit).Take(request.Limit),
				RowCount = rowCount
			};
		}


		/// <summary>
		/// GET specific Campain instance
		/// </summary>
		/// <example>api/Campain/5</example>
		/// <param name="id">The id of the Campain</param>
		/// <returns><see cref="ViewModels.Campain"/></returns>
		[Route("api/v1/Campains/{id}")]
		public ViewModels.Campain Get(int id) {
			return _campainRepository.Get(id).ToViewModel(_mapper);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		//[Route("api/Campains/Requests/{id}")]
		//public ViewModels.CampainRequest GetRequest(int id) { 
		//	return _campainRepository.Get(id).ToRequestViewModel(_mapper);
		//}
		/// <summary>
		/// POST a new Campain record
		/// </summary>
		/// <example>api/Campain/</example>
		/// <param name="value"><see cref="ViewModels.CampainRequest"/></param>
		//[Route("api/v1/Campains/")]
		//public void Post(ViewModels.CampainRequest value) {
		//	var offer = _offerRepository.Get(value.OfferId, value.NetworkId);
		//	var trafficSource = _trafficSourceRepository.Get(value.TrafficSourceId);
		//	var c = new Data.Campain() {
		//		Name = value.Name,
		//		//TrackingId = value.TrackingId,
		//		TrafficSource = trafficSource,
		//		//Offer = offer,
		//		//Payout = value.PayoutAmount,
		//		//Status = value.Status,
		//		// UNDONE: insert here logics against payouts for statistics
		//		#region 
		//		//Payouts = new List<Data.CampainPayout>() 
		//		//{
		//		//	new Data.CampainPayout() {
		//		//		DateOpen = DateTime.Now,
		//		//		Payout = value.PayoutAmount,
		//		//	}
		//		//}
		//		#endregion
		//	};
		//	_campainRepository.Post(c);
		//}
		///// <summary>
		///// PUT and update an existent Campain
		///// </summary>
		///// <example>api/Campain/5</example>
		///// <param name="id">The primary key of Campain</param>
		///// <param name="value"><see cref="ViewModels.Campain"/></param>
		//[Route("api/v1/Campains/{id}")]
		//public void Put(int id, [FromBody]ViewModels.CampainRequest value) {
		//	var offer = _offerRepository.Get(value.OfferId, value.NetworkId);
		//	var trafficSource = _trafficSourceRepository.Get(value.TrafficSourceId);
		//	var model = value.ToModel(_mapper);
		//	//model.Offer = offer;
		//	model.TrafficSource = trafficSource;
		//	//model.Payout = value.PayoutAmount;
		//	// UNDONE: insert here logics against payouts for statistics
		//	#region
		//	//model.Payouts = new List<Data.CampainPayout>();
		//	//model.Payouts.Add(new Data.CampainPayout() {DateOpen = DateTime.Now, CampainId = model.Id, Payout = value.PayoutAmount });
		//	#endregion
		//	_campainRepository.Put(model);
		//}
		/// <summary>
		/// DELETE an instance of Campain
		/// </summary>
		/// <example>api/Campain/5</example>
		/// <param name="id">The primary key of the Campain to delete</param>
		//[Route("api/v1/Campains/{id}")]
		//public void Delete(int id) {
		//	_campainRepository.Delete(id);
		//}
	}
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
}