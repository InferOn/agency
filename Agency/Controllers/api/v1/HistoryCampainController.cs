﻿using AgencyLib;
using AgencyLib.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;
using Agency.ViewModels;

namespace Agency.Controllers.api {
	public partial class HistoryCampainController : ApiController {
		private readonly IHistoryCampainRepository _historyCampainRepository;
		private readonly IMapper _mapper;
		public HistoryCampainController(IHistoryCampainRepository historyCampainRepository, IMapper mapper) {
			_historyCampainRepository = historyCampainRepository;
			_mapper = mapper;
		}
		[Route("api/v1/historyCampain/")]
		public HistoryCampainResponse Get([FromUri]HistoryCampainRequest request) {
			request = request ?? new HistoryCampainRequest() { Limit = 10 };
			CampainHistoryListFilter chlf = request.ToHistoryCampainListFilter(_mapper);
			chlf.Limit = chlf.Limit <= 0 ? chlf.Limit = 10 : chlf.Limit;
			IEnumerable<AgencyLib.Data.CampainHistory> pre_ts = _historyCampainRepository.Get(chlf);
			var ts = pre_ts.Select(x => x.ToViewModel(_mapper));
			return new HistoryCampainResponse() {
				HistoryCampains = ts,
				RowCount = ts.Count()
			};
		}
		[Route("api/v1/historyCampain/")]
		[HttpPost]
		public HistoryCampainSaveResponse AddNewHistoryCampain([FromBody]ViewModels.CampainHistory campain) {
			var result = _historyCampainRepository.Create(campain.ToModel(_mapper));
			return new HistoryCampainSaveResponse() { };
		}
		[Route("api/v1/historyCampain/")]
		[HttpPut]
		public HistoryCampainSaveResponse UpdateHistoryCampain([FromBody]ViewModels.CampainHistory campain) {
			var result = _historyCampainRepository.Update(campain.ToModel(_mapper));
			return new HistoryCampainSaveResponse() { };
		}
		[Route("api/v1/historyCampain/Delete/")]
		[HttpPost]
		public HistoryCampainSaveResponse DeleteHistoryCampain([FromBody]int[] campaignIds) {
			var result = _historyCampainRepository.Delete(campaignIds);
			return new HistoryCampainSaveResponse() { };
		}
	}
}