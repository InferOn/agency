﻿using AgencyLib;
using AgencyLib.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;
using Agency.ViewModels;

namespace Agency.Controllers.api {
	/// <summary>
	/// TrafficSource CRUD API Controller 
	/// </summary>
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning disable CS1658 // Warning is overriding an error
	public partial class TrafficSourceController : ApiController {
	private readonly ITrafficSourceRepository _trafficSourceRepository;
		private readonly IMapper _mapper;
		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="trafficSourceRepository"><see cref="ITrafficSourceRepository"/></param>
		/// <param name="mapper"><see cref="IMapper"/></param>
		public TrafficSourceController(ITrafficSourceRepository trafficSourceRepository, IMapper mapper) {
			_trafficSourceRepository = trafficSourceRepository;
			_mapper = mapper;
		}
		/// <summary>
		/// GET every TrafficSource instance
		/// </summary>
		/// <returns><see cref="IEnumerable{ViewModels.TrafficSource}"/></returns>
		[Route("api/v1/trafficsources/")]
		[Authorize]
		public TrafficSourceResponse Get([FromUri]TrafficSourceRequest request) {
			request = request ?? new TrafficSourceRequest() { Limit = 10 };
			var tsf = request.ToTrafficSourceListFilter(_mapper);
			tsf.Limit = tsf.Limit <= 0 ? tsf.Limit = 10 : tsf.Limit;
			var pre_ts = _trafficSourceRepository.Get(tsf);
			var ts = pre_ts.Select(x => x.ToViewModel(_mapper));
			return new TrafficSourceResponse() {
				TrafficSources = ts,
				RowCount = !string.IsNullOrEmpty(request.Name) ? ts.Count() : (ts.Any() ? ts.Count() : 0)
			};
		}
		/// <summary>
		/// GET specific TrafficSource instance
		/// </summary>
		/// <param name="id">The id of the TrafficSource</param>
		/// <returns><see cref="ViewModels.TrafficSource"/></returns>
		[Route("api/v1/trafficSources/{id}")]
		public ViewModels.TrafficSource Get(int id) {
			return _trafficSourceRepository.Get(id).ToViewModel(_mapper);
		}
		/// <summary>
		/// POST a new TrafficSource record
		/// </summary>
		/// <param name="value"><see cref="TrafficSource"/></param>
		[Route("api/v1/trafficSources/")]
		public void Post([FromBody]AgencyLib.Data.TrafficSource value) {
			_trafficSourceRepository.Post(value);
		}
		/// <summary>
		/// PUT and update an existent TrafficSource
		/// </summary>
		/// <param name="id">The primary key of TrafficSource</param>
		/// <param name="value"><see cref="TrafficSource"/></param>
		[Route("api/v1/trafficSources/{id}")]
		public void Put(int id, [FromBody]AgencyLib.Data.TrafficSource value) {
			_trafficSourceRepository.Put(value);
		}
		/// <summary>
		/// DELETE an instance of TrafficSource
		/// </summary>
		/// <param name="id">The primary key of the TrafficSource to delete</param>
		[Route("api/v1/trafficSources/{id}")]
		public void Delete(int id) {
			_trafficSourceRepository.Delete(id);
		}
	}
#pragma warning restore CS1658 // Warning is overriding an error
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
}