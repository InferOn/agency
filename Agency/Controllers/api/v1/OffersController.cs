﻿using AgencyLib;
using Data = AgencyLib.Data;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using AutoMapper;
using Agency.ViewModels;
using AgencyLib.Providers.Resolvers.Targets;
using AgencyLib.Providers.Resolvers;

namespace Agency.Controllers.api {
	/// <summary>
	/// Offers CRUD API Controller 
	/// </summary>
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning disable CS1658 // Warning is overriding an error
	public partial class OffersController : ApiController {
		private readonly IOfferRepository _offerRepository;
		private readonly IMapper _mapper;
		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="offerRepository"><see cref="IOfferRepository"/></param>
		/// <param name="mapper"><see cref="IMapper"/></param>
		public OffersController(IOfferRepository offerRepository, IMapper mapper) {
			_offerRepository = offerRepository;
			_mapper = mapper;
		}


		[Route("api/v1/offers/requestAccess/{networkId}/{offerId}")]
		[HttpGet()]
		public bool RequestAccess(int networkId, int offerId) { 
			return _offerRepository.RequestAccess(networkId, offerId);
		}

		[Route("api/v1/offers/creativities/{networkId}/{offerId}")]
		[HttpGet()]
		public CreativitiesResult FetchCreativities(int networkId, int offerId) {
			return _offerRepository.FetchCreativities(networkId, offerId);
		}

		/// <summary>
		/// Track changes across registered affiliate networks
		/// </summary>
		[Route("api/v1/offers/watch")]
		[HttpGet()]
		public void Watch() {
			_offerRepository.WatchRemoteAPI();
		}
		/// <summary>
		/// Notify offer changes across registered affiliate networks
		/// </summary>
		[Route("api/v1/offers/notify")]
		[HttpGet()]
		public void Notify() {
			_offerRepository.NotifyOffers();
		}
		/// <summary>
		/// GET offers by list filter
		/// </summary>
		/// <example>api/Offer/</example>
		/// <returns><see cref="IEnumerable{ViewModels.OfferResponse}"/></returns>
		/// 
		[Route("api/v1/offers/")]
		[Authorize]
		//public OfferResponse Get([FromUri]ViewModels.OfferRequest request) {
		//	request = request ?? new ViewModels.OfferRequest() { Limit = 10 };
		//	var olf = request.ToOfferListFilter(_mapper);
		//	olf.Limit = olf.Limit <= 0 ? olf.Limit = 10 : olf.Limit;
		//	IEnumerable<Data.OfferData> pre_offers = _offerRepository.Get(olf);
		//	IEnumerable<Offer> offers = pre_offers.Select(x => x.ToViewModel(_mapper));
		//	return new OfferResponse() {
		//		Offers = offers,
		//		RowCount = !string.IsNullOrEmpty(request.Name) ? offers.Count() : (offers.Any() ? offers.First().Total_Rows : 0)
		//	};
		//}
		public OfferResponse Get([FromUri]ViewModels.OfferRequest request) {
			request = request ?? new ViewModels.OfferRequest() { Limit = 10 };
			var olf = request.ToOfferListFilter(_mapper);
			olf.Limit = olf.Limit <= 0 ? olf.Limit = 10 : olf.Limit;
			IEnumerable<Data.OfferData> results = _offerRepository.Get(olf);
			//var results = offers.ToViewModel(_mapper);
			var rowCount = results.Count();

			var result = new OfferResponse() {
				Offers = results.Skip(request.Page * request.Limit).Take(request.Limit).ToList(),
				RowCount = rowCount
			};

			return result;
		}


		/// <summary>
		/// GET specific Offer instance
		/// </summary>
		/// <param name="offerId">The id of the Offer</param>
		/// <param name="networkId">The id of the Network</param>
		/// <returns><see cref="ViewModels.Offer"></see></returns>
		[HttpGet()]
		[Route("api/v1/offers/{networkId}/{offerId}")]
		public ViewModels.Offer Get(int offerId, int networkId) {
			return _offerRepository.Get(offerId, networkId).ToViewModel(_mapper);
		}
		/// <summary>
		/// Get offers by network id
		/// </summary>
		/// <param name="networkId">PK of network</param>
		/// <returns></returns>
		[HttpGet()]
		[Route("api/v1/offers/{networkId}/")]
		public IEnumerable<ViewModels.Offer> Get(int networkId) {
			return _offerRepository.Get(networkId).ToViewModel(_mapper);
		}
		/// <summary>
		/// POST a new Offer record
		/// </summary>
		/// <example><example>api/Offer/</example></example>
		/// <param name="value"><see cref="Data.Offer"/></param>
		/// <returns><see cref="HttpResponseMessage"/></returns>
		[Route("api/v1/offers/")]
		public HttpResponseMessage Post([FromBody]Data.Offer value) {
			try {
				_offerRepository.Post(value);
			}
			catch (System.Exception ex) {
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
			return Request.CreateResponse(HttpStatusCode.OK);
		}
		/// <summary>
		/// PUT and update an existent Offer
		/// </summary>
		/// <example>api/Offer/5</example>
		/// <param name="id">The primary key of Offer</param>
		/// <param name="value"><see cref="Data.Offer"></see></param>
		/// <returns><see cref="HttpResponseMessage"/></returns>
		[Route("api/v1/offers/{id}")]
		public HttpResponseMessage Put(int id, [FromBody]Data.Offer value) {
			try {
				_offerRepository.Put(value);

			}
			catch (System.Exception ex) {
				return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
			return Request.CreateResponse(HttpStatusCode.OK);
		}
		/// <summary>
		/// DELETE an Offer from repository
		/// </summary>
		/// <param name="id"></param>
		[Route("api/v1/offers/{id}")]
		public void Delete(int id) {
			_offerRepository.Delete(id);
		}
	}
}