﻿using AgencyLib.Data;
using AutoMapper;
using System.Web.Http;
using log4net;
using AgencyLib.Repositories;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace Agency.Controllers.api {
	/// <summary>
	/// Oauth2 Account Controller
	/// </summary>
	public partial class AccountController : ApiController {
		#region members
		private readonly IAuthRepository _authRepository;
		private readonly IMapper _mapper;
		private readonly ILog _logger;
		#endregion
		/// <summary>
		/// default ctor
		/// </summary>
		/// <param name="authRepository">SQL authorization repository instance</param>
		/// <param name="mapper">Singleton Automapper mapper instance</param>
		/// <param name="logger">Singleton Log4Net logger instance</param>
		public AccountController(IAuthRepository authRepository, IMapper mapper, ILog logger) {
			_authRepository = authRepository;
			_mapper = mapper;
			_logger = logger;
		}
		/// <summary>
		/// Register a new user
		/// </summary>
		/// <param name="userModel"></param>
		/// <returns>Http action result</returns>
		[AllowAnonymous]
		[Route("api/v1/Auth/Register/")]
		public async Task<IHttpActionResult> Register(UserModel userModel) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}
			IdentityResult result = await _authRepository.RegisterUser(userModel);
			IHttpActionResult errorResult = GetErrorResult(result);
			if (errorResult != null) {
				return errorResult;
			}
			return Ok();
		}
		
		[AllowAnonymous]
		[Route("api/v1/Auth/Delete/")]
		public async Task<IHttpActionResult> Delete(UserModel userModel) {
			if (!ModelState.IsValid) {
				return BadRequest(ModelState);
			}
			IdentityResult result = await _authRepository.DeleteUser(userModel);
			IHttpActionResult errorResult = GetErrorResult(result);
			if (errorResult != null) {
				return errorResult;
			}
			return Ok();
		}

		[Route("api/v1/Auth/Users/")]
		public IEnumerable<IdentityUser> GetAll() {
			return _authRepository.GetAll();
		}

		private IHttpActionResult GetErrorResult(IdentityResult result) {
			if (result == null) {
				return InternalServerError();
			}
			if (!result.Succeeded) {
				if (result.Errors != null) {
					foreach (string error in result.Errors) {
						ModelState.AddModelError("", error);
					}
				}
				if (ModelState.IsValid) {
					// No ModelState errors are available to send, so just return an empty BadRequest.
					return BadRequest();
				}
				return BadRequest(ModelState);
			}
			return null;
		}
	}
}