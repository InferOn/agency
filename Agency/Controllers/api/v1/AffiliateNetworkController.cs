﻿using AgencyLib;
using AgencyLib.Data;
using AutoMapper;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Agency.Utils;
using log4net;
using Agency.ViewModels;

namespace Agency.Controllers.api {
#pragma warning disable CS1658 // Warning is overriding an error
#pragma warning disable CS1584 // XML comment has syntactically incorrect cref attribute
	/// <summary>
	/// AffiliateNetwork CRUD API Controller 
	/// </summary>
	public partial class AffiliateNetworkController : ApiController {
		private readonly IAffiliateNetworkRepository _affiliateNetworkRepository;
		private readonly IMapper _mapper;
		private readonly ILog _logger;
		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="affiliateNetworkRepository"><see cref="IAffiliateNetworkRepository"/></param>
		/// <param name="mapper"><see cref="IMapper"/></param>
		/// <param name="logger"><see cref="ILog"/></param>
		public AffiliateNetworkController(IAffiliateNetworkRepository affiliateNetworkRepository, IMapper mapper, ILog logger) {
			_affiliateNetworkRepository = affiliateNetworkRepository;
			_mapper = mapper;
			_logger = logger;
		}
		/// <summary>
		/// GET every Affiliate Network instance
		/// </summary>
		/// <example>api/AffiliateNetwork/</example>
		/// <returns><see cref="IEnumerable{ViewModels.AffiliateNetwork}"/></returns>
		//[Authorize]
		[Route("api/v1/AffiliateNetworks/")]
		public AffiliateNetworkResponse Get([FromUri]ViewModels.AffiliateNetworkRequest request) {
			var alf = new AffiliateNetworkListFilter();
			if (request == null) {
				alf.Limit = 10;
			}
			else {
				alf.Page = request.Page;
				alf.Limit = request.Limit;
				alf.Name = request.Name;
			}
			IEnumerable<ViewModels.AffiliateNetwork> vm = _affiliateNetworkRepository.Get(alf).Select(x => x.ToViewModel(_mapper));
			return new AffiliateNetworkResponse() {
				Networks = vm,
				RowCount = vm.Count()
			};
		}
		/// <summary>
		/// GET specific Affiliate Network instance
		/// </summary>
		/// <example>api/AffiliateNetwork/5</example>
		/// <param name="id">The id of the affiliate network</param>
		/// <returns>ViewModels.AffiliateNetwork</returns>
		[Route("api/v1/AffiliateNetworks/{id}")]
		public ViewModels.AffiliateNetwork Get(int id) {
			return _affiliateNetworkRepository.Get(id).ToViewModel(_mapper);
		}
		/// <summary>
		/// POST a new Affiliate Network instance
		/// </summary>
		/// <example>api/AffiliateNetwork/</example>
		/// <param name="value"><see cref="AffiliateNetwork"/></param>
		[Route("api/v1/AffiliateNetworks/")]
		public void Post(ViewModels.NetworkRequest value) {
			_affiliateNetworkRepository.Post(value.ToModel(_mapper));
		}
		[Route("api/v1/AffiliateNetworks/Delete/")]
		[HttpPost]
		public AffiliateNetworkResponse DeleteHistoryCampain([FromBody]int[] networkIds) {
			var result = _affiliateNetworkRepository.Delete(networkIds);
			return new AffiliateNetworkResponse() { };
		}
		/// <summary>
		/// PUT and update an existent Affiliate Network
		/// </summary>
		/// <example>api/AffiliateNetwork/5</example>
		/// <param name="id">The primary key of affiliate network</param>
		/// <param name="value"><see cref="AffiliateNetwork"/></param>
		[Route("api/v1/AffiliateNetworks/{id}")]
		[HttpPut]
		public AffiliateNetworkResponse Put(int id, [FromBody]ViewModels.AffiliateNetwork value) {
			_affiliateNetworkRepository.Put(value.ToModel(_mapper));
			return new AffiliateNetworkResponse() { };
		}
	}
#pragma warning restore CS1584 // XML comment has syntactically incorrect cref attribute
#pragma warning restore CS1658 // Warning is overriding an error
}