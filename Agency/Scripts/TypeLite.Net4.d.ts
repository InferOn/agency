﻿
 
 

 


declare namespace ViewModels {
	interface AffiliateNetwork {
		AccountSettings: string;
		Id: number;
		LastGet: Date;
		Name: string;
		TrackingId: string;
	}
	interface Campain {
		Id: number;
		Name: string;
		Offer: ViewModels.Offer;
		Payout: number;
		Payouts: ViewModels.CampainPayout[];
		Status: number;
		TrackingId: string;
		TrafficSource: ViewModels.TrafficSource;
	}
	interface CampainPayout {
		CampainId: number;
		Date: Date;
		Id: number;
		Payout: number;
	}
	interface CampainRequest {
		Id: number;
		Name: string;
		NetworkId: number;
		OfferId: number;
		PayoutAmount: number;
		Status: number;
		TrackingId: string;
		TrafficSourceId: number;
	}
	interface NetworkRequest {
		AccountSettings: string;
		Id: number;
		Name: string;
		TrackingId: string;
		Note: string;
		Active: boolean;
		LastGet: Date;
	}
	interface Offer {
		allow_direct_links: string;
		allow_website_links: string;
		AppCategories: string;
		AppDescription: string;
		AppID: number;
		AppOS: string;
		AppPrice: string;
		AppRating: number;
		approval_status: string;
		ConversionCap: number;
		ConversionType: string;
		Countries: string;
		Currency: string;
		DateExpiration: Date;
		default_goal_name: string;
		default_payout: string;
		Description: string;
		dne_download_url: string;
		dne_list_id: string;
		dne_third_party_list: string;
		dne_unsubscribe_url: string;
		email_instructions: string;
		email_instructions_from: string;
		email_instructions_subject: string;
		featured: string;
		has_goals_enabled: string;
		InitialConversionCap: number;
		InitialPayoutCap: number;
		is_expired: string;
		Name: string;
		Network: ViewModels.AffiliateNetwork;
		NetworkId: number;
		NetworkName: string;
		OfferId: number;
		payout_type: string;
		PayoutCap: number;
		Payouts: ViewModels.OfferPayout[];
		percent_payout: string;
		preview_url: string;
		require_approval: string;
		require_terms_and_conditions: string;
		show_custom_variables: string;
		show_mail_list: string;
		Status: string;
		StatusValidation: AgencyLib.Data.eStatusValidation;
		terms_and_conditions: string;
		Total_Rows: number;
		TrackingId: string;
		TrackingURL: string;
		use_target_rules: string;
	}
	interface OfferPayout {
		Date: Date;
		Id: number;
		OfferId: number;
		Payout: number;
	}
	interface TrafficSource {
		Id: number;
		Name: string;
		TrackingId: string;
	}
}


