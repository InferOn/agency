﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Agency {
	/// <summary>
	/// Route configurations
	/// </summary>
	public class RouteConfig {
		/// <summary>
		/// Routes registration
		/// </summary>
		/// <param name="routes">Route collection to enrich</param>
		public static void RegisterRoutes(RouteCollection routes) {
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
					name: "Default",
					url: "{controller}/{action}/{id}"/*,*/
					//defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
