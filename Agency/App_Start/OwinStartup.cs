﻿using System;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;

[assembly: OwinStartup(typeof(Agency.App_Start.Startup))]
namespace Agency.App_Start {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class Startup {
		public void Configuration(IAppBuilder app) {
			ConfigureOAuth(app);
			app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
		}
		private void ConfigureOAuth(IAppBuilder app) {
			var authRepository = new AgencyLib.Repositories.AuthRepository(System.Configuration.ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString);
			OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions() {
				AllowInsecureHttp = true,
				TokenEndpointPath = new PathString("/api/v1/Auth/token"),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(365),
				
				Provider = new AgencyAuthorizationServerProvider(authRepository)
			};
			app.UseOAuthBearerTokens(OAuthServerOptions);
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}