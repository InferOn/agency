using System.Web.Http;
using WebActivatorEx;
using Agency;
using Swashbuckle.Application;
using System.Reflection;
using System;
using System.Web.Http.Description;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]
namespace Agency {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class SwaggerConfig {
		public static void Register() {
			var thisAssembly = typeof(SwaggerConfig).Assembly;
			var currentAssembly = Assembly.GetAssembly(typeof(SwaggerConfig));

			GlobalConfiguration.Configuration
					.EnableSwagger("docs/{apiVersion}/api", c => {
						c.MultipleApiVersions(
								(apiDesc, targetApiVersion) => ResolveVersionSupportByRouteConstraint(apiDesc, targetApiVersion),
								(vc) => {
									vc.Version("v0", "0.0.1 alpha");
									vc.Version("v1", "1.0.0 beta");
								});
						c.UseFullTypeNameInSchemaIds();
						c.IncludeXmlComments(System.Web.Hosting.HostingEnvironment.MapPath("~/bin/datamodel.XML"));
					})
					.EnableSwaggerUi("docs/{*assetPath}", c => {
						c.EnableDiscoveryUrlSelector();
						c.DisableValidator();
						c.InjectStylesheet(currentAssembly, "Agency.Content.swagger_override.css");
						c.InjectJavaScript(currentAssembly, "Agency.Content.swagger_override.js");
						c.DocExpansion(DocExpansion.List);
					});
		}
		private static bool ResolveVersionSupportByRouteConstraint(ApiDescription apiDesc, string targetApiVersion) {
			return apiDesc.ID.Contains("/" + targetApiVersion + "/");
		}
	}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}
