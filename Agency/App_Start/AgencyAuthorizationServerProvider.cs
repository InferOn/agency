﻿using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using AgencyLib.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace Agency.App_Start {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class AgencyAuthorizationServerProvider : OAuthAuthorizationServerProvider {
		private IAuthRepository _authRepository;
		public AgencyAuthorizationServerProvider(IAuthRepository authRepository): base() {
			_authRepository = authRepository;
		}
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
		
			context.Validated();
		}
		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
			context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
			//CR-SCORE: 50
			_authRepository = new AuthRepository(System.Configuration.ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString);
			IdentityUser user = await _authRepository.FindUser(context.UserName, context.Password);
				if (user == null) {
					context.SetError("invalid_grant", "The user name or password is incorrect.");
					return;
				}
			var identity = new ClaimsIdentity(context.Options.AuthenticationType);
			//identity.AddClaim(new Claim("sub", context.UserName));
			//identity.AddClaim(new Claim("role", "user"));
			context.Validated(identity);
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}