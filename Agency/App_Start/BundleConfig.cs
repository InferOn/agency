﻿using System.Web.Optimization;
using System.Collections;
using System.Collections.Generic;
using Castle.DynamicProxy;
using log4net;
using System;

namespace Agency.App_Start {
	/// <summary>
	/// Bundles configurations
	/// </summary>
	public class BundleConfig {
		/// <summary>
		/// Bundles registrations
		/// </summary>
		/// <param name="bundles">bundle collection to enrich</param>
		public static void RegisterBundles(BundleCollection bundles) {
			bundles.Add(new StyleBundle("~/bundles/styles")
							.Include("~/Content/CSS/material/css/material-design-iconic-font.min.css")
							.Include("~/Content/CSS/material/fonts/Material-Design-Iconic-Font.woff")
							.Include("~/Content/CSS/material/fonts/Material-Design-Iconic-Font.woff2")
							.Include("~/Content/CSS/material/fonts/Material-Design-Iconic-Font.ttf")
							.Include("~/Content/CSS/material/fonts/Material-Design-Iconic-Font.svg")
							.Include("~/Content/CSS/angular-material-1-1-3.min.css")

							.Include("~/Content/CSS/ag-grid.css")
							.Include("~/Content/CSS/theme-dark.css")
							.Include("~/Content/CSS/theme-dark.css")
							);
			/*
			*	Other AG-Grid skins available	
			* 
				<link href="~/Content/CSS/theme-blue.css" rel="stylesheet" type="text/css" />
				<link href="~/Content/CSS/theme-fresh.css" rel="stylesheet" type="text/css" />
				<link href="~/Content/CSS/theme-bootstrap.css" rel="stylesheet" type="text/css" />
				<link href="~/Content/CSS/theme-material.css" rel="stylesheet" type="text/css" />
			* 
			*/
			bundles.Add(
				new ScriptBundle("~/bundles/scripts")
					.Include(new string[] {
						"~/Scripts/jquery-1.10.2.min.js",
						"~/Scripts/amplify.min.js",
						"~/Scripts/ag-grid.min.js",
						"~/Scripts/angular-1-4-8.min.js",
						"~/Scripts/angular-ui-router-0-2-15.js",
						"~/Scripts/angular-animate-1-4-8.min.js",
						"~/Scripts/angular-aria-1-4-8.min.js",
						"~/Scripts/angular-messages-1-4-8.js",
						"~/Scripts/angular-material-1-1-3.min.js",
					})
			);
			bundles.Add(
				new ScriptBundle("~/bundles/app")
					.Include(new string[] {
						"~/app/Utils/interfaces.js",
						"~/app/Utils/Helper.js",
						"~/app/Services/apiService.js",

						"~/app/Network/NetworkController.js",
						"~/app/Network/NetworkDialogBaseController.js",
						"~/app/Network/NetworkDialogController.js",
						"~/app/Network/NetworkEditDialogController.js",
						
						"~/app/Campain/CampainController.js",
						"~/app/Campain/CampainDialogBaseController.js",
						"~/app/Campain/CampainDialogController.js",
						"~/app/Campain/CampainEditDialogController.js",

						"~/app/Offers/OffersController.js",

						"~/app/TrafficSources/TrafficSourcesBaseDialogController.js",
						"~/app/TrafficSources/TrafficSourcesController.js",
						"~/app/TrafficSources/TrafficSourcesDialogController.js",
						"~/app/TrafficSources/TrafficSourcesEditDialogController.js",

						"~/app/Utils/_toolbarController.js",
						"~/app/Utils/_contentController.js",
						"~/app/agencyapp.js",
					})
			);
		}
	}
}