﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Data = AgencyLib.Data;

namespace Agency.App_Start {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public static class AutomapperProfile {
		public static void Configure(IMapperConfigurationExpression cfg) {
			cfg.CreateMap<ViewModels.OfferRequest, Data.OfferListFilter>();
			cfg.CreateMap<ViewModels.TrafficSourceRequest, Data.TrafficSourceListFilter>();
			cfg.CreateMap<ViewModels.CampainRequest, Data.CampainListFilter>();

			cfg.CreateMap<Data.AffiliateNetwork, ViewModels.AffiliateNetwork>();
			cfg.CreateMap<ViewModels.AffiliateNetwork, Data.AffiliateNetwork>();
			cfg.CreateMap<Data.Campain, ViewModels.Campain>();

			cfg.CreateMap<ViewModels.CampainRequest, Data.Campain>();
			cfg.CreateMap<IEnumerable<ViewModels.CampainRequest>, IEnumerable<Data.Campain>>();
			
			cfg.CreateMap<ViewModels.CampainHistory, Data.CampainHistory>();
			cfg.CreateMap<Data.CampainHistory, ViewModels.CampainHistory>();

			cfg.CreateMap<IEnumerable<ViewModels.CampainHistory>, IEnumerable<Data.CampainHistory>>();
			cfg.CreateMap<IEnumerable<Data.CampainHistory>, IEnumerable<ViewModels.CampainHistory>>();


			cfg.CreateMap<ViewModels.HistoryCampainRequest, Data.CampainHistoryListFilter>();
			cfg.CreateMap<Data.CampainHistoryListFilter, ViewModels.HistoryCampainRequest>();

			cfg.CreateMap<Data.CampainPayout, ViewModels.CampainPayout>();
			cfg.CreateMap<Data.Offer, ViewModels.Offer>();
			cfg.CreateMap<Data.OfferData, ViewModels.Offer>();
			cfg.CreateMap<ViewModels.Offer, Data.OfferData>();
			cfg.CreateMap<Data.OfferPayout, ViewModels.OfferPayout>();
			cfg.CreateMap<Data.TrafficSource, ViewModels.TrafficSource>();
			cfg.CreateMap<ViewModels.NetworkRequest, Data.AffiliateNetwork>();
			cfg.CreateMap<Data.AffiliateNetwork, ViewModels.NetworkRequest>();
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}