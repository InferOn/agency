using System.Web.Http;
using Agency.DependencyResolution;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(Agency.App_Start.StructuremapWebApi), "Start")]

namespace Agency.App_Start {

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public static class StructuremapWebApi {
		public static void Start() {
			var container = StructuremapMvc.StructureMapDependencyScope.Container;
			GlobalConfiguration.Configuration.DependencyResolver = new StructureMapWebApiDependencyResolver(container);
		}
	}
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
}