using Agency.App_Start;

using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(StructuremapMvc), "Start")]
[assembly: ApplicationShutdownMethod(typeof(StructuremapMvc), "End")]

namespace Agency.App_Start {
	using System.Web.Mvc;

	using Microsoft.Web.Infrastructure.DynamicModuleHelper;

	using Agency.DependencyResolution;

	using StructureMap;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public static class StructuremapMvc {
		#region Public Properties

		public static StructureMapDependencyScope StructureMapDependencyScope { get; set; }

		#endregion

		#region Public Methods and Operators

		public static void End() {
			StructureMapDependencyScope.Dispose();
		}

		public static void Start() {
			IContainer container = IoC.Initialize();
			StructureMapDependencyScope = new StructureMapDependencyScope(container);
			DependencyResolver.SetResolver(StructureMapDependencyScope);
			DynamicModuleUtility.RegisterModule(typeof(StructureMapScopeModule));
		}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

		#endregion
	}
}