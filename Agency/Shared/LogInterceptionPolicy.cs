namespace Agency.Utils {
	using AgencyLib;
	using AutoMapper;
	using StructureMap.Building.Interception;
	using System.Collections.Generic;
	using Utils;
	using StructureMap.Pipeline;
	using System;
	/// <summary>
	/// Policy for the log interception
	/// </summary>
	public class LogInterceptionPolicy : IInterceptorPolicy {
		public string Description {
			get { return "Just a loggin interception policy"; }
		}
		public IEnumerable<StructureMap.Building.Interception.IInterceptor> DetermineInterceptors(Type pluginType, Instance instance) {
			if (pluginType == typeof(IAgencyDataContext)) {
				yield return new FuncInterceptor<IAgencyDataContext>(i =>
										(IAgencyDataContext)
												DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(IAgencyDataContext), i));
			}
			if (pluginType == typeof(IOfferRepository)) {
				yield return new FuncInterceptor<IOfferRepository>(i =>
										(IOfferRepository)
												DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(IOfferRepository), i));
			}
			if (pluginType == typeof(IAffiliateNetworkRepository)) {
				yield return new FuncInterceptor<IAffiliateNetworkRepository>(i =>
										(IAffiliateNetworkRepository)
												DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(IAffiliateNetworkRepository), i));
			}
			if (pluginType == typeof(ICampainRepository)) {
				yield return new FuncInterceptor<ICampainRepository>(i =>
										(ICampainRepository)
												DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(ICampainRepository), i));
			}
			if (pluginType == typeof(ITrafficSourceRepository)) {
				yield return new FuncInterceptor<ITrafficSourceRepository>(i =>
										(ITrafficSourceRepository)
												DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(ITrafficSourceRepository), i));
			}
			if (pluginType == typeof(IMapper)) {
				yield return new FuncInterceptor<IMapper>(i =>
											(IMapper)
													DynamicProxyHelper.CreateInterfaceProxyWithTargetInterface(typeof(IMapper), i));
			}
		}
	}
}