﻿using Castle.DynamicProxy;
using log4net;

namespace Agency.Utils {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class LoggingInterceptor : IInterceptor {
		private readonly ILog _logger;
		public LoggingInterceptor(ILog logger) {
			_logger = logger;
		}
		public void Intercept(IInvocation invocation) {
			try {
				invocation.Proceed();
			}
			catch (System.Exception ex) {
				_logger.ErrorFormat("{0} - {1}\n{2}\n{3}",System.DateTime.Now.ToString("yyyy-MM-dd hh:mm"), 
																								  string.Format("{0}.{1}", invocation.Method.DeclaringType.FullName, invocation.Method.Name), 
																									ex.Message, 
																									ex.StackTrace);
				throw ex;
			}
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}