﻿using AutoMapper;
using System.Collections.Generic;
using Data = AgencyLib.Data;

namespace Agency.Utils {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public static class Mappings {

		public static Data.CampainHistoryListFilter ToHistoryCampainListFilter(this ViewModels.HistoryCampainRequest source, IMapper mapper) { 
			return mapper.Map<Data.CampainHistoryListFilter>(source);

		}


		public static Data.TrafficSourceListFilter ToTrafficSourceListFilter(this ViewModels.TrafficSourceRequest source, IMapper mapper) {
			return mapper.Map<Data.TrafficSourceListFilter>(source);
		}

		public static Data.OfferListFilter ToOfferListFilter(this ViewModels.OfferRequest source, IMapper mapper) {
			return mapper.Map<Data.OfferListFilter>(source);
		}

		public static Data.CampainListFilter ToCampainListFilter(this ViewModels.CampainRequest source, IMapper mapper) { 
			return mapper.Map<Data.CampainListFilter>(source);
		}

		public static Data.AffiliateNetwork ToModel(this ViewModels.NetworkRequest source, IMapper mapper) {
			return mapper.Map<Data.AffiliateNetwork>(source);
		}
		public static Data.Campain ToModel(this ViewModels.CampainRequest source, IMapper mapper) {
			return mapper.Map<Data.Campain>(source);
		}
		public static ViewModels.Campain ToViewModel(this Data.Campain source, IMapper mapper) {
			return mapper.Map<ViewModels.Campain>(source);
		}
		public static IEnumerable<ViewModels.Campain> ToViewModel(this IEnumerable<Data.Campain> source, IMapper mapper) {
			return mapper.Map<IEnumerable<ViewModels.Campain>>(source);
		}

		public static ViewModels.CampainRequest ToRequestViewModel(this Data.Campain source, IMapper mapper) {
			var map = mapper.Map<ViewModels.CampainRequest>(source);
			return map;
		}
		public static Data.Campain ToModel(this ViewModels.Campain source, IMapper mapper) {
			return mapper.Map<Data.Campain>(source);
		}
		public static ViewModels.AffiliateNetwork ToViewModel(this Data.AffiliateNetwork source, IMapper mapper) {
			return mapper.Map<ViewModels.AffiliateNetwork>(source);
		}
		public static Data.AffiliateNetwork ToModel(this ViewModels.AffiliateNetwork source, IMapper mapper) {
			return mapper.Map<Data.AffiliateNetwork>(source);
		}
		public static ViewModels.CampainPayout ToViewModel(this Data.CampainPayout source, IMapper mapper) {
			return mapper.Map<ViewModels.CampainPayout>(source);
		}
		public static Data.CampainPayout ToModel(this ViewModels.CampainPayout source, IMapper mapper) {
			return mapper.Map<Data.CampainPayout>(source);
		}

		public static ViewModels.OfferPayout ToViewModel(this Data.OfferPayout source, IMapper mapper) {
			return mapper.Map<ViewModels.OfferPayout>(source);
		}
		public static Data.OfferPayout ToModel(this ViewModels.OfferPayout source, IMapper mapper) {
			return mapper.Map<Data.OfferPayout>(source);
		}

		#region campain history
		public static ViewModels.CampainHistory ToViewModel(this Data.CampainHistory source, IMapper mapper) {
			return mapper.Map<ViewModels.CampainHistory>(source);
		}

		public static Data.CampainHistory ToModel(this ViewModels.CampainHistory source, IMapper mapper) {
			return mapper.Map<Data.CampainHistory>(source);
		}

		#endregion

		#region Traffic Sources
		public static ViewModels.TrafficSource ToViewModel(this Data.TrafficSource source, IMapper mapper) {
			return mapper.Map<ViewModels.TrafficSource>(source);
		}
		public static Data.TrafficSource ToModel(this ViewModels.TrafficSource source, IMapper mapper) {
			return mapper.Map<Data.TrafficSource>(source);
		}
		public static IEnumerable<ViewModels.TrafficSource> ToViewModel(this IEnumerable<Data.TrafficSource> source, IMapper mapper) {
			return mapper.Map<IEnumerable<ViewModels.TrafficSource>>(source);
		}
		#endregion

		#region Offers

		public static ViewModels.Offer ToViewModel(this Data.Offer source, IMapper mapper) {
			return mapper.Map<ViewModels.Offer>(source);
		}
		public static ViewModels.Offer ToViewModel(this Data.OfferData source, IMapper mapper) {
			return mapper.Map<ViewModels.Offer>(source);
		}
		public static IEnumerable<ViewModels.Offer> ToViewModel(this IEnumerable<Data.Offer> source, IMapper mapper) {
			return mapper.Map<IEnumerable<ViewModels.Offer>>(source);
		}
		public static Data.Offer ToModel(this Data.Offer source, IMapper mapper) {
			return mapper.Map<Data.Offer>(source);
		} 
		#endregion
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

}