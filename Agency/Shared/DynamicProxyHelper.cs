﻿using log4net;
using System;

namespace Agency.Utils {
	/// <summary>
	/// Helper to manage proxy invocation by interface instances
	/// </summary>
	public class DynamicProxyHelper {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
		public static object CreateInterfaceProxyWithTargetInterface(Type interfaceType, object concreteObject) {
			var dynamicProxy = new Castle.DynamicProxy.ProxyGenerator();

			var result = dynamicProxy.CreateInterfaceProxyWithTargetInterface(
					interfaceType,
					concreteObject,
					new[] { new LoggingInterceptor(LogManager.GetLogger("AgencyPlayground")) });

			return result;
		}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
	}
}