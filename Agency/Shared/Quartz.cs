﻿using AutoMapper;
using Quartz;
using Quartz.Impl;
using System.Collections.Generic;
using Data = AgencyLib.Data;

namespace Agency.Utils.Quartz {
	public class FetchJob : IJob {
		public void Execute(IJobExecutionContext context) {
			//todo: add here relevant operations
		}
	}
	public class JobScheduler {
		public static void Start() {
			IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
			scheduler.Start();

			IJobDetail job = JobBuilder.Create<FetchJob>().Build();

			ITrigger trigger = TriggerBuilder.Create()
					.WithDailyTimeIntervalSchedule
						(s =>
							 s.WithIntervalInMinutes(2)
							.OnEveryDay()
							.StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
						)
					.Build();

			scheduler.ScheduleJob(job, trigger);
		}
	}
}