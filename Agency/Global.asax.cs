﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Optimization;
using Agency.App_Start;
using Agency.Utils.Quartz;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Agency {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class Global : HttpApplication {
		void Application_Start(object sender, EventArgs e) {
			log4net.Config.XmlConfigurator.Configure();
			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			JobScheduler.Start();
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}