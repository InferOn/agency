﻿
module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		pkg: grunt.file.readJSON('./package.json'),
		paths: {
			src: {
				sass: './scss',
				js: './application/',
				img: './images',
			},
			dest: {
				css: './assets/css',
				js: './Script',
				img: './assets/images'
			}
		},
		sass: {
			dev: {
				options: {
					style: 'expanded'
				},
				files: [{
					expand: true,
					cwd: '<%= paths.src.sass %>',
					src: ['**/*.scss'],
					dest: '<%= paths.dest.css %>',
					ext: '.css'
				}]
			}
		},
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 9', 'ios 6', 'android 4'],
				map: true
			},
			files: {
				expand: true,
				flatten: true,
				src: '<%= paths.dest.css %>/*.css',
				dest: '<%= paths.dest.css %>'
			}
		},
		cssmin: {
			options: {
				keepSpecialComments: 1
			},
			minify: {
				expand: true,
				cwd: '<%= paths.dest.css %>',
				src: ['*.css', '!*.min.css'],
				dest: '<%= paths.dest.css %>',
				ext: '.min.css'
			}
		},
		imagemin: {
			png: {
				options: {
					optimizationLevel: 7
				},
				files: [
				{
					expand: true,
					cwd: '<%= paths.src.img %>',
					src: ['**/*.png'],
					dest: '<%= paths.dest.img %>',
					ext: '.png'
				}
				]
			},
			jpg: {
				options: {
					progressive: true
				},
				files: [
				{
					expand: true,
					cwd: '<%= paths.src.img %>',
					src: ['**/*.jpg'],
					dest: '<%= paths.dest.img %>',
					ext: '.jpg'
				}
				]
			}
		},
		ts: {
			default: {
				src: ["<%= paths.src.js %>/**/*.ts"],
				outDir: "./",
				options: {
					target: 'es5',
					noImplicitAny: false,
					removeComments: false,
					noEmitOnError: false,
					sourceMap: true,
					references: [
						"core",
						"dom",
						"./Scripts/typings/**/*.d.ts",
					],
				},
			}
		},
		watch: {
			sass: {
				files: '<%= paths.src.sass %>/**/*.{scss,sass}',
				tasks: ['sass', 'autoprefixer', 'cssmin']
			},
			scripts: {
				files: ["<%= paths.src.js %>/**/*.ts"],
				tasks: ['ts:default'],
				options: {
					spawn: false
				}
			},
			images: {
				files: ['<%= paths.src.img %>/**/*.{png,jpg,gif}'],
				tasks: ['newer:imagemin'],
				options: {
					spawn: false
				}
			}
		},
		browserSync: {
			dev: {
				bsFiles: {
					src: [
						'<%= paths.dest.css %>/*.css',
						'<%= paths.dest.js %>/**/*.js',
						'<%= paths.dest.img %>/**/*.{jpg,png}'
					]
				},
				options: {
					server: {
						baseDir: './'
					},
					watchTask: true,
					https: false,
					browser: 'google chrome'
				}
			}
		}
	});
	grunt.registerTask('default', ['browserSync', 'watch']);
};