namespace Agency.DependencyResolution {
	using System.Web;

	using App_Start;

	using StructureMap.Web.Pipeline;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public class StructureMapScopeModule : IHttpModule {
		#region Public Methods and Operators

		public void Dispose() {
		}

		public void Init(HttpApplication context) {
			context.BeginRequest += (sender, e) => StructuremapMvc.StructureMapDependencyScope.CreateNestedContainer();
			context.EndRequest += (sender, e) => {
				HttpContextLifecycle.DisposeAndClearAll();
				StructuremapMvc.StructureMapDependencyScope.DisposeNestedContainer();
			};
		}

		#endregion
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}