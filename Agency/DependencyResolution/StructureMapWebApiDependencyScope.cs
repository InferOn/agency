using System.Web.Http.Dependencies;
using StructureMap;

namespace Agency.DependencyResolution {
	/// <summary>
	/// The structure map web api dependency scope.
	/// </summary>
	public class StructureMapWebApiDependencyScope : StructureMapDependencyScope, IDependencyScope {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
		public StructureMapWebApiDependencyScope(IContainer container)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
						: base(container) {
		}
	}
}
