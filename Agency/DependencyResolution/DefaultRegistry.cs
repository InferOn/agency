namespace Agency.DependencyResolution {
	using AgencyLib;
	using App_Start;
	using AutoMapper;
	using log4net;
	using StructureMap.Pipeline;
	using StructureMap;
	using Utils;
	using AgencyLib.Repositories;

	/// <summary>
	/// Default dependency registry for StructureMap
	/// </summary>
	/// 
	public class DependencyRegistry : Registry {
		/// <summary>
		/// StructureMap dependency registry
		/// </summary>
		public DependencyRegistry() {
			Scan(scan => {
				scan.TheCallingAssembly();
				scan.WithDefaultConventions();
			});

			#region by web config
			var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString;
			var mimeMailer = System.Configuration.ConfigurationManager.AppSettings["MimeMailer"];
			var mimePort = System.Configuration.ConfigurationManager.AppSettings["MimePort"];
			var mimeUser = System.Configuration.ConfigurationManager.AppSettings["MimeUser"];
			var mimePassword = System.Configuration.ConfigurationManager.AppSettings["MimePassword"];
			var mimeFrom = System.Configuration.ConfigurationManager.AppSettings["MimeFrom"];
			var summaryAddresses = System.Configuration.ConfigurationManager.AppSettings["SummaryAddresses"];
			#endregion

			ForSingletonOf<ILog>().Use(LogManager.GetLogger("Agency"));
			ForSingletonOf<INotifierConfiguration>().Use(new NotifierConfiguration(mimeMailer, mimePort, mimeUser, mimePassword, mimeFrom, summaryAddresses));
			For<INotifier>().Use<Notifier>()
			.Ctor<INotifierConfiguration>();
			ForSingletonOf<IMapper>().Use(new MapperConfiguration(AutomapperProfile.Configure).CreateMapper());
			For<IAgencyDataContext>(Lifecycles.Transient).Use<AgencyContext>();
			For<IOfferRepository>(Lifecycles.Transient).Use<OfferRepository>()
			.Ctor<string>().Is(connectionString);
			For<IAffiliateNetworkRepository>(Lifecycles.Transient).Use<AffiliateNetworkRepository>()
			.Ctor<string>().Is(connectionString);
			For<ICampainRepository>(Lifecycles.Transient).Use<CampainRepository>()
			.Ctor<string>().Is(connectionString);
			For<ITrafficSourceRepository>(Lifecycles.Transient).Use<TrafficSourceRepository>()
			.Ctor<string>().Is(connectionString);
			For<IAuthRepository>(Lifecycles.Transient).Use<AuthRepository>()
			.Ctor<string>().Is(connectionString);
			For<IHistoryCampainRepository>(Lifecycles.Transient).Use<HistoryCampainRepository>()
			.Ctor<string>().Is(connectionString);
			Policies.Interceptors(new LogInterceptionPolicy());
		}
	}
}