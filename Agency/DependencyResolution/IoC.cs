namespace Agency.DependencyResolution {
	using StructureMap;

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
	public static class IoC {
		public static IContainer Initialize() {
			return new Container(c => c.AddRegistry<DependencyRegistry>());
		}
	}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}