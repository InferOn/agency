﻿(function () {
	document.title = "Agency swagger API "; // fix title

	var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
	link.type = 'image/x-icon';
	link.rel = 'shortcut icon';
	link.href = 'http://www.stackoverflow.com/favicon.ico';
	link.href = 'http://www.inferon.it/Assetts/Images/Favicon/favicon-32x32.png';
	document.getElementsByTagName('head')[0].appendChild(link);  // fix favicon

	var toolbarTitle = document.querySelector(".logo__title");
	toolbarTitle.innerText = "Agency API"; // fix toolbar icon

	var input_apiKey = document.querySelector("#input_apiKey");
	input_apiKey.style.display = "none"; // remove api keys

}());