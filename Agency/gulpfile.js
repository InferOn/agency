/// <binding ProjectOpened='build-local-watch' />

var prefix = "./node_modules/";
var gulp = require("gulp");
var watch = require("gulp-watch");
var tslint = require("gulp-tslint");
var ngConfig = require('gulp-ng-config');
var gutil = require("gulp-util");
var concat = require("gulp-concat");
var sourcemaps = require("gulp-sourcemaps");
var minify = require("gulp-js-minify");
var ts = require("gulp-typescript");
var sass = require('gulp-sass');
var autoprefixer = require("gulp-autoprefixer");
var imagemin = require("gulp-imagemin");
var browserSync = require("browser-sync").create();
var less = require("gulp-less");

gulp.task("environment_local_config", function () {
	return gulp.src("./config.json")
	.pipe(ngConfig("appConfig", {
		environment: "env.local"
	}))
	.pipe(gulp.dest("./application/"))
});
gulp.task("environment_dev_config", function () {
	return gulp.src("./config.json")
	.pipe(ngConfig("appConfig", {
		environment: "env.develop"
	}))
	.pipe(gulp.dest("./application/"))
});
gulp.task("environment_prod_config", function () {
	return gulp.src("./config.json")
	.pipe(ngConfig("appConfig", {
		environment: "env.production"
	}))
	.pipe(gulp.dest("./application/"))
});

gulp.task("ensure_templates", function () {
	gulp.src("./application/templates/**/*.html")
		.pipe(gulp.dest("./app/templates/"))
	gulp.src("./application/components/**/*.html")
		.pipe(gulp.dest("./app/components/"))
})

gulp.task("build_dist_js", function () {
	return gulp.src([
		"./application/config.js",
		"./application/utils/constants.js",
		"./application/app.js",

		"./application/services/fetchrequest.js",
		"./application/services/apiService.js",
		"./application/services/authInterceptorServiceFactory.js",
		"./application/services/authServiceFactory.js",
		"./application/controllers/mainController.js",
		"./application/controllers/headerController.js",
		"./application/controllers/menuController.js",
		"./application/controllers/table-base-controller.js",

		"./application/controllers/offers/filtersController.js",
		"./application/controllers/offers/backFiltersController.js",
		"./application/controllers/offers/closeFiltersController.js",
		"./application/controllers/offers/offer-search-controller.js",
		"./application/controllers/offers/offer-table-controller.js",

		"./application/controllers/trafficsources/trafficsources-table-controller.js",

		"./application/controllers/campains/campain-search-controller.js",
		"./application/controllers/campains/campain-table-controller.js",

		"./application/controllers/networks/networks-table-controller.js",
		"./application/controllers/networks/networks-dialog-controller.js",
		
		"./application/controllers/historycampains/history-campain-table-controller.js",
		"./application/controllers/historycampains/history-campain-dialog-controller.js"


	])
	.pipe(sourcemaps.init())
	.pipe(concat("dist.js"))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest("./app"))
});

gulp.task("build_vendor_css", function () {
	return gulp.src([
		"./assets/css/material/css/angular-material-1-1-3.min.css",
		"./assets/css/material/css/md-data-table.min.css",
		"./assets/css/material/css/md-expansion-panel.min.css",
		"./assets/css/ui-grid.min.css"
		/*
			<link href="/assets/css/angular-material-1-1-3.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/css/md-data-table-style.css" rel="stylesheet" type="text/css" />
			<link href="/assets/css/style.min.css" rel="stylesheet" type="text/css" />
		 */
	])
	.pipe(concat("vendor.css"))
	.pipe(gulp.dest("./assets/css"))

})

gulp.task("build_vendor_js", function () {
	return gulp.src([
		"./vendor/jquery.min.js",
		"./vendor/amplify.js",
		"./vendor/moment.min.js",
		"./vendor/angular-1-4-8.min.js",
		"./vendor/clipboard.min.js",
		"./vendor/jquery.color.js",
		"./vendor/ngclipboard.js",
		"./vendor/md-expansion-panel.min.js",
		"./vendor/ui-grid.min.js",
		"./vendor/fixed-table-header.min.js",
		"./vendor/angular-ui-router.min.js",
		"./vendor/angular-animate-1-4-8.min.js",
		"./vendor/angular-messages-1-4-8.js",
		"./vendor/angular-aria-1-4-8.min.js",
		"./vendor/angular-material-1-1-3.min.js",
		"./vendor/angular-momentjs.min.js",
		
		"./vendor/angular-sanitize.js",
		"./vendor/angular-local-storage.min.js",
		"./vendor/underscore-min.js",
		"./vendor/lodash.min.js",
		"./vendor/angular-material-icons.min.js",
		"./vendor/md-data-table-templates.js",
		"./vendor/md-data-table.js",
		"./vendor/iso-3166-country-codes-angular.min.js"
	])
	.pipe(sourcemaps.init())
	.pipe(concat("vendor.js"))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest("./app"))
})
gulp.task("minify_dist_js", function () {
	return gulp.src(["./app/dist.js"])
	.pipe(sourcemaps.init())
	.pipe(minify())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest("./app/app.min.js"))
})
gulp.task("transpile_ts", function () {
	return gulp.src('./application/**/*.ts')
			.pipe(ts({
				declarationFiles: false,
				noResolve: false,
				noImplicitAny: false,
				sourceMap: true
			}))
			.pipe(gulp.dest("./application/"))
})
gulp.task("tslint", function () {
	return gulp.src("./application/**/*.ts")
			.pipe(tslint({
				formatter: "verbose",
				configuration: {
					rules: {
						curly: true,
						eofline: false,
						forin: true,
						radix: true,
						semicolon: true,
						typedef: true,
						ban: true,
						"indent": [true, "tabs"],
						"no-duplicate-variable": true,
						"no-eval": true,
						"no-internal-module": true,
						"no-var-keyword": true,
						"one-line": [true, "check-open-brace", "check-whitespace"],
						"quotemark": [true, "double"],
						"triple-equals": [true, "allow-null-check"],
						"typedef-whitespace": [true, {
							"call-signature": "nospace",
							"index-signature": "nospace",
							"parameter": "nospace",
							"property-declaration": "nospace",
							"variable-declaration": "nospace"
						}],
						"variable-name": [true, "ban-keywords"],
						"whitespace": [true,
							"check-branch",
							"check-decl",
							"check-operator",
							"check-separator",
							"check-type"
						]
					},
					"jsRules": {
						"indent": [true, "tabs"],
						"no-duplicate-variable": true,
						"no-eval": true,
						"one-line": [true, "check-open-brace", "check-whitespace"],
						"quotemark": [true, "double"],
						"semicolon": true,
						"triple-equals": [true, "allow-null-check"],
						"variable-name": [true, "ban-keywords"],
						"whitespace": [true,
							"check-branch",
							"check-decl",
							"check-operator",
							"check-separator",
							"check-type"
						]
					}
				}
			}))
			.pipe(tslint.report({
				emitError: true, summarizeFailureOutput: true
			}));

});
gulp.task("ts-sugar", ["transpile_ts", "tslint", ])
gulp.task("js-bundler", ["build_vendor_js", "build_dist_js" ])
gulp.task("pre-build-local", [
	"environment_local_config",
	"ensure_templates",
	"less",
	"ts-sugar",
	"js-bundler"
])
gulp.task("pre-build-dev", [
	"environment_dev_config",
	"ensure_templates",
	"ts-sugar",
	"js-bundler"
])
gulp.task("pre-build-prod", [
	"environment_prod_config",
	"ensure_templates",
	"ts-sugar",
	"js-bundler"
])

gulp.task('ts-watch', function () {
	return gulp.watch('./application/**/*.ts', ["ts-sugar"]);
});

gulp.task('build-local-watch', function () {
	return gulp.watch([
	"./application/**/*.ts",
	"./application/*.html",
	"./application/**/*.html",
	"./index.html",
	"./vendor/md-data-table-templates.js",
	"./less/**/*.less"], ["pre-build-local"]);
});

gulp.task("autoprefixer", function () {
	var srcPath = "./assets/css/**/*.css";
	var destPath = "./assets/css/**/*.css";

	return gulp.src(srcPath)
				.pipe(autoprefixer({
					browsers: ['last 2 versions', 'ie 9', 'ios 6', 'android 4'],
					map: true
				}))
				.pipe(gulp.dest(destPath))
})

gulp.task("imagemin", function () {
	gulp.src('./images/*')
				.pipe(imagemin(
				[
					imagemin.jpegtran({ progressive: true }),
					imagemin.optipng({ optimizationLevel: 7 }),
				]
				))
				.pipe(gulp.dest('./assets/images'))
})

gulp.task('serve', ['sass'], function () {

	browserSync.init({
		server: "./"
	});

	gulp.watch("./scss/**/*.scss", ['sass'], ['autoprefixer']);

});

gulp.task("sass", function () {
	return gulp.src('./scss/**/*.scss')
		.pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
		.pipe(gulp.dest('./assets/css'))
		//.pipe(gulp.dest('./Content/'))
		.pipe(browserSync.stream());
});

gulp.task("less", function () {
	gulp.src('./less/**/*.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		//.pipe(concat("dist.css"))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./assets/css/'));
});
gulp.task('default', ['serve']);